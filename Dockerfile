### STAGE 1: Build ###
FROM node:alpine AS build

#### make the 'app' folder the current working directory
WORKDIR /app

#### copy things
COPY . .

#### install angular cli
RUN npm install -g @angular/cli

#### install project dependencies
RUN npm ci && npm run build

### STAGE 2: Run ###
FROM nginxinc/nginx-unprivileged 

#### copy nginx conf
#COPY nginx.conf /etc/nginx/nginx.conf

#### copy artifact build from the 'build environment'
COPY --from=build /app/dist/ultima /usr/share/nginx/html

#### don't know what this is, but seems cool and techy
CMD ["nginx", "-g", "daemon off;"]
