import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppNotfoundComponent } from './ui/components/pages/notFound/app.notfound.component';
import { AppUIComponent } from './ui/components/app.ui.component';

export const routes: Routes = [
    {
        path: '',
        component: AppUIComponent,
        loadChildren: './ui/components/app.ui.module#AppUIModule'
    },
    { path: '**', component: AppNotfoundComponent }
];

export const AppRoutes: ModuleWithProviders = RouterModule.forRoot(routes, { scrollPositionRestoration: 'enabled' });
