import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { AppRoutes } from './app.routes';
import { ServiceModule } from './infraestructure/services/service.module';
import { AppUIModule } from './ui/components/app.ui.module';
import {TriStateCheckboxModule} from "primeng/tristatecheckbox";
import {NgxSpinnerModule} from "ngx-spinner";
import {InterceptedService} from "./infraestructure/services/spinner/intercepted.service";
import {InputTextModule} from 'primeng/inputtext';
import {ToastModule} from 'primeng/toast';

import {FormsModule} from "@angular/forms";
import {CKEditorModule} from "@ckeditor/ckeditor5-angular";
import {EditorModule} from "primeng/editor";

@NgModule({
    declarations: [
        AppComponent,
    ],
    imports: [
        AppUIModule,
        BrowserModule,
        AppRoutes,
        HttpClientModule,
        ServiceModule,
        BrowserAnimationsModule,
        TriStateCheckboxModule,
        InputTextModule,
        ToastModule,
        CKEditorModule,
        FormsModule,
        EditorModule,
        NgxSpinnerModule,
    ],
    providers: [
        { provide: LocationStrategy, useClass: HashLocationStrategy },
        { provide: HTTP_INTERCEPTORS, useClass: InterceptedService, multi: true},
    ],
    bootstrap: [AppComponent]
})
export class AppModule {C}
