import {CommonModule} from '@angular/common';
import {NgModule} from "@angular/core";
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {TriStateCheckboxModule} from "primeng";
import {PRIMENG_MODULES} from 'src/app/shared/enums/primeng.elements';
import {PipeModule} from 'src/app/shared/pipes/pipe.module';
import {AppUIComponent} from './app.ui.component';
import {AppUIRoutes} from './app.ui.routes';
import {LayoutModule} from './layouts/layout.module';
import {AppAccessdeniedComponent} from './pages/accessDenied/app.accessdenied.component';
import {AppDashBoardComponent} from './pages/dashboard/app.dashBoard.component';
import {AppNotfoundComponent} from './pages/notFound/app.notfound.component';
import {AppAssignedProceduresConponent} from './tools/HealthRegister/assignedProcedures/app.assignedProcedures.component';
import {AssignmentComponent} from './tools/HealthRegister/assignment/assignment.component';
import {ConceptsComponent} from './tools/HealthRegister/concepts/concepts.component';
import {ConsultHealthRecordComponent} from "./tools/HealthRegister/consultHealthRecord/consultHealthRecord.component";
import {ModalityImportSellComponent} from './tools/HealthRegister/modalityImportSell/modalityImportSell.component';
import {PopUpVerifyComponent} from './tools/HealthRegister/pop-up-verify/pop-up-verify.component';
import {AppProcedureInformationComponent} from './tools/HealthRegister/procedureInformation/app.procedureInformation.component';
import {ReAssignTaskComponent} from './tools/HealthRegister/re-assign-task/re-assign-task.component';
import {AlcoholicDrinksComponent} from './pages/procedure/shared/ti-alcoholicDrinks/app.ti-alcoholicDrinks.component';
import {AppTiSimpleCopyComponent} from "./pages/procedure/shared/ti-simpleCopy/app.ti-simpleCopy.component";
import {AppTiMedicalDevicesComponent} from "./pages/procedure/shared/ti-medicalDevices/app.ti-medicalDevices.component";
import {AppGenericTableComponent} from "../../shared/components/genericTable/app.genericTable.component";
import {RadioButtonModule} from 'primeng/radiobutton';
import { ManageAssignedTasksComponent } from './tools/HealthRegister/manage-assigned-tasks/manage-assigned-tasks.component';
import { DocumentaryProductionComponent } from './tools/HealthRegister/documentaryProduction/documentaryProduction.component';
import { GeneralInformationComponent } from './tools/HealthRegister/general-information/general-information.component';
import { UnifyConceptsComponent } from './tools/HealthRegister/unify-concepts/unify-concepts.component';
import { AppAssignedProceduresComponent } from './tools/HealthRegister/app.assigned-procedures/app.assigned-procedures.component';
import {NgxSpinnerModule} from "ngx-spinner";

import {TiPharmacologicalEvaluationTwoComponent} from './pages/procedure/shared/technicalInformation/ti-pharmacological-evaluation-two/app.ti-pharmacological-evaluation-two.component';
import {TiPharmacologicalEvaluationComponent} from './pages/procedure/shared/technicalInformation/ti-pharmacological-evaluation/ti-pharmacological-evaluation.component';
import {TiPharmacologicalEvaluationFourComponent} from './pages/procedure/shared/technicalInformation/ti-pharmacological-evaluation-four/ti-pharmacological-evaluation-four.component';
import {TiPharmacologicalEvaluationSixComponent} from './pages/procedure/shared/technicalInformation/ti-pharmacological-evaluation-six/ti-pharmacological-evaluation-six.component';
import {TiPharmacologicalEvaluationEightComponent} from './pages/procedure/shared/technicalInformation/ti-pharmacological-evaluation-eight/ti-pharmacological-evaluation-eight.component';
import {TiPharmacologicalEvaluationFiveComponent} from './pages/procedure/shared/technicalInformation/ti-pharmacological-evaluation-five/app.ti-pharmacological-evaluation-five.component';
import {TiPharmacologicalEvaluationSevenComponent} from './pages/procedure/shared/technicalInformation/ti-pharmacological-evaluation-seven/app.ti-pharmacological-evaluation-seven.component';
import {AppProductInfoComponent} from "./pages/procedure/shared/productInfo/app.productInfo.component";
import {AppProcedureInfoComponent} from "./pages/procedure/shared/procedureInfo/app.procedureInfo.component";
import {AppHealthRegisterInfoComponent} from "./pages/procedure/shared/healthRegisterInfo/app.healthRegisterInfo.component";
import {AppCertificateComponent} from "./pages/procedure/shared/certificate/app.certificate.component";
import {AppLegalInformationAttorneyComponent} from "./pages/procedure/shared/legalInformationAttorney/app.legalInformationAttorney.component";
import {AppLegalInformationHeadlineComponent} from "./pages/procedure/shared/legalInformationHeadline/app.legalInformationHeadline.component";
import {AppLegalInformationRoleComponent} from "./pages/procedure/shared/legalInformationRole/app.legalInformationRole.component";
import {AppElectronicNotificationComponent} from "./pages/procedure/shared/electronicNotification/app.electronicNotification.component";
import {AppShowMoreComponent} from "./pages/procedure/shared/showMore/app.showMore.component";
import {AppPopupRoleComponent} from "./pages/procedure/shared/popupRole/app.popupRole.component";
import {AppPopupHeadlineComponent} from "./pages/procedure/shared/popupHeadline/app.popupHeadline.component";
import {AppPopupAttorneyComponent} from "./pages/procedure/shared/popupAttorney/app.popupAttorney.component";
import {AppPaymentDescriptionComponent} from "./pages/procedure/shared/paymentDescription/app.paymentDescription.component";
import {AppConfirmPaymentConsignmentComponent} from "./pages/procedure/shared/confirmPaymentConsignment/app.confirmPaymentConsignment.component";
import {AppConfirmPaymentPseComponent} from "./pages/procedure/shared/confirmPaymentPse/app.confirmPaymentPse.component";
import {AppRateInformationComponent} from "./pages/procedure/shared/rateInformation/app.rateInformation.component";
import {AppGuidingQuestionsComponent} from "./pages/procedure/shared/guidingQuestions/app.guidingQuestions.component";
import {AppSearchFilterComponent} from "./pages/procedure/shared/searchFilter/app.searchFilter.component";
import {AppConsultFiledComponent} from "./pages/procedure/shared/consultFiled/app.consultFiled.component";
import {AppTableOfRadicatesComponent} from "./pages/procedure/shared/tableOfRadicates/app.tableOfRadicates.component";
import { TiMedicinesComponent } from './pages/procedure/shared/technicalInformation/ti-medicines/ti-medicines';
import {TiPharmacologicalEvaluationThreeComponent} from "./pages/procedure/shared/technicalInformation/ti-pharmacological-evaluation-three/ti-pharmacological-evaluation-three.component";
import {CKEditorModule} from "@ckeditor/ckeditor5-angular";
import { PagesComponent } from './pages/pages.component';

@NgModule({
    declarations: [
        AppUIComponent,
        AppNotfoundComponent,
        AppAccessdeniedComponent,
        AppDashBoardComponent,
        AppProductInfoComponent,
        AppProcedureInfoComponent,
        AppHealthRegisterInfoComponent,
        AlcoholicDrinksComponent,
        AppTiSimpleCopyComponent,
        AppTiMedicalDevicesComponent,
        AppCertificateComponent,
        AppLegalInformationAttorneyComponent,
        AppLegalInformationHeadlineComponent,
        AppLegalInformationRoleComponent,
        AppElectronicNotificationComponent,
        AppShowMoreComponent,
        AppPopupRoleComponent,
        AppPopupHeadlineComponent,
        AppPopupAttorneyComponent,
        ConceptsComponent,
        ConsultHealthRecordComponent,
        ModalityImportSellComponent,
        AppAssignedProceduresComponent,
        ReAssignTaskComponent,
        AssignmentComponent,
        DocumentaryProductionComponent,
        PopUpVerifyComponent,
        ManageAssignedTasksComponent,
        GeneralInformationComponent,
        UnifyConceptsComponent,
        AppAssignedProceduresConponent,
        AppProcedureInformationComponent,
        AppPaymentDescriptionComponent,
        AppConfirmPaymentConsignmentComponent,
        AppConfirmPaymentPseComponent,
        AppRateInformationComponent,
        AppGuidingQuestionsComponent,
        AppSearchFilterComponent,
        AppConsultFiledComponent,
        AppTableOfRadicatesComponent,
        AppGenericTableComponent,
        TiMedicinesComponent,
        TiPharmacologicalEvaluationTwoComponent,
        TiPharmacologicalEvaluationComponent,
        TiPharmacologicalEvaluationFourComponent,
        TiPharmacologicalEvaluationTwoComponent,
        TiPharmacologicalEvaluationSixComponent,
        TiPharmacologicalEvaluationThreeComponent,
        TiPharmacologicalEvaluationEightComponent,
        TiPharmacologicalEvaluationFiveComponent,
        TiPharmacologicalEvaluationSevenComponent,
        PagesComponent,
    ],
    exports: [
        AppUIComponent,
        AppNotfoundComponent,
        AppAccessdeniedComponent,
        AppDashBoardComponent,
        AppProductInfoComponent,
        AppProcedureInfoComponent,
        AppHealthRegisterInfoComponent,
        AlcoholicDrinksComponent,
        AppTiSimpleCopyComponent,
        AppTiMedicalDevicesComponent,
        AppCertificateComponent,
        AppLegalInformationAttorneyComponent,
        AppLegalInformationHeadlineComponent,
        AppLegalInformationRoleComponent,
        AppElectronicNotificationComponent,
        AppShowMoreComponent,
        AppPopupRoleComponent,
        AppPopupHeadlineComponent,
        AppPopupAttorneyComponent,
        ConceptsComponent,
        ConsultHealthRecordComponent,
        ModalityImportSellComponent,
        ReAssignTaskComponent,
        AssignmentComponent,
        AppAssignedProceduresConponent,
        DocumentaryProductionComponent,
        PopUpVerifyComponent,
        AppProcedureInformationComponent,
        AppPaymentDescriptionComponent,
        AppConfirmPaymentConsignmentComponent,
        AppConfirmPaymentPseComponent,
        AppRateInformationComponent,
        AppGuidingQuestionsComponent,
        AppSearchFilterComponent,
        AppConsultFiledComponent,
        AppTableOfRadicatesComponent,
        AppGenericTableComponent,
        TiMedicinesComponent,
        TiPharmacologicalEvaluationTwoComponent,
        TiPharmacologicalEvaluationComponent,
        TiPharmacologicalEvaluationFourComponent,
        TiPharmacologicalEvaluationTwoComponent,
        TiPharmacologicalEvaluationSixComponent,
        TiPharmacologicalEvaluationThreeComponent,
        TiPharmacologicalEvaluationEightComponent,
        TiPharmacologicalEvaluationFiveComponent,
        TiPharmacologicalEvaluationSevenComponent,
        AppAssignedProceduresComponent
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        RadioButtonModule,
        FormsModule,
        LayoutModule,
        ...PRIMENG_MODULES,
        AppUIRoutes,
        PipeModule.forRoot(),
        TriStateCheckboxModule,
    ],
    // entryComponents: [PopUpReusableComponent]
})
export class AppUIModule {}
