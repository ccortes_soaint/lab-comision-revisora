import { RouterModule, Routes } from '@angular/router';
import { AppAccessdeniedComponent } from './pages/accessDenied/app.accessdenied.component';
import { AppUIComponent } from './app.ui.component';
import { UI_ROUTES_PATH } from './app.ui.routing.name';
import { AppDashBoardComponent } from './pages/dashboard/app.dashBoard.component';
import { AuthGuardService } from 'src/app/infraestructure/services/auth/authGuard.service';


const UI_ROUTES: Routes = [
    { path: UI_ROUTES_PATH.accessDenied, component: AppAccessdeniedComponent },
    { path: '', redirectTo: UI_ROUTES_PATH.dashboard, pathMatch: 'full' },
    {
        path: '',
        component: AppUIComponent,
        data: {
            breadcrumb: UI_ROUTES_PATH.home
        },
        children: [
            // {
                // canActivate: [AuthGuardService],
                // path: UI_ROUTES_PATH.modal,
                // component: ListComponentComponent,
                // data: {
                //     breadcrumb: UI_ROUTES_PATH.dashboard
                // }
            // }, 
            {
                canActivate: [AuthGuardService],
                path: UI_ROUTES_PATH.dashboard,
                component: AppDashBoardComponent,
                data: {
                    breadcrumb: UI_ROUTES_PATH.dashboard
                }
            },
            {
                canActivate: [AuthGuardService],
                path: UI_ROUTES_PATH.procedure.value,
                loadChildren: () => import('src/app/ui/components/pages/procedure/app.procedure.module').then(mod => mod.ProcedureModule)
            },
            {
                canActivate: [AuthGuardService],
                path: UI_ROUTES_PATH.healthRecord.value,
                loadChildren: () => import('src/app/ui/components/pages/healthRecord/app.healthRecord.module').then(mod => mod.HealthRecordModule)
            },
            {
                canActivate: [AuthGuardService],
                path: UI_ROUTES_PATH.audit.value,
                loadChildren: () => import('src/app/ui/components/pages/audit/app.audit.module').then(mod => mod.AuditModule)
            },
            {
                /* canActivate: [AuthGuardService], */
                path: UI_ROUTES_PATH.scheduling.value,
                //loadChildren: () => import('src/app/ui/components/pages/unification/unification.module').then(mod => mod.UnificationModule)
                loadChildren: () => import('src/app/ui/components/pages/scheduling/scheduling.module').then(mod => mod.SchedulingModule)
                //loadChildren: () => import('src/app/ui/components/pages/clarifications/clarifications.module').then(mod => mod.ClarificationsModule)
            },
            {
                /* canActivate: [AuthGuardService], */
                path: UI_ROUTES_PATH.executionRoom.value,
                loadChildren: () => import('src/app/ui/components/pages/execution-room/execution-room.module').then(mod => mod.ExecutionRoomModule)
            },
            {
                /* canActivate: [AuthGuardService], */
                path: UI_ROUTES_PATH.inquiries.value,
                loadChildren: () => import('src/app/ui/components/pages/inquiries/app.inquiries.module').then(mod => mod.AppInquiriesModule)
            },
        ]
    }
];

export const AppUIRoutes = RouterModule.forChild(UI_ROUTES);