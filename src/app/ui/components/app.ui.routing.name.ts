export const UI_ROUTES_PATH = {
    home: 'Inicio',
    dashboard: 'Dashboard',
    accessDenied: 'AccessDenied',
    procedure: { value: 'Radicacion', name: 'Radicación' },
    healthRecord: { value: 'RegistroSanitario', name: 'Comision revisora' },
    audit: { value: 'Auditoria', name: 'Auditoria' },
    scheduling: { value: 'scheduling', name: 'Trabajo Agendado' },
    executionRoom: { value: 'executionRoom', name: 'Ejecución Sala' },
    inquiries: { value: 'inquiries', name: 'Consultas' },
}