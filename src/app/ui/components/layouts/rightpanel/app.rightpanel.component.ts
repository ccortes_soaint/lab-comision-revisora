import { Component } from '@angular/core';
import { AppUIComponent } from '../../app.ui.component';

@Component({
	selector: 'app-rightpanel',
	templateUrl: './app.rightpanel.component.html'
})
export class AppRightpanelComponent {

	constructor(public app: AppUIComponent) {
	}
}
