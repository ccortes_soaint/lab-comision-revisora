import { Component } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { MenuItem } from 'primeng/primeng';
import { filter } from 'rxjs/operators';

@Component({
    selector: 'app-breadcrumb',
    templateUrl: './app.breadcrumb.component.html'
})
export class AppBreadcrumbComponent {

    static readonly ROUTE_DATA_BREADCRUMB = 'breadcrumb';
    readonly home = { icon: ' ', url: 'home' };
    menuItems: MenuItem[];

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute
    ) {
        this.router.events
            .pipe(filter(event => event instanceof NavigationEnd))
            .subscribe(() => {
                this.menuItems = this.createBreadcrumbs(this.activatedRoute.root)
            });
    }

    private createBreadcrumbs(route: ActivatedRoute, url: string = '#', breadcrumbs: MenuItem[] = []): MenuItem[] {
        const children: ActivatedRoute[] = route.children;
        
        if (children.length === 0)
            return breadcrumbs;

        for (const child of children) {
            const routeURL: string = child.snapshot.url.map(segment => segment.path).join('/');
            if (routeURL !== '')
                url += `/${routeURL}`;

            var label = child.snapshot.data[AppBreadcrumbComponent.ROUTE_DATA_BREADCRUMB];
            if (!!label && label != undefined) {
                if (label.indexOf(':id') > -1)
                    label = routeURL;

                breadcrumbs.push({ label, url });
            }

            return this.createBreadcrumbs(child, url, breadcrumbs);
        }
    }
}
