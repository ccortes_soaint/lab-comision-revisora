import { Component, OnDestroy } from '@angular/core';
import { MenuItem } from 'primeng';
import { Subscription } from 'rxjs';
import { UserDTO } from 'src/app/domain/model/dto/auth/userDTO';
import { LoginService } from 'src/app/infraestructure/services/auth/login.service';
import { MenuService } from 'src/app/infraestructure/services/menu/app.menu.service';
import { PoliciesService } from 'src/app/infraestructure/services/profile/policies.service';
import { ORDER_MENU } from '../../app.ui.environment';
import {RolDTO} from '../../../../domain/model/dto/profile/rolDTO';

@Component({
    selector: 'app-menu',
    templateUrl: './app.menu.component.html'
})
export class AppMenuComponent implements OnDestroy {

    model: any[];
    ownerUser: UserDTO;
    currentUser$: Subscription;
    menu$: Subscription;

    constructor(
        private loginService: LoginService,
        private menuService: MenuService,
        private policieService: PoliciesService
    ) {
        this.currentUser$ = this.loginService.currentUser.subscribe(x => {
            this.ownerUser = x;
        });


        this.menu$ = this.menuService.menu$.subscribe(elements => {
            if (elements && elements.length)
                this.model = this.callMenuItem(elements, this.ownerUser);
        });
    }

    callMenuItem(options: any[], ownerUser: UserDTO): Array<MenuItem> {
        var roles = ['any'];
        var model = new Array<MenuItem>();
        var menu = new Array<MenuItem>();
        var pass = new Array<string>();
        var pagesPolicies = this.policieService.pagesPoliciesValue();

        if (!!ownerUser && !!ownerUser.roles)
            ownerUser.roles.forEach((element: RolDTO) => {
                roles.push(element.rol);
            });

        if (pagesPolicies && pagesPolicies.length)
            for (let idx = 0; idx < pagesPolicies.length; idx++) {
                for (let index = 0; index < roles.length; index++) {
                    const role = roles[index];
                    if (role.toUpperCase() == pagesPolicies[idx].role.toUpperCase()) {
                        if (pagesPolicies[idx].menuHeaderPass)
                            pagesPolicies[idx].menuHeaderPass.forEach(item => {
                                if (item.policiesStatus)
                                    pass.push(item.nameCode)
                            });
                        break;
                    }
                }
            }

        if (pass.length > 0) {
            pass = pass.filter((value, index, matriz) => matriz.indexOf(value) === index);
            for (let idx = 0; idx < pass.length; idx++) {
                for (let idy = 0; idy < options.length; idy++) {
                    if (options[idy] && options[idy].id) {
                        if (pass[idx].toUpperCase() == options[idy].id.toUpperCase()) {
                            model.push(options[idy]);
                        }
                    }
                }
            }
        }

        loop1: for (let idx = 0; idx < ORDER_MENU.length; idx++) {
            const order = ORDER_MENU[idx].toUpperCase();
            loop2: for (let idy = 0; idy < model.length; idy++) {
                if (model[idy].id && model[idy].id.toUpperCase() === order) {
                    menu.push(model[idy]);
                    break loop2;
                }
            }
        }

        return menu;
    }

    ngOnDestroy() {
        this.currentUser$.unsubscribe();
        this.menu$.unsubscribe();
    }
}
