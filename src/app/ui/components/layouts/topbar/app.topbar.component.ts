import { Component } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { LoginService } from 'src/app/infraestructure/services/auth/login.service';
import { MenuService } from 'src/app/infraestructure/services/menu/app.menu.service';
import { AppUIComponent } from '../../app.ui.component';

@Component({
    selector: 'app-topbar',
    templateUrl: './app.topbar.component.html'
})
export class AppTopbarComponent {

    constructor(
        public app: AppUIComponent,
        private readonly oauthService: OAuthService,
        private menuService: MenuService,
        private loginService: LoginService
    ) { }

    logout() {
        this.menuService.resetMenu();
        this.loginService.logout();
        this.oauthService.logOut();
    }
}
