import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PRIMENG_MODULES } from 'src/app/shared/enums/primeng.elements';
import { AppBreadcrumbComponent } from './breadcrumb/app.breadcrumb.component';
import { AppFooterComponent } from './footer/app.footer.component';
import { AppMenuComponent } from './menu/app.menu.component';
import { AppMenuitemComponent } from './menuItem/app.menuitem.component';
import { AppInlineProfileComponent } from './profile/app.profile.component';
import { AppRightpanelComponent } from './rightpanel/app.rightpanel.component';
import { AppTopbarComponent } from './topbar/app.topbar.component';

@NgModule({
    declarations: [
        AppBreadcrumbComponent,
        AppFooterComponent,
        AppMenuComponent,
        AppMenuitemComponent,
        AppInlineProfileComponent,
        AppRightpanelComponent,
        AppTopbarComponent
    ],
    exports: [
        AppBreadcrumbComponent,
        AppFooterComponent,
        AppMenuComponent,
        AppMenuitemComponent,
        AppInlineProfileComponent,
        AppRightpanelComponent,
        AppTopbarComponent
    ],
    imports: [
        CommonModule,
        ...PRIMENG_MODULES
    ]
})
export class LayoutModule { }
