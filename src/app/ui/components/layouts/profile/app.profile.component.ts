import { Component } from '@angular/core';
import { trigger, state, transition, style, animate } from '@angular/animations';
import { AppUIComponent } from '../../app.ui.component';
import { AppComponent } from 'src/app/app.component';
import { OAuthService } from 'angular-oauth2-oidc';

@Component({
    selector: 'app-inline-profile',
    templateUrl: './app.profile.component.html',
    animations: [
        trigger('menu', [
            state('hidden', style({
                height: '0px'
            })),
            state('visible', style({
                height: '*'
            })),
            transition('visible => hidden', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)')),
            transition('hidden => visible', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)'))
        ])
    ]
})
export class AppInlineProfileComponent {

    active: boolean;

    constructor(
        public app: AppUIComponent,
        private oauthService: OAuthService
    ) { }

    onClick(event) {
        this.active = !this.active;
        event.preventDefault();
    }

    onLogout() {
        // this.loginService.logout();
        // this.scs.onDisconnect();
        // this.policieService.addPagesPolicies([DEFAULT_POLICIES]);
        // this.menuService.addItemsMenu(this.addMenuItem());
        // this.router.navigate(['/' + PAGES_ROUTES_PATH.user + '/' + USER_PATH.login]);
        this.oauthService.logOut();
    }
}
