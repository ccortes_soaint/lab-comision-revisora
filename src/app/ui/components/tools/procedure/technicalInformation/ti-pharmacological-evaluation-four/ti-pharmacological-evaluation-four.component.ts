import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-ti-pharmacological-evaluation-four',
  templateUrl: './ti-pharmacological-evaluation-four.component.html',
  styles: []
})
export class TiPharmacologicalEvaluationFourComponent implements OnInit {
    form: FormGroup;
    resultInformation: any;
    formOtherStudy: FormGroup;



  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
      this.initForm();




      this.resultInformation = [
          {
              titleStudy: 'xxxx',
              parameter: 'xxxx',
              title: 'xxxx',
              resume: 'xxxx',
              folio: 'xxxx',
              actions: 'xxxx'
          },
          {
              titleStudy: 'xxxx',
              parameter: 'xxxx',
              title: 'xxxx',
              resume: 'xxxx',
              folio: 'xxxx',
              actions: 'xxxx'
          }

      ];
  }
    initForm() {
      this.formOtherStudy = this.formBuilder.group({
          'studyTitle': [null , Validators.required],
          'resume': [null , Validators.required],
          'folio': [null , Validators.required]
      });
        this.form = this.formBuilder.group({
            'parametro': [null , Validators.required],
            'title': [null , Validators.required],
            'resume': [null , Validators.required],
            'folio': [null , Validators.required]

        });
    }

    add() {
      if (this.form.valid) {

          this.form.reset();
      }

      if ( this.formOtherStudy.valid){
          this.formOtherStudy.reset();
      }
        console.log(this.form)
    }


}
