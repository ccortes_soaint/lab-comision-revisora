import { Component, OnInit } from '@angular/core';
import {DATE_ES} from "../../../app.ui.environment";


interface Option {
    name: string;
    code: string;
}
@Component({
  selector: 'app-consult-health-record',
  templateUrl: './consultHealthRecord.component.html',
  styles: []
})
export class ConsultHealthRecordComponent implements OnInit {

    es = DATE_ES;
    requestDate: Date;
    options: any[];
    selectedOption: Option;
    options2: any[];

    constructor() {


    }
    ngOnInit() {
        this.options = [
            {name: 'option 1', code: 'op1'},
            {name: 'Option 2', code: 'op2'},
            {name: 'option 3', code: 'op3'},
            {name: 'option 4', code: 'op4'},
            {name: 'option 5', code: 'op5'}
        ];

        this.options2 = [
            {name: 'option 1', code: 'op1'},
            {name: 'Option 2', code: 'op2'},
            {name: 'option 3', code: 'op3'},
            {name: 'option 4', code: 'op4'},
            {name: 'option 5', code: 'op5'}
        ];


    }


}
