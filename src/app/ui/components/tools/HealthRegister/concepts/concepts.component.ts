import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {SpinnerService} from "../../../../../infraestructure/services/spinner/spinner.service";
import {VersionDocumentoDTO} from "../../../../../domain/model/dto/document/DocumentoDTO";

@Component({
    selector: 'app-concepts',
    templateUrl: './concepts.component.html',
})
export class ConceptsComponent implements OnInit {

    form: FormGroup;
    concepts: any[];
    generateConcept = false;
    resultAttach: any[];
    uploadedFiles;
    listaVersionesDocumento: VersionDocumentoDTO[] = [];
    pdfFileBase;
    value: any;
    which: any[];

    constructor(private formBuilder: FormBuilder,) { }

    ngOnInit() {
        this.initForm();
        this.concepts = [
            { data: 'xxx', user: 'xxxx', rol: '', concepto: 'xxxxx', decision: '' },
        ];
        this.resultAttach = [
            { nameDoc: 'Formato informacion de evaluacion' },
        ];
        this.which = [
            { name: 'xxx', code: 'xxx' },
            { name: 'xxxx', code: 'xxxx' },
        ];
    }

    initForm() {
        this.form = this.formBuilder.group({
            tipoAnexo: [null],
            descripcion: [null],
        });
    }

    openMod() {
        this.generateConcept = true;
    }

    closeMod() {
        this.generateConcept = false;
    }

    save() {

    }
    attachDocument(event) {
        if (event.files.length === 0) {
            return false;
        }
        this.uploadedFiles = [
            {
                id: this.listaVersionesDocumento.length > 0 ? this.listaVersionesDocumento[this.listaVersionesDocumento.length - 1].id : null,
                nombre: encodeURI(event.files[0].name),
                tipo: 'pdf',
                version: '',
                contenido: '',
                file: event.files[0],
                size: event.files[0].size
            },

        ];

        event.files.forEach((page) => {
            const reader = new FileReader();
            reader.readAsDataURL(page);
            reader.onload = () => {
                this.pdfFileBase = reader.result;
                console.log(this.pdfFileBase);
            };
        });
    }
    onUpload(event) {
        for (let file of event.files) {
            this.uploadedFiles.push(file);
        }
    }
    deleteFile(index) {
        this.uploadedFiles[0].files = null;
    }
}
