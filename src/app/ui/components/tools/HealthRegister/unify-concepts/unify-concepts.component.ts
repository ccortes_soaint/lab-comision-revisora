import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-unify-concepts',
  templateUrl: './unify-concepts.component.html',
  styles: []
})
export class UnifyConceptsComponent implements OnInit {
    tableUnifyConcepts: any [] = [];

  constructor() { }

  ngOnInit() {
      this.tableUnifyConcepts = [
          {group:'Medicamentos', concept:'XXXX', date: '25/11/2020', decision:'Aprueba'},
          {group:'Farmacovigilancia', concept:'XXXX', date: '25/11/2020', decision:'Requiere'}
      ]
  }

}
