import { Component, OnInit } from '@angular/core';
import {MenuItem} from "primeng";

@Component({
  selector: 'app-manage-assigned-tasks',
  templateUrl: './manage-assigned-tasks.component.html',
})
export class ManageAssignedTasksComponent implements OnInit {
    tabIndex = 0;
    activeIndex: number = 0;
    items: MenuItem[];
    isReview: boolean;
  constructor() { }

  ngOnInit(): void {
  }
    disableReview(event) {
      if (event.index == 1){
          this.isReview = true;
      }else {
          this.isReview = false;
      }
    }
}
