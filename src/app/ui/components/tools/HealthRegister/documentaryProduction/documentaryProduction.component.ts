import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-documentary-production',
  templateUrl: './documentaryProduction.component.html',
  styles: []
})
export class DocumentaryProductionComponent implements OnInit {
    tableAssociatedDocument: any[] = [];
    tableDocumentManagement: any[] = [];
    disable: boolean = true;
    options: any [];
    @Input() isReview: boolean;

  constructor() { }

  ngOnInit(){
      this.tableAssociatedDocument = [
          {nameDocument: 'Resolucion', user: 'xxxx', date: '25/11/2020', action:''}
      ],
      this.tableDocumentManagement = [
          {campus:' ' , dependence:'xxxx', rol:'xxxx', officer:'xxx', action:' ' }
      ],
      this.options = [
          {name: 'option 1', code: 'op1'},
          {name: 'Option 2', code: 'op2'},
          {name: 'option 3', code: 'op3'},
          {name: 'option 4', code: 'op4'},
          {name: 'option 5', code: 'op5'}
      ];
  }

}
