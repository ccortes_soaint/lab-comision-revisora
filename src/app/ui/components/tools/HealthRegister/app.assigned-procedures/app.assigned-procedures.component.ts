import {ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {UI_ROUTES_PATH} from '../../../app.ui.routing.name';
import {HEALTH_RECORD_PATH} from "../../../pages/healthRecord/app.healthRecord.routing.name";
import {Router} from "@angular/router";
import {SpinnerService} from "../../../../../infraestructure/services/spinner/spinner.service";
import {ObtenerServiceExampleService} from "../../../../../infraestructure/services/spinner/obtener-service-example.service";
import {SelectItem} from "primeng";
import {CommonApi} from "../../../../../infraestructure/services/common.api";
import {Observable, of} from "rxjs";

@Component({
    selector: 'app-assigned-procedures',
    templateUrl: './app.assigned-procedures.component.html',
})
export class AppAssignedProceduresComponent implements OnInit {
    @Output() sendViewTaskAssigned = new EventEmitter<boolean>();
    toAssign = false;
    public showInfo = false;
    showTable = true;
    rows: number = 5;
    rowsOptions: SelectItem[]
    _Proc: Observable<any>;
    produce: any;

    constructor(private router: Router,
                private commonApi: CommonApi,
                private changeDetection: ChangeDetectorRef) {
    }

    ngOnInit() {
        this.loadRowsOptions();

    }
    @Input('procedures') set procedures$(value: Observable<any>){
        this._Proc = value || of([])
        this.produce = value;
    }
    get procedures$(){
        return this._Proc;
    }


    loadRowsOptions() {
        this.rowsOptions = this.commonApi.getRowsOptions();
    }

    openMod() {
        this.toAssign = true;
    }

    close() {
        this.toAssign = false;
    }

    show() {
        this.showInfo = true;
        this.showTable = false;
    }

    getShow() {
        if (this.showTable == true) {
            return true;
        } else {
            return false;
        }
    }

    viewTaskAssigned() {
        this.sendViewTaskAssigned.emit(true);

        let urlPather = '/' + UI_ROUTES_PATH.healthRecord.value + '/' + HEALTH_RECORD_PATH.assignedWork.value + '/';
        this.router.navigate([urlPather + HEALTH_RECORD_PATH.manageAssignedTask.value]);
    }

    filter(event) {
        const input: string = event.target.value;
        if (input && input.length > 0) {
           this._Proc = this.produce.filter(procedure => procedure.name.startsWith(input))
        } else {
           this._Proc = this.produce
           }
    }
}
