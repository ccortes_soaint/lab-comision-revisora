import {Component, OnInit, ViewChild} from '@angular/core';
import {Table} from 'primeng';

@Component({
    selector: 'app-assignment',
    templateUrl: './assignment.component.html',
})
export class AssignmentComponent implements OnInit {
    assign: any[];
    group: any[];
    customers: any[];

    selectedCustomers: any[];

    representatives: any[];

    statuses: any[];

    loading: boolean = true;

    @ViewChild('dt') table: Table;
    constructor() { }

    ngOnInit(): void {
        this.assign = [
            {group: '', legalName: '', assignprocedureL: '', pendingProcedureL: '', technicalName: '', assignProcedureT: '', pendingProcedureT: ''},
            {group: '', legalName: '', assignprocedureL: '', pendingProcedureL: '', technicalName: '', assignProcedureT: '', pendingProcedureT: ''},

        ];

        this.group = [
            {name: 'xxx', code: ''},
            {name: 'xxxxxx', code: ''},
        ];
    }

}
