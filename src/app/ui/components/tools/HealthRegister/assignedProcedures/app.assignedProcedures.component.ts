import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-assignedProcedures',
  templateUrl: './app.assignedProcedures.conponent.html',
})
export class AppAssignedProceduresConponent {

  @Input() procedures: any[];
  toAssign: boolean = false;

  constructor() { }

  goToAssignedWork() {
    this.toAssign = true;
  }

  cancelOperation() {
    this.toAssign = false;
  }

}
