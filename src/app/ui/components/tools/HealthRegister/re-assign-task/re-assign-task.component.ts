import {Component, Input, OnInit} from '@angular/core';
import {DATE_ES} from "../../../app.ui.environment";
import {ParameterDTO} from "../../../../../domain/model/dto/comunes/parameterDTO";


@Component({
  selector: 'app-re-assign-task',
  templateUrl: './re-assign-task.component.html'
})
export class ReAssignTaskComponent implements OnInit {
    @Input() groupLs: ParameterDTO[];
    @Input() subGroupLs: ParameterDTO[];
    @Input() procedureTypeLs: ParameterDTO[];
    @Input() rolLs: ParameterDTO[];
    es = DATE_ES;
    initialDate: Date;
    finalDate: Date;
    results: any[];
    official: string;
    reassignTo: string;
    proceduretypeSelect: ParameterDTO;
    groupSelect: ParameterDTO;
    subGroupSelect: ParameterDTO;
    rolSelect: ParameterDTO;
  constructor() { }

  ngOnInit(): void {
      this.results = [
          {group: '', subgroup: ''},
      ];
  }




}
