import {Component, Input, OnInit} from '@angular/core';
import {DATE_ES} from "../../../app.ui.environment";
import {ParameterDTO} from "../../../../../domain/model/dto/comunes/parameterDTO";



@Component({
  selector: 'app-pop-up-verify',
  templateUrl: './pop-up-verify.component.html'
})
export class PopUpVerifyComponent implements OnInit {
    @Input() complianceBpm: ParameterDTO[];
    es = DATE_ES;
    documentExpeditionDate: Date;
    validityDate: Date;
    expeditionDocumentDate: Date;
    lastInspectionDate: Date;
    cetifyExpeditionDate: Date;
    regulatoryAuditing: string;
    issuingCountry: string;
    regulatoryAuthority: string;
    documentNumber: number;
    complianceSelect: ParameterDTO;
    selectedVerifies: string[] = [];
    checked: boolean = false;
  constructor() { }

  ngOnInit(): void {
  }

}
