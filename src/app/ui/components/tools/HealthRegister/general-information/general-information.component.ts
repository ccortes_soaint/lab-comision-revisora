import {Component, Input, OnInit} from '@angular/core';
import {ParameterDTO} from "../../../../../domain/model/dto/comunes/parameterDTO";
import {DATE_ES} from "../../../app.ui.environment";

@Component({
  selector: 'app-general-information',
  templateUrl: './general-information.component.html'
})
export class GeneralInformationComponent implements OnInit {
    @Input() modalityLs: ParameterDTO[];
    @Input() procedureTypeLs: ParameterDTO[];
    @Input() subGroupLs: ParameterDTO[];
    @Input() productCategoryLs: ParameterDTO[];
    @Input() categoryProcedureLs: ParameterDTO[];
    es = DATE_ES;
    documentalVerificationDate: Date;
    radicateDate: Date;
    productType: string;
    group: string;
    procedureTypeSelect: ParameterDTO;
    modalitySelect: ParameterDTO;
    subGroupSelect: ParameterDTO;
    productCategorySelect: ParameterDTO;
    categoryProcedureSelect: ParameterDTO;
    documentsAssociated: any[];
    evaluation: any[];
  constructor() { }

  ngOnInit(): void {

      this.documentsAssociated = [
          {documentName: 'Clasificación del reactivo de diagnóstico In Vit', creationDate: '', invoice:'10' },
          {documentName: 'ETIQUETAS,  las cuales son las emitidas  ', creationDate: '', invoice:'3' },
      ];

      this.evaluation = [
          {itemToEvaluate: 'Verificar si el nombre del producto ya ha sido registrado anteriormente', compliance: ''},
          {itemToEvaluate: 'Verificar que no se radique el nombre de un producto cuyo registro se haya dejado vencer sin ser renovado, no podrá solicitar nuevo registro antes de haber transcurrido un año despues del registro', compliance: ''},
      ];
  }

}
