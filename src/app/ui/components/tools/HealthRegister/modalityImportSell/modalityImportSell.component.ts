import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-modality-import-sell',
    templateUrl: './modalityImportSell.component.html',
    styles: []
})
export class ModalityImportSellComponent implements OnInit {

    results: any[] = [];
    tablaFabricantesEnvases: any[] = [];
    tablaComposicion: any[] = [];
    tablaMateriasPrimas: any[] = [];

    constructor() { }

    ngOnInit() {
        this.results = [
            { evaluacion: 'Evaluación del certificacdo de venta libre| Cumplimiento' },
            { evaluacion: 'Es expedido por la autoridad sanitaria-nombre y país|' },
            { evaluacion: 'Debe incluir el principio activo del medicamento' },
            { evaluacion: 'Debe incluir la forma farmacéutica del medicamento' },
            { evaluacion: ' Debe incluir la concentración del producto' },
            { evaluacion: 'Debe incluir al titular del registro en el país origen' },
            { evaluacion: 'Debe incluir fabricante(s) y envasador(es) del medicamento según aplique' }
        ]

        this.tablaFabricantesEnvases = [
            { rol: 'Fabricante', direccion: 'xxxxx', acciones: '' },
            { rol: 'Envasador', direccion: 'xxxxx', acciones: '' },
            { rol: 'Acondicionador', direccion: 'xxxxx', acciones: '' }

        ]

        this.tablaComposicion = [
            { item: 'Nomenclatura IUPAC para todas las sustancias  (Exipientes minimo generico' },
            { item: 'Debe verificarse muy bien la forma qujimica como se reporta el reactivo (sal, base, acido, hidrato, etc' }
        ]
        this.tablaMateriasPrimas = [
            { itemEvaluar: 'Verificar las especificaciones de calidad de excipientes' },
            { itemEvaluar: 'Verificar las especificaciones de calidad de materiales de envase y empaque' },
            { itemEvaluar: 'Verificar las especificaciones de calidad de dispositivos médicos (si acompañan al producto)' },
            { itemEvaluar: 'Verificar las especificaciones de calidad de prinicpios activos' },

        ]
    }

}
