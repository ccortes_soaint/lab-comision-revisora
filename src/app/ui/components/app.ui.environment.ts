import { PagesPoliciesDTO } from 'src/app/domain/model/dto/profile/pagesPoliciesDTO';
import { UI_ROUTES_PATH } from './app.ui.routing.name';
import { AUDIT_PATH } from './pages/audit/app.audit.routing.name';
import { HEALTH_RECORD_PATH } from './pages/healthRecord/app.healthRecord.routing.name';
import { PROCEDURE_PATH } from './pages/procedure/app.procedure.routing.name';

export const ORDER_MENU: string[] = [
    UI_ROUTES_PATH.home,
    UI_ROUTES_PATH.procedure.value,
    UI_ROUTES_PATH.audit.value,
    UI_ROUTES_PATH.scheduling.value,
    UI_ROUTES_PATH.executionRoom.value,
    UI_ROUTES_PATH.inquiries.value,
    UI_ROUTES_PATH.healthRecord.value,
]

export const DEFAULT_POLICIES: PagesPoliciesDTO = {
    role: 'any',
    menuHeaderPass: [
        {
            nameCode: UI_ROUTES_PATH.home,
            route: 'Menú/' + UI_ROUTES_PATH.home,
            policiesStatus: true
        },
        {
            nameCode: UI_ROUTES_PATH.procedure.value,
            route: 'Menú/' + UI_ROUTES_PATH.procedure.value,
            policiesStatus: true
        },
        {
            nameCode: UI_ROUTES_PATH.healthRecord.value,
            route: 'Menú/' + UI_ROUTES_PATH.healthRecord.value,
            policiesStatus: true
        },
        {
            nameCode: UI_ROUTES_PATH.audit.value,
            route: 'Menú/' + UI_ROUTES_PATH.audit.value,
            policiesStatus: true
        },
        {
            nameCode: UI_ROUTES_PATH.scheduling.value,
            route: 'Menú/' + UI_ROUTES_PATH.scheduling.value,
            policiesStatus: true
        },
        {
            nameCode: UI_ROUTES_PATH.executionRoom.value,
            route: 'Menú/' + UI_ROUTES_PATH.executionRoom.value,
            policiesStatus: true
        },
        {
            nameCode: UI_ROUTES_PATH.inquiries.value,
            route: 'Menú/' + UI_ROUTES_PATH.inquiries.value,
            policiesStatus: true
        }
    ],
    subMenuHeaderPass: [
        {
            nameCode: PROCEDURE_PATH.new.value,
            route: '/' + UI_ROUTES_PATH.procedure.value + '/' + PROCEDURE_PATH.new.value,
            policiesStatus: true
        },
        {
            nameCode: PROCEDURE_PATH.view.value,
            route: '/' + UI_ROUTES_PATH.procedure.value + '/' + PROCEDURE_PATH.view.value,
            policiesStatus: true
        },
        {
            nameCode: PROCEDURE_PATH.generalInfo.value,
            route: '/' + UI_ROUTES_PATH.procedure.value + '/' + PROCEDURE_PATH.view.value + '/' + PROCEDURE_PATH.generalInfo.value,
            policiesStatus: true
        },
        {
            nameCode: PROCEDURE_PATH.generalRate.value,
            route: '/' + UI_ROUTES_PATH.procedure.value + '/' + PROCEDURE_PATH.view.value + '/' + PROCEDURE_PATH.generalRate.value,
            policiesStatus: true
        },
        {
            nameCode: PROCEDURE_PATH.legalInfo.value,
            route: '/' + UI_ROUTES_PATH.procedure.value + '/' + PROCEDURE_PATH.view.value + '/' + PROCEDURE_PATH.legalInfo.value,
            policiesStatus: true
        },
        {
            nameCode: PROCEDURE_PATH.technicalInfo.value,
            route: '/' + UI_ROUTES_PATH.procedure.value + '/' + PROCEDURE_PATH.view.value + '/' + PROCEDURE_PATH.technicalInfo.value,
            policiesStatus: true
        },
        {
            nameCode: PROCEDURE_PATH.paymentInfo.value,
            route: '/' + UI_ROUTES_PATH.procedure.value + '/' + PROCEDURE_PATH.view.value + '/' + PROCEDURE_PATH.paymentInfo.value,
            policiesStatus: true
        },
        {
            nameCode: HEALTH_RECORD_PATH.assignedWork.value,
            route: '/' + UI_ROUTES_PATH.healthRecord.value + '/' + HEALTH_RECORD_PATH.assignedWork.value,
            policiesStatus: true
        },
        {
            nameCode: HEALTH_RECORD_PATH.viewHealthRecord.value,
            route: '/' + UI_ROUTES_PATH.healthRecord.value + '/' + HEALTH_RECORD_PATH.viewHealthRecord.value,
            policiesStatus: true
        },
        {
            nameCode: HEALTH_RECORD_PATH.reassignTasks.value,
            route: '/' + UI_ROUTES_PATH.healthRecord.value + '/' + HEALTH_RECORD_PATH.reassignTasks.value,
            policiesStatus: true
        },
        {
            nameCode: HEALTH_RECORD_PATH.manageAssignedTask.value,
            route: '/' + UI_ROUTES_PATH.healthRecord.value + '/' + HEALTH_RECORD_PATH.manageAssignedTask.value,
            policiesStatus: true
        },
        {
            nameCode: AUDIT_PATH.reviewRequest.value,
            route: '/' + UI_ROUTES_PATH.audit.value + '/' + AUDIT_PATH.reviewRequest.value,
            policiesStatus: true
        },
        {
            nameCode: AUDIT_PATH.scheduleAudit.value,
            route: '/' + UI_ROUTES_PATH.audit.value + '/' + AUDIT_PATH.scheduleAudit.value,
            policiesStatus: true
        },
        {
            nameCode: AUDIT_PATH.assignOfficer.value,
            route: '/' + UI_ROUTES_PATH.audit.value + '/' + AUDIT_PATH.assignOfficer.value,
            policiesStatus: true
        },
        {
            nameCode: AUDIT_PATH.manageAudit.value,
            route: '/' + UI_ROUTES_PATH.audit.value + '/' + AUDIT_PATH.manageAudit.value,
            policiesStatus: true
        },
        {
            nameCode: AUDIT_PATH.consultActs.value,
            route: '/' + UI_ROUTES_PATH.audit.value + '/' + AUDIT_PATH.consultActs.value,
            policiesStatus: true
        },
        {
            nameCode: AUDIT_PATH.followUpVisits.value,
            route: '/' + UI_ROUTES_PATH.audit.value + '/' + AUDIT_PATH.followUpVisits.value,
            policiesStatus: true
        },
        {
            nameCode: PROCEDURE_PATH.attachDocuments.value,
            route: '/' + UI_ROUTES_PATH.procedure + '/' + PROCEDURE_PATH.view.value + '/' + PROCEDURE_PATH.attachDocuments.value,
            policiesStatus: true
        },
        {
            nameCode: 'Consul',
            route: '/' + UI_ROUTES_PATH.inquiries.value + '/inque' ,
            policiesStatus: true
        }
    ],
    routePath: null
};

export const DATE_ES = {
    firstDayOfWeek: 0,
    dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
    dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
    dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
    monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
    today: 'Hoy',
    clear: 'Limpiar',
    dateFormat: 'mm/dd/yy',
    weekHeader: 'Wk'
};
