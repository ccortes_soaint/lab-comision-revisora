import { DropdownModule } from 'primeng/dropdown';
import { FormGroup } from '@angular/forms';
import { FormBuilder, FormControl } from '@angular/forms';
import { Component, Input, OnInit } from '@angular/core';
import { SelectItem } from 'primeng';
import { SettledService } from 'src/app/infraestructure/services/inquiries/settled.service';

interface City {
  name: string;
  code: string;
}

@Component({
  selector: 'app-select-room',
  templateUrl: './app.select-room.component.html',
  styleUrls: ['./app.select-room.component.css']
})

export class AppSelectRoomComponent implements OnInit {

  @Input() type;
  fgSelectRoom: FormGroup;

  Lista: SelectItem[];
  selected;

  handleClick() {

  }

  constructor(private formBuilder: FormBuilder, private catalogs: SettledService) {

    this.catalogs.getRooms()
    .subscribe(
      (data: any) => {
        console.log(data);       
        data.objectResponse.map((p, i) => {
          // crear nueva propiedad de nombre producto{i + 1}
          p[`label`] = `${p.NombreCorto}: ${p.Nombre}`;
          p[`value`] = p.Id;

          // remover la propiedad actual
          delete p.Dependencia;
          delete p.Nombre;
          delete p.NombreCorto;
          delete p.Id;

          // retornar el nuevo objeto
          return p;
        });

        let valueToPush = new Array();
        valueToPush[`label`] = '...';
        valueToPush[`value`] = 0;
        data.objectResponse.unshift(valueToPush);

        this.Lista = data.objectResponse;
      },
      err => {
        console.log("Fallo en la consulta de informacion de consulta");
      }
    );

    // this.Lista = [
    //   { label: '', value: null },
    //   { label: 'SEM:Sala Especializada de Medica', value: { id: 1, name: '', code: '' } },
    // ];

  }


  ngOnInit(): void {

    console.log(this.type);

    this.fgSelectRoom = this.initFormGroupSelectRoom();

    this.fgSelectRoom.valueChanges.subscribe(val => {
      console.log(val)
    });
  }

  initFormGroupSelectRoom(){
    return this.formBuilder.group({
      roomName:[''],
    })
  }
}