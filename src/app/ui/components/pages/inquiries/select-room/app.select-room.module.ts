import { ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PRIMENG_MODULES } from 'src/app/shared/enums/primeng.elements';
import { AppSelectRoomComponent } from './app.select-room.component';

@NgModule({
    declarations: [AppSelectRoomComponent],
    imports: [
        CommonModule,
        ...PRIMENG_MODULES,
        ReactiveFormsModule
      ],
      exports: [AppSelectRoomComponent]
    })
    export class AppSelectRoomModule { }