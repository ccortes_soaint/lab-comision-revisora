import { ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppFilingDataComponent } from './app.filing-data.component';
import { PRIMENG_MODULES } from 'src/app/shared/enums/primeng.elements';

@NgModule({
    declarations: [AppFilingDataComponent],
    imports: [
        CommonModule,
        ...PRIMENG_MODULES,
        ReactiveFormsModule
      ],
      exports: [AppFilingDataComponent]
    })
    export class AppFilingDataModule { }