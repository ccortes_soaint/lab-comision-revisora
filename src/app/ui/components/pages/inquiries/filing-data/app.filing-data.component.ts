import { FormGroup } from '@angular/forms';
import { FormBuilder, FormControl } from '@angular/forms';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-filing-data',
  templateUrl: './app.filing-data.component.html',
  styleUrls: ['./app.filing-data.component.css']
})
export class AppFilingDataComponent implements OnInit {

  @Input() type; 

  fgFilingData: FormGroup;


  constructor(private formBuilder:FormBuilder) {
    
   }

  ngOnInit(): void {
    // console.log(this.type);

    this.fgFilingData = this.initFormGroupFilingData();

    this.fgFilingData.valueChanges.subscribe(val => {
      console.log(val)
    });


  }

  initFormGroupFilingData(){
    return this.formBuilder.group({
      filedNumber:[''],
      proceedings:[''],
      Headline:[''],
      composition:[''],
      indications:[''],
      precautions:[''],
      filingDate:[''],
      productName:[''],
      activePriciple:[''],
      pharamecuenticalForm:[''],
      contraindications:[''],
    })
  }
  
}
