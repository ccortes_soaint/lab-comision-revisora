import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-popup-request-form',
  templateUrl: './app.popup-request-form.component.html',
  styleUrls: ['./app.popup-request-form.component.css']
})
export class AppPopupRequestFormComponent implements OnInit {
  options: any = [];
  display: boolean = false;

  constructor() {
    this.options = [
      { label: '...', value: null },
      { label: 'Consultas', value: { id: 1, name: 'consultas' } },
      { label: 'Aclaraciones', value: { id: 2, name: 'aclaraciones' } },
      { label: 'Unificaciones', value: { id: 3, name: 'unificaciones' } },
    ];
  }

  ngOnInit(): void {
  }

  showDialog() {
    this.display = true;
  }

  handleClick(event) {

  }
}
