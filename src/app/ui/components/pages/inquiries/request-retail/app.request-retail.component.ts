import { FormGroup } from '@angular/forms';
import { FormBuilder, FormControl } from '@angular/forms';
import { Component, Input, OnInit} from '@angular/core';
import { SelectItem } from 'primeng';




interface City {
  name: string;
  code: string;
}

@Component({
  selector: 'app-request-retail',
  templateUrl: './app.request-retail.component.html',
  styleUrls: ['./app.request-retail.component.css']
})
export class AppRequestRetailComponent implements OnInit {
  @Input() type;

  fgRequest: FormGroup;


  Lista: SelectItem[];
  Fecha: { name: string }[];
  NumeroActa: { name: string }[];
  Numeral: { name: string }[];
  selected;


  handleClick() {

  }

  constructor(private formBuilder:FormBuilder) {

    this.Lista = [
      { label: ' Seleccione', value: null },
      { label: 'SEM', value: { id: 1, name: '', code: '' } },
      { label: 'SEMNNIMB ', value: { id: 1, name: '', code: '' } },
      { label: 'SEMH ', value: { id: 1, name: '', code: '' } },
      { label: 'SEPFSD', value: { id: 1, name: '', code: '' } },
    ]
  };

  ngOnInit(): void { 
    this.fgRequest = this.initFormGroupRequest();

    this.Fecha = [
      { name: 'Seleccione' },
      { name: '2021-01' },
      { name: '2021-02' },
      { name: '2021-03' },
      { name: '2021-04' },
      { name: '2021-05' },
      { name: '2021-06' },
      { name: '2021-07' },
      { name: '2021-08' },
      { name: '2021-09' },
      { name: '2021-10' },
      { name: '2021-11' },
      { name: '2021-12' },
    ]



    this.NumeroActa = [
      { name: 'Seleccione' },
      { name: '2021-01' },
      { name: '2021-03' },
      { name: '2021-04' },
      { name: '2021-05' },
      { name: '2021-06' },
      { name: '2021-07' },
      { name: '2021-08' },
      { name: '2021-09' },
      { name: '2021-10' },
      { name: '2021-11' },
      { name: '2021-12' },
      { name: '2021-13' },
      { name: '2021-14' },
      { name: '2021-15' },
      { name: '2021-16' },
      { name: '2021-17' },
      { name: '2021-18' },
      { name: '2021-19' },
      { name: '2021-20' },
      { name: '2021-21' },
      { name: '2021-22' },
      { name: '2021-23' },
      { name: '2021-24' },

    ]


    this.Numeral = [
      { name: 'Seleccione' },
      { name: '3.1.1 '},
      { name: '3.1.2 '},
      { name: '3.1.2 '},
    ]

    this.fgRequest = this.initFormGroupRequest();

    this.fgRequest.valueChanges.subscribe(val => {
      console.log(val)
    });
  
  }
  
  initFormGroupRequest(){
    return this.formBuilder.group({
      room:[''],
      actNumber:[''],
      actDate:[''],
      numeral:['']
    })
  
  }
    
}


