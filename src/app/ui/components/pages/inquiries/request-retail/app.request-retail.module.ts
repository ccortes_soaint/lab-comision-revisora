import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppRequestRetailComponent } from './app.request-retail.component';
import { PRIMENG_MODULES } from 'src/app/shared/enums/primeng.elements';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [AppRequestRetailComponent],
  imports: [
    CommonModule,
    ...PRIMENG_MODULES,
    ReactiveFormsModule
  ],
  exports: [AppRequestRetailComponent]
})
export class AppRequestRetailModule { }