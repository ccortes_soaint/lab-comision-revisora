import { AppFilingDataComponent } from './filing-data/app.filing-data.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppInquiriesRoutingModule } from './app.inquiries.routing';
import { PRIMENG_MODULES } from 'src/app/shared/enums/primeng.elements';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppAttachDocumentsModule } from './attach-documents/app.attach-documents.module';
import { AppInformationGeneralComponent } from './information-general/app.information-general.component';
import { AppConsultationSettledRelatedComponent } from './consultation-settled-related/app.consultation-settled-related.component';
import { AppDetailsRequestComponent } from './details-request/app.details-request.component';
import { AppPopupSendRequestComponent } from './popup-send-request/app.popup-send-request.component';
import { AppPopupRequestRoomComponent } from './popup-request-room/app.popup-request-room.component';
import { AppInquiriesComponent } from './app.inquiries.component';
import { AppPopupRequestFormComponent } from './popup-request-form/app.popup-request-form.component';
import { AppRequestRetailComponent } from './request-retail/app.request-retail.component';
import { AppPrejudiceComponent } from './prejudice/app.prejudice.component';
import { AppFilingDataModule } from './filing-data/app.filing-data.module';
import { AppAttachDocumentsComponent } from './attach-documents/app.attach-documents.component';
import { ListInputsModule } from '../unification/shared/list-inputs/list-inputs.module';
import { AppSelectRoomModule } from './select-room/app.select-room.module';
import { AppInformationGeneralModule } from './information-general/app.information-general.module';
import { AppRequestRetailModule } from './request-retail/app.request-retail.module';
import { AppLinksAttachDocumentsComponent } from './links-attach-documents/app.links-attach-documents.component';

@NgModule({
  declarations: [
    AppInquiriesComponent, 
    AppConsultationSettledRelatedComponent,
    AppDetailsRequestComponent,
    AppPopupSendRequestComponent,
    AppPopupRequestRoomComponent,
    AppPopupRequestFormComponent,
    AppPrejudiceComponent,
    AppLinksAttachDocumentsComponent,
  ],
  imports: [
    CommonModule,
    AppInquiriesRoutingModule,
    ...PRIMENG_MODULES,
    FormsModule,
    ReactiveFormsModule,
    AppAttachDocumentsModule,
    AppFilingDataModule,
    ListInputsModule,
    AppSelectRoomModule,
    AppInformationGeneralModule,
    AppRequestRetailModule
  ],
  providers:[],
  exports: [
    AppInquiriesComponent, 
    AppAttachDocumentsComponent, 
    AppPrejudiceComponent,
    AppDetailsRequestComponent, 
    AppInformationGeneralComponent, 
    AppFilingDataComponent,
    AppRequestRetailComponent
  ]
})
export class AppInquiriesModule { }