import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ConfirmationService } from 'primeng';

export interface linkConsulting {
  nombreDocumento?: string;
}

@Component({
  selector: 'app-links-attach-documents',
  templateUrl: './app.links-attach-documents.component.html',
  styleUrls: ['./app.links-attach-documents.component.css']
})

export class AppLinksAttachDocumentsComponent implements OnInit {

  @Input() type:string;

  fgRequestDetail: FormGroup;

  displayDialog: boolean;
  link: linkConsulting = {};
  selectedLink: linkConsulting;
  newLink: boolean = true;
  links: linkConsulting[];
  cols: any[];
  inputLink: string = null;

  constructor(private confirmationService: ConfirmationService, private formBuilder: FormBuilder) { }

  ngOnInit() {

    this.links = [
      { nombreDocumento: 'https://upload.wikimedia.org/wikipedia/commons/thumb/8/87/PDF_file_icon.svg/1200px-PDF_file_icon.svg.png' },
    ];

    this.cols = [
      { field: 'nombreDocumento', header: 'Nombre Documento' },
      { field: 'acciones', header: 'Acciones' }
    ];

    // console.log(this.type);

    // this.fgRequestDetail = this.initFormGroupRequestDetail();

    // this.fgRequestDetail.valueChanges.subscribe(val => {
    //   console.log(val)
    // });
  }

  // initFormGroupRequestDetail(){
  //   return this.formBuilder.group({
  //     consultation:[''],
  //   })
  
  // }

  showDialogToAdd() {
    this.link = {};
    this.displayDialog = true;
  }

  saveLink() {
    console.log(this.inputLink);
    const arrayLink = {
      nombreDocumento: this.inputLink
    };

    let links = [...this.links];
    if (this.newLink)
      links.push(arrayLink);
    else
      links[this.links.indexOf(this.selectedLink)] = this.link;

    this.links = links;
    this.inputLink = null;
    // this.displayDialog = false;
  }

  updateLink() {
    let links = [...this.links];
    links[this.links.indexOf(this.selectedLink)] = this.link;

    this.links = links;
    this.displayDialog = false;
  }

  deleteLink(req: linkConsulting) {
    let index = this.links.indexOf(req);
    this.links = this.links.filter((val, i) => i != index);
    this.link = null;
    this.displayDialog = false;
  }

  confirm(req: linkConsulting) {
    this.confirmationService.confirm({
      message: '¿Esta seguro que desea eliminar la información?',
      accept: () => {
        this.deleteLink(req);
      }
    });
  }

  selectLink(req: linkConsulting) {
    this.link = this.cloneLink(req);
    this.displayDialog = true;
  }

  cloneLink(c: linkConsulting): linkConsulting {
    let link = {};
    for (let prop in c) {
      link[prop] = c[prop];
    }
    return link;
  }

}
