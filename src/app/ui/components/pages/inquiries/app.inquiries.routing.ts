import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'src/app/infraestructure/services/auth/authGuard.service';
import { UI_ROUTES_PATH } from '../../app.ui.routing.name';
import { AppInquiriesComponent } from './app.inquiries.component';

const routes: Routes = [
    { path: '', redirectTo: UI_ROUTES_PATH.inquiries.value, pathMatch: 'full' },
    {
        path: '',
        component: AppInquiriesComponent,
        canActivateChild: [AuthGuardService],
        data: {
            breadcrumb: UI_ROUTES_PATH.inquiries.name
        },
        children:[]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class AppInquiriesRoutingModule {
}