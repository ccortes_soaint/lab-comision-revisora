import { Component, Input, OnInit } from '@angular/core';
import { ConfirmationService } from 'primeng/api';
import { FormGroup } from '@angular/forms';
import { FormBuilder, FormControl } from '@angular/forms';
// import { requirementConsultingDTO } from 'src/app/domain/model/dto/comunes/requirementConsultingDTO';

export interface requirementConsulting {
  origen?: string;
  funcionario?: string;
  fecha?: string;
  requerimiento?: string;
}

@Component({
  selector: 'app-details-request',
  templateUrl: './app.details-request.component.html',
  styleUrls: ['./app.details-request.component.css']
})

export class AppDetailsRequestComponent implements OnInit {

  @Input() type:string;

  fgRequestDetail: FormGroup;

  displayDialog: boolean;
  requirement: requirementConsulting = {};
  selectedRequirement: requirementConsulting;
  newRequirement: boolean = true;
  requirements: requirementConsulting[];
  cols: any[];
  descriptionRequirement: string = null;

  constructor(private confirmationService: ConfirmationService, private formBuilder:FormBuilder) { }

  ngOnInit() {

    this.requirements = [
      { origen: 'Direccion de registro', funcionario: 'Pedro Perez', fecha: '08-03-2021', requerimiento: 'Se solicita validar si es posible suministrar el acetominofen en dosis de 500 mgr 5 veces al dia para pacientes de rango de edad 20 años' },
    ];

    this.cols = [
      { field: 'origen', header: 'Origen' },
      { field: 'funcionario', header: 'Funcionario' },
      { field: 'fecha', header: 'Fecha' },
      { field: 'requerimiento', header: 'Requerimiento consulta' },
      { field: 'acciones', header: 'Acciones' }
    ];

    // console.log(this.type);

    // this.fgRequestDetail = this.initFormGroupRequestDetail();

    // this.fgRequestDetail.valueChanges.subscribe(val => {
    //   console.log(val)
    // });
  }

  // initFormGroupRequestDetail(){
  //   return this.formBuilder.group({
  //     consultation:[''],
  //   })
  
  // }

  showDialogToAdd() {
    this.requirement = {};
    this.displayDialog = true;
  }

  saveRequirement() {
    const arrayRequirement = {
      origen: '',
      funcionario: '',
      fecha: '',
      requerimiento: this.descriptionRequirement
    };

    let requirements = [...this.requirements];
    if (this.newRequirement)
      requirements.push(arrayRequirement);
    else
      requirements[this.requirements.indexOf(this.selectedRequirement)] = this.requirement;

    this.requirements = requirements;
    this.descriptionRequirement = null;
    // this.displayDialog = false;
  }

  updateRequirement() {
    let requirements = [...this.requirements];
    requirements[this.requirements.indexOf(this.selectedRequirement)] = this.requirement;

    this.requirements = requirements;
    this.displayDialog = false;
  }

  deleteRequirement(req: requirementConsulting) {
    let index = this.requirements.indexOf(req);
    this.requirements = this.requirements.filter((val, i) => i != index);
    this.requirement = null;
    this.displayDialog = false;
  }

  confirm(req: requirementConsulting) {
    this.confirmationService.confirm({
      message: '¿Esta seguro que desea eliminar la información?',
      accept: () => {
        this.deleteRequirement(req);
      }
    });
  }

  selectRequirement(req: requirementConsulting) {
    this.requirement = this.cloneRequirement(req);
    this.displayDialog = true;
  }

  cloneRequirement(c: requirementConsulting): requirementConsulting {
    let requirement = {};
    for (let prop in c) {
      requirement[prop] = c[prop];
    }
    return requirement;
  }
}
