import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Inquiries } from 'src/app/domain/model/dto/inquiries/inquiries.model';
import { SettledService } from 'src/app/infraestructure/services/inquiries/settled.service';
import { AppInformationGeneralComponent } from './information-general/app.information-general.component';

enum QueryParams {
  OPTION1 = 'anx3',
  OPTION2 = 'anx4',
  OPTION3 = 'other'
}

@Component({
  selector: 'app-inquiries',
  templateUrl: './app.inquiries.component.html',
  styleUrls: ['./app.inquiries.component.css']
})

export class AppInquiriesComponent implements OnInit {

  visibility = true;
  visibilityPopup = false;
  submitted: boolean;
  // displayParentPopup: boolean = false;
  display: boolean = false;
  type: string;
  query = QueryParams;
  listInputs;
  radicadoEj: string;
  inquiries: Inquiries;
  form: FormGroup;

  // @ViewChild(ChildComponent) childComponent: AppInformationGeneralComponent;
  // @ViewChild("child2Component") child2: any;
  // @ViewChild("child3Component") child3: any;

  constructor(private route: ActivatedRoute, private settled: SettledService, private formBuilder: FormBuilder) {
    this.route.queryParams.subscribe(params => {
      this.type = params['type'] || this.query.OPTION1;
      // this.initVariablesByOption(this.type)
    });

    this.form = formBuilder.group({
      childForm1: '',
      // childForm1: this.formBuilder.group({
      //   input1: [null, Validators.required]
      // })
    });

    // this.inquiriesForm = this.formBuilder.group({
    //   infGral: new FormControl('', Validators.required),
      // tIdentidad: ['', Validators.required],
      // cedula: ['', Validators.required],
      // pNombre: ['', Validators.required],
      // sNombre: [''],
      // pApellido: ['', Validators.required],
      // sApellido: [''],
      // cPrincipal: ['', [Validators.email, Validators.required]],
      // cSecundario: ['', Validators.email],
      // pSecreta: ['', Validators.required],
      // rSecreta: ['', Validators.required],
      // cPass: ['', Validators.required],
      // cPassR: ['', Validators.required],
    // });
  }

  ngOnInit(): void {
    this.radicadoEj = 'N00018235';
    this.settled.getSettledId(this.radicadoEj)
      .subscribe(
        (data: any) => {
          // console.log(data);
        },
        err => {
          console.log("Fallo en la consulta del radicado");
        }
      );

    // this.settled.getRooms()
    //   .subscribe(
    //     (data: any) => {
    //       // console.log(data);
    //     },
    //     err => {
    //       console.log("Fallo en la consulta de las salas");
    //     }
    //   );

    // this.settled.getTypeRequest()
    //   .subscribe(
    //     (data: any) => {
    //       // console.log(data);
    //     },
    //     err => {
    //       console.log("Fallo en la consulta de tipo de Solicitud");
    //     }
    //   );

    // this.settled.getInformationGeneral()
    // .subscribe(
    //   (data: any) => {
    //     console.log(data);
    //   },
    //   err => {
    //     console.log("Fallo en la consulta de informacion de consulta");
    //   }
    // );
  }

  // get form(){
  //   return this.inquiriesForm.controls;
  // }

  get subform1(): FormGroup {
    return this.form.get('subform1') as FormGroup;
  }
  onSubmit(){

    // console.log(this.child1.form.value);
    // console.log(this.child2.form.value);
    // console.log(this.child3.form.value);

    // this.submitted = true;
    
    // if(this.inquiriesForm.invalid){
    //   return;
    // }

    // alert("Success" + JSON.stringify(this.inquiriesForm.value, null, 4));
  }

  onReset(){
    this.submitted = false;
    // this.inquiriesForm.reset();
  }

  // initVariablesByOption(opt: string) {
  //   this.listInputs = this.initListInputsGeneralInfo(opt);
  // }

  // initListInputsGeneralInfo(opt: string) {
  //   if (opt == this.query.OPTION1) {
  //     return [
  //       { id: "rad1", type: 'text', name: "Radicado", disabled: true, value: "202012345" },
  //       { id: "rad2", type: 'datetime-local', name: "Fecha solicitud", disabled: true, value: '2021-03-19T13:33' },
  //       { id: "rad3", type: 'text', name: "Remitente", disabled: true, value: "Andrea Quintana - ciudadano" },
  //     ]
  //   }
  //   if (opt == this.query.OPTION2) {
  //     return [
  //       { id: "rad1", type: 'text', name: "Radicado relacionado", disabled: true, value: "xxxxxxxxx" },
  //       { id: "rad2", type: 'date', name: "Fecha radicado relacionado", disabled: true, value: "xxxxxxxxx" },
  //       { id: "rad3", type: 'text', name: "Nombre del producto", disabled: true, value: "xxxxxxxxx" },
  //       { id: "rad4", type: 'text', name: "Principio activo", disabled: true, value: "xxxxxxxxx" },
  //       { id: "rad5", type: 'text', name: "Composicion", disabled: true, value: "" },
  //       { id: "rad6", type: 'text', name: "Expediente", disabled: true, value: "xxxxxxxxx" },
  //     ]
  //   }
  // }

  showDialog() {
    // this.displayParentPopup = true;
    this.display = true;
  }

}
