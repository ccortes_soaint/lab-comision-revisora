import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppAttachDocumentsComponent } from './app.attach-documents.component';
import { PRIMENG_MODULES } from 'src/app/shared/enums/primeng.elements';
import { PdfViewerModule } from 'ng2-pdf-viewer';

@NgModule({
  declarations: [AppAttachDocumentsComponent],
  imports: [
    CommonModule,
    ...PRIMENG_MODULES,
    PdfViewerModule
  ],
  exports: [AppAttachDocumentsComponent]
})
export class AppAttachDocumentsModule { }

