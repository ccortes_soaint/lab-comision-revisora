import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ConfirmationService, MessageService } from 'primeng';

export class pFileUploadDTO {
  name?: string;
  date?: Date;
  file?: any;
  active?: boolean = false;
}

@Component({
  selector: 'app-attach-documents',
  templateUrl: './app.attach-documents.component.html',
  styleUrls: ['./app.attach-documents.component.css']
})

export class AppAttachDocumentsComponent implements OnInit {
  documentType: any;
  form: FormGroup;

  head: any;
  resultsDocumentation: Array<pFileUploadDTO> = [];
  nameAttach: any;
  listaVersionesDocumento: any[] = [];
  pdfFileBase;
  uploadedFiles;
  date: any;
  previewVisible: boolean = false;
  src: any;
  archivo: any;

  constructor(private formBuilder: FormBuilder, private messageService: MessageService, private confirmationService: ConfirmationService) {  }

  ngOnInit(): void {
  }

  // Funcion Eliminar documento
  delete(index: number) {
    this.confirmationService.confirm({
      message: '¿Esta seguro que desea eliminar la informacion?',
      header: 'Confirmacion eliminar',
      icon: 'pi pi-info-circle',
      accept: () => {
        this.resultsDocumentation.splice(index, 1);
        // this.messageService.add({
        //   severity: 'success',
        //   summary: 'Exitoso',
        //   detail: 'Eliminado exítosamente',
        //   closable: true
        // });
      }
    });
  }

  // Funcion Visualizar documento
  viewPdf(fileDocument) {
    this.archivo = fileDocument.file;
    this.previewVisible = true;
    // console.log(this.uploadedFiles)
    /* this.file = this.uploadedFiles[0].file;*/
  }

  // Funcion Adjuntar documento
  toAttach(event, fileUpload) {
    if (event.files.length === 0) {
      return false;
    }
    this.uploadedFiles = [
      {
        id: this.listaVersionesDocumento.length > 0 ? this.listaVersionesDocumento[this.listaVersionesDocumento.length - 1].id : null,
        nombre: encodeURI(event.files[0].name),
        tipo: 'pdf',
        version: '',
        contenido: '',
        file: event.files[0],
        size: event.files[0].size,
        date: new Date()
      },

    ];
    // Convertir file a base 64 para poder ser vizualizado
    const file = event.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      let document = new pFileUploadDTO();
      document.name = this.uploadedFiles[0].file.name;
      document.date = this.uploadedFiles[0].date;
      document.file = reader.result;

      this.resultsDocumentation.push(document)
      //console.log("Tabla: " + JSON.stringify(this.resultsDocumentation));
      /* console.log("Archivo: " + reader.result);*/
      this.nameAttach = this.uploadedFiles[0].file.name;
      this.date = this.uploadedFiles[0].date;

    };

    fileUpload.clear();

    this.messageService.add({
      severity: 'success',
      summary: 'Exitoso',
      detail: 'Adjuntado exítosamente',
      closable: true
    });
  }
}
