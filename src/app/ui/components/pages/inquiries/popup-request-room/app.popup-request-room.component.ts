import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-popup-request-room',
  templateUrl: './app.popup-request-room.component.html',
  styleUrls: ['./app.popup-request-room.component.css']
})
export class AppPopupRequestRoomComponent implements OnInit {
  options: any = [];
  display: boolean = false;

  constructor() {
    this.options = [
      { label: '...', value: null },
      { label: 'Consultas', value: { id: 1, name: 'consultas' } },
      { label: 'Aclaraciones', value: { id: 2, name: 'aclaraciones' } },
      { label: 'Unificaciones', value: { id: 3, name: 'unificaciones' } },
      { label: 'Solicitud de audiencia', value: { id: 3, name: 'solicitud' } },
      { label: 'Vitales no disponibles', value: { id: 3, name: 'vitales' } },
    ];
  }

  ngOnInit(): void {
  }

  showDialog() {
    this.display = true;
  }

  handleClick(event) {

  }
}
