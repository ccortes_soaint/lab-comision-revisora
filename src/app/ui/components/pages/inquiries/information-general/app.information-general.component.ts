
import { DropdownModule } from 'primeng/dropdown';
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { SelectItem } from 'primeng';
import { SettledService } from 'src/app/infraestructure/services/inquiries/settled.service';

interface list {
  id: number;
  name: string;
  code: string;
}

@Component({
  selector: 'app-information-general',
  templateUrl: './app.information-general.component.html',
  styleUrls: ['./app.information-general.component.css']
})

export class AppInformationGeneralComponent implements OnInit {

  @Input() type;
  fgInforGeneral: FormGroup;
  lista: SelectItem[];
  seleccionado: list;
  nombre: string;
  // @Input() inquiriesForm: FormGroup;
  // @Input() label: string;

  @Input() form: FormGroup;

  // @ViewChild("child1Form") form: any;

  constructor(private catalogs: SettledService, private formBuilder: FormBuilder) {
    this.catalogs.getInformationGeneral()
    .subscribe(
      (data: any) => {
        console.log(data);       
        data.objectResponse.map((p, i) => {
          // crear nueva propiedad de nombre producto{i + 1}
          p[`label`] = p.Descripcion;
          p[`value`] = p.Id;

          // remover la propiedad actual
          delete p.Descripcion;
          delete p.Codigo;
          delete p.Id;

          // retornar el nuevo objeto
          return p;
        });

        let valueToPush = new Array();
        valueToPush[`label`] = '...';
        valueToPush[`value`] = 0;
        valueToPush[`disabled`] = true;
        data.objectResponse.unshift(valueToPush);

        this.lista = data.objectResponse;
      },
      err => {
        console.log("Fallo en la consulta de informacion de consulta");
      }
    );

    // this.lista = [
    //   { label: '...', value: null },
    //   { label: 'Informacion decreto 2085 de 2002 nuevas entidades quimicas en el area de medicamentos', value: 'Informacion sobre el decreto 2085 de 2002' },
    //   { label: 'Solucion de inclusion o exclusion de vitales no disponibles', value: 'Solucion de inclusion' },
    // ];

  }

    
  initFormGroupInforGeneral() {
    // return this.formBuilder.group({
    //   Lista: ['']
    // })
  }

  ngOnInit(): void {
    // console.log(this.type);

    // this.fgInforGeneral = this.initFormGroupInforGeneral();

    // this.fgInforGeneral.valueChanges.subscribe(val => {
    //   console.log(val)
    // });
  }   
}






