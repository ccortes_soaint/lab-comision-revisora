import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppInformationGeneralComponent } from './app.information-general.component';
import { PRIMENG_MODULES } from 'src/app/shared/enums/primeng.elements';

@NgModule({
  declarations: [
    AppInformationGeneralComponent
  ],
  imports: [
    CommonModule,
    ...PRIMENG_MODULES,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    AppInformationGeneralComponent
  ]
})
export class AppInformationGeneralModule { }