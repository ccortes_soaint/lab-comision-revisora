import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AttachDocumentModalComponent } from './attach-document-modal.component';
import { TableAttachDocumentModule } from '../shared/table-attach-document/table-attach-document.module';
import { PRIMENG_MODULES } from 'src/app/shared/enums/primeng.elements';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PdfViewerModule } from 'ng2-pdf-viewer';



@NgModule({
  declarations: [AttachDocumentModalComponent],
  imports: [
    CommonModule,
    TableAttachDocumentModule,
    ...PRIMENG_MODULES,
    FormsModule,
    ReactiveFormsModule,
    PdfViewerModule
  ],
  exports: [AttachDocumentModalComponent]
})
export class AttachDocumentModalModule { }