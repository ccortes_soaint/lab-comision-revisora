import { formatDate } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SchedulingService } from 'src/app/infraestructure/services/scheduling.service';


@Component({
  selector: 'app-attach-document-modal',
  templateUrl: './attach-document-modal.component.html',
  styleUrls: ['./attach-document-modal.component.css']
})
export class AttachDocumentModalComponent implements OnInit {

  fgSearch:FormGroup;

  columnsTable: { field: string; header: string; }[];
  valuesTable: { name: string; date: string; }[];
  listActions: { icon: string; action: (e: any) => void; }[] = [];
  listSchedulingMonth: { id: number; Descripcion: any; }[];

  viewPdfDocument:boolean=false;
  archivo: any;

  constructor(
    private formBuilder:FormBuilder,
    public schedulingService:SchedulingService
  ) { }

  ngOnInit(): void {
    this.fgSearch = this.initFormGroupSearch();
    this.initTableVariables();

    let fcRoom = this.fgSearch.get('room');
    fcRoom.valueChanges.subscribe(room => {
      this.schedulingService.fetchListRoomScheduling(room.Id).then(listRoomScheduling => {
        this.listSchedulingMonth = listRoomScheduling.map((roomScheduling, index) =>{
          return {id:index,Descripcion:roomScheduling.monthAgenda};
        });
      })
    })   
  }

  buttonEvent(){
    console.log(this.fgSearch.value)
  }

  initFormGroupSearch(){
    return this.formBuilder.group({
      room:['', Validators.required],
      schedulingMonth:['', Validators.required],
      file:['']
    })
  }

  initTableVariables(){
    this.columnsTable = this.initColumnsTable();
    this.valuesTable = this.initValuesTable();
    this.listActions = this.initListActions();
  } 

  initColumnsTable(){
    return [
      { field: 'name', header: 'Nombre documento' },
      { field: 'date', header: 'fecha' },
    ];
  }
  initValuesTable(){
    return [];
  }
  initListActions(){
    return [
      { icon: 'remove_red_eye', action: this.viewPdf.bind(this) },
      { icon: 'delete', action: this.removeItem.bind(this) },
    ];
  }

  viewPdf(fileDocument){
    this.archivo = fileDocument.file;
    this.viewPdfDocument = true;
  }

  removeItem(){
    this.valuesTable.length = 0;
  }

  toAttachDocument(upload){
    upload.files[0].date = formatDate(new Date(), 'dd MMMM y h:mm:ss', 'en-US');
    this.valuesTable.push(upload.files[0]);

    this.fgSearch.patchValue({file:this.valuesTable})

    // Get Base64 file
    if (typeof (FileReader) !== 'undefined') {
      let reader = new FileReader();
      reader.onload = (e: any) => {
        upload.files[0].file = e.target.result;
      };
      reader.readAsArrayBuffer(upload.files[0]);
    }
  }

}
