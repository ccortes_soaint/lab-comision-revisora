import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { SchedulingService } from 'src/app/infraestructure/services/scheduling.service';

@Component({
  selector: 'app-view-agenda',
  templateUrl: './view-agenda.component.html',
  styleUrls: ['./view-agenda.component.css']
})
export class ViewAgendaComponent implements OnInit {

  fgSearchScheduling:FormGroup;

  valuesTable;
  columnsTable;
  listActions = [];

  constructor(
    private formBuilder:FormBuilder,
    public schedulingService:SchedulingService
  ) { }

  ngOnInit(): void {
    this.fgSearchScheduling = this.initFormGroupSearch();
    this.initTableVariables()
  }

  initFormGroupSearch(){
    return this.formBuilder.group({
      room:[''],
      schedulingCut:[''],
      schedulingMonth:['']
    })
  }

  initTableVariables(){
    this.columnsTable = this.initColumnsTable();
    this.valuesTable = this.initValuesTable();
    this.listActions = this.initListActions();
  } 

  initColumnsTable(){
    return [
      { field: 'name', header: 'Nombre documento' },
      { field: 'date', header: 'Corte' },
    ];
  }
  initValuesTable(){
    return [
      { name: 'Agenda_01_mayo_2021', date: '01-01 marzo 2021 11:02:26' },
      { name: 'Agenda_02_mayo_2021', date: '02-10 marzo 2021 14:09:31' },
      { name: 'Agenda_03_mayo_2021', date: '03-28 marzo 2021 16:07:02' },
    ];
  }
  initListActions(){
    return [
      { icon: 'remove_red_eye', action: this.callback },
    ];
  }

  callback(e){
    console.log(e)
  }


}
