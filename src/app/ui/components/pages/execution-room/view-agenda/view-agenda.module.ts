import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewAgendaComponent } from './view-agenda.component';
import { TableAttachDocumentModule } from '../shared/table-attach-document/table-attach-document.module';
import { PRIMENG_MODULES } from 'src/app/shared/enums/primeng.elements';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [ViewAgendaComponent],
  imports: [
    CommonModule,
    TableAttachDocumentModule,
    ...PRIMENG_MODULES,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [ViewAgendaComponent]
})
export class ViewAgendaModule { }
