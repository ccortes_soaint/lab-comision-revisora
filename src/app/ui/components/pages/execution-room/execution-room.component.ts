import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { SchedulingService } from 'src/app/infraestructure/services/scheduling.service';

@Component({
  selector: 'app-execution-room',
  templateUrl: './execution-room.component.html',
  styleUrls: ['./execution-room.component.css']
})
export class ExecutionRoomComponent implements OnInit {

  listButtons: { url: string; nameicon: string; name: string;action: void; }[];
  valuesTable;
  columnsTable;
  fgSearchScheduling:FormGroup;
  
  displayView: boolean = false;
  displayViewAgenda: boolean = false;
  displayAttachAct: boolean = false;
  displayUpdateState: boolean = false;
  
  constructor( 
    private formBuilder:FormBuilder,
    private schedulingService:SchedulingService
  ) { }

  ngOnInit(): void {

    this.fgSearchScheduling = this.initFormGroupSearch();
    this.columnsTable = this.initColumnsTable();

    this.listButtons = [
      {url:"#",nameicon:"remove_red_eye", name:"Ver",action:this.showDialogByAction.bind(this)},
      {url:"#",nameicon:"notes", name:"Consultar Agenda", action:this.showDialogByAction.bind(this)},
      {url:"#",nameicon:"attach_file", name:"Adjuntar Acta",action:this.showDialogByAction.bind(this)},
      {url:"#",nameicon:"edit", name:"Actualizar estado",action:this.showDialogByAction.bind(this)},
    ];

    this.schedulingService.fetchProcedureScheduled().then(items => {
      this.valuesTable = items;
    })
  }

  showDialogByAction(e, action:string) {
    e.preventDefault()
    switch (action) {
      case 'Ver': this.displayView = true; break;
      case 'Consultar Agenda': this.displayViewAgenda = true; break;
      case 'Adjuntar Acta': this.displayAttachAct = true; break;
      case 'Actualizar estado': this.displayUpdateState = true; break;
      default: break;
    }
  }

  initFormGroupSearch(){
    return this.formBuilder.group({
      stateProcedure:[''],
      room:[''],
      schedulingMonth:[''],
      schedulingCut:['']
    })
  }

  initColumnsTable(){
    return [
      { field: 'numberRecorded', header: 'Número de Radicado' },
      { field: 'date', header: 'Fecha de Radicado - Recibido' },
      { field: 'group', header: 'Grupo' },
      { field: 'numberFile', header: 'Numero de Expediente' },
      { field: 'typeProcedure', header: 'Tipo de trámite' },
      { field: 'typeRequest', header: 'Tipo solicitud' },
      { field: 'stateProcedure', header: 'Estado del trámite' },
      { field: 'nameProduct', header: 'Nombre del Producto' },
      { field: 'activeSubstance', header: 'Principio Activo' },
      { field: 'room', header: 'Sala' },
    ];
  }

}
