import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { SchedulingService } from 'src/app/infraestructure/services/scheduling.service';

@Component({
  selector: 'app-update-state-work-assigned',
  templateUrl: './update-state-work-assigned.component.html',
  styleUrls: ['./update-state-work-assigned.component.css']
})
export class UpdateStateWorkAssignedComponent implements OnInit {

  @Input() procedure:{numeroRadicado:string,fechaRadicado:string, fechaResuelto:Date} = {
    numeroRadicado: "202005189", 
    fechaRadicado: "2021-05-27T05:00:00.000Z", 
    fechaResuelto:new Date()
  };

  fgUpdateState:FormGroup;

  statusOption;

  constructor(
    private formBuilder:FormBuilder,
    public schedulingService:SchedulingService
  ) { }

  ngOnInit(): void {
    this.fgUpdateState = this.initFormGroupUpdateState();

    this.schedulingService.fetchListStateProcedure('ANZ006').then(res=>{
      this.statusOption = res;
    })

    this.fgUpdateState.valueChanges.subscribe(val => {
      console.log(val)
    });

  }

  initFormGroupUpdateState(){
    return this.formBuilder.group({
      state:[''],
      observations:['',[Validators.maxLength(500)]],
    })
  }

  saveResponse(){
    console.log(this.fgUpdateState.value)
  }

}
