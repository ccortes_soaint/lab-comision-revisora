import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UpdateStateWorkAssignedComponent } from './update-state-work-assigned.component';
import { PRIMENG_MODULES } from 'src/app/shared/enums/primeng.elements';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [UpdateStateWorkAssignedComponent],
  imports: [
    CommonModule,
    ...PRIMENG_MODULES,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [UpdateStateWorkAssignedComponent]
})
export class UpdateStateWorkAssignedModule { }
