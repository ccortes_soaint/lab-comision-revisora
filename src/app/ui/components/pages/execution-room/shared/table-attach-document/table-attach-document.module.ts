import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableAttachDocumentComponent } from './table-attach-document.component';
import { PRIMENG_MODULES } from 'src/app/shared/enums/primeng.elements';



@NgModule({
  declarations: [TableAttachDocumentComponent],
  imports: [
    CommonModule,
    ...PRIMENG_MODULES
  ],
  exports: [TableAttachDocumentComponent]
})
export class TableAttachDocumentModule { }
