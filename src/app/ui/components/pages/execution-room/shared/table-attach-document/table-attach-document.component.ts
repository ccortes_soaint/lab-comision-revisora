import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-table-attach-document',
  templateUrl: './table-attach-document.component.html',
  styleUrls: ['./table-attach-document.component.css']
})
export class TableAttachDocumentComponent implements OnInit {

  @Input() valuesTable;
  @Input() columnsTable: { field:string; header:string}[];
  @Input() listActions:{icon:string; action:void}[] = [];

  constructor() { }

  ngOnInit(): void {
    
  }


}
