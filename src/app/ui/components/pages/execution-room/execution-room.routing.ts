import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'src/app/infraestructure/services/auth/authGuard.service';
import { UI_ROUTES_PATH } from '../../app.ui.routing.name';
import { ExecutionRoomComponent } from './execution-room.component';

const routes: Routes = [
    { path: '', redirectTo: UI_ROUTES_PATH.executionRoom.value, pathMatch: 'full' },
    {
        path: '',
        component: ExecutionRoomComponent,
        canActivateChild: [AuthGuardService],
        data: {
            breadcrumb: UI_ROUTES_PATH.executionRoom.name
        },
        children:[]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ExecutionRoomRoutingModule {
}