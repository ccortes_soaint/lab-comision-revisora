import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-update-status-modal',
  templateUrl: './update-status-modal.component.html',
  styleUrls: ['./update-status-modal.component.css']
})
export class UpdateStatusModalComponent implements OnInit {

  @Input() form : any; 
  addTodo(title:string){
    console.log(title)
  }

  returnOption: { name: string}[];

  constructor() { 

    this.returnOption = [
      {name: 'Conceptuado recomienda aprobar'},
      {name: 'Conceptuado recomienda negar'},
      {name: 'Conceptuado recomienda requerir'},
      {name: 'Aplazado'},
  ];

 }
  ngOnInit(): void {
  }
}
