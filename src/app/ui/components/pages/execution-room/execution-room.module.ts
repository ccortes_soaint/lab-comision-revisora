import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExecutionRoomComponent } from './execution-room.component';
import { ExecutionRoomRoutingModule } from './execution-room.routing';
import { SearchSchedulingModule } from '../scheduling/search-scheduling/search-scheduling.module';
import { TableSchedulingModule } from '../scheduling/table-scheduling/table-scheduling.module';
import { ListButtonActionModule } from '../scheduling/shared/list-button-action/list-button-action.module';
import { PRIMENG_MODULES } from 'src/app/shared/enums/primeng.elements';
import { UpdateStatusModalComponent } from './update-status-modal/update-status-modal.component';
import { ViewAgendaModule } from './view-agenda/view-agenda.module';
import { TableAttachDocumentModule } from './shared/table-attach-document/table-attach-document.module';
import { AttachDocumentModalModule } from './attach-document-modal/attach-document-modal-module';
import { UpdateStateWorkAssignedModule } from './shared/update-state-work-assigned/update-state-work-assigned.module';

@NgModule({
  declarations: [ExecutionRoomComponent, UpdateStatusModalComponent],
  imports: [
    CommonModule,
    ExecutionRoomRoutingModule,
    SearchSchedulingModule,
    TableSchedulingModule,
    ListButtonActionModule,
    ...PRIMENG_MODULES,
    ViewAgendaModule,
    TableAttachDocumentModule,
    AttachDocumentModalModule,
    UpdateStateWorkAssignedModule
  ],
  exports: [ExecutionRoomComponent]
})
export class ExecutionRoomModule { }
