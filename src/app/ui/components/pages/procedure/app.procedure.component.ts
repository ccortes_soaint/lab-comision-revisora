import { Component } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { MenuItem } from 'primeng';
import { filter } from 'rxjs/operators';
import { MenuService } from 'src/app/infraestructure/services/menu/app.menu.service';
import { UI_ROUTES_PATH } from '../../app.ui.routing.name';
import { PROCEDURE_PATH } from './app.procedure.routing.name';

@Component({
    selector: 'app-procedure',
    templateUrl: './app.procedure.component.html',
})
export class AppProcedureComponent {

    private items: MenuItem[];

    constructor(
        private router: Router,
        private menuService: MenuService
    ) {
        this.router.events
            .pipe(filter(event => event instanceof NavigationEnd))
            .subscribe((aux: NavigationEnd) => {
                const route = '/' + UI_ROUTES_PATH.procedure.value;

                if (aux.url == route)
                    this.addMenu();
            });
    }

    addMenu() {
        this.items = [
            {
                id: PROCEDURE_PATH.new.value,
                label: PROCEDURE_PATH.new.name,
                icon: 'add',
                routerLink: ['/' + UI_ROUTES_PATH.procedure.value + '/' + PROCEDURE_PATH.new.value]
            },
            {
                id: PROCEDURE_PATH.view.value,
                label: PROCEDURE_PATH.view.name,
                icon: 'search',
                routerLink: ['/' + UI_ROUTES_PATH.procedure.value + '/' + PROCEDURE_PATH.view.value]
            }
        ];

        this.menuService.addChildItem(this.items, UI_ROUTES_PATH.procedure.value);
    }
}