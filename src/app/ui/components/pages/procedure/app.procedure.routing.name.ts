export const PROCEDURE_PATH = {
    view: { value: 'Consultar', name: 'Consultar Tramites' },
    new: { value: 'Nueva', name: 'Nueva solicitud' },
    generalInfo: { value: 'InformacionGeneral', name: 'Información general' },
    generalRate: { value: 'InformacionTarifa', name: 'Información tarifa' },
    legalInfo: { value: 'InformacionLegal', name: 'Información legal' },
    technicalInfo: { value: 'InformacionTecnica', name: 'Información técnica' },
    paymentInfo: { value: 'InformacionPago', name: 'Información pago' },
    attachDocuments: { value: 'AdjuntarDocumento', name: 'Adjuntar documento' }
}
