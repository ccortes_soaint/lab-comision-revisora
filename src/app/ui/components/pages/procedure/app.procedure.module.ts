import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PRIMENG_MODULES } from 'src/app/shared/enums/primeng.elements';
import { AppUIModule } from '../../app.ui.module';
import { LayoutModule } from '../../layouts/layout.module';
import { AppAddProcedureComponent } from './addProcedure/app.addProcedure.component';
import { AppGeneralInformationComponent } from './addProcedure/generalInformation/app.generalInformation.component';
import { AppGeneralRateComponent } from './addProcedure/generalRate/app.generalRate.component';
import { AppLegalInformationComponent } from './addProcedure/legalInformation/app.legalInformation.component';
import { AppPaymentInformationComponent } from './addProcedure/paymentInformation/app.paymentInformation.component';
import { AppTechnicalInformationComponent } from './addProcedure/technicalInformation/app.technicalInformation.component';
import { AppProcedureComponent } from './app.procedure.component';
import { ProcedureRoutingModule } from './app.procedure.routes';
import { AppViewProcedureComponent } from './viewProcedure/app.viewProcedure.component';
import { AppAttachDocumentsComponent } from './addProcedure/attachDocuments/app.attachDocuments.component';
import { AppAnnulFiledComponent} from "./addProcedure/annulFiled/app.annulFiled.component";


@NgModule({
    declarations: [
        AppProcedureComponent,
        AppViewProcedureComponent,
        AppAddProcedureComponent,
        AppGeneralInformationComponent,
        AppGeneralRateComponent,
        AppLegalInformationComponent,
        AppPaymentInformationComponent,
        AppTechnicalInformationComponent,
        AppAttachDocumentsComponent,
        AppAnnulFiledComponent
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        LayoutModule,
        ...PRIMENG_MODULES,
        ProcedureRoutingModule,
        AppUIModule
    ]
})
export class ProcedureModule { }
