import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'src/app/infraestructure/services/auth/authGuard.service';
import { UI_ROUTES_PATH } from '../../app.ui.routing.name';
import { AppDashBoardComponent } from '../dashboard/app.dashBoard.component';
import { AppAddProcedureComponent } from './addProcedure/app.addProcedure.component';
import { AppGeneralInformationComponent } from './addProcedure/generalInformation/app.generalInformation.component';
import { AppGeneralRateComponent } from './addProcedure/generalRate/app.generalRate.component';
import { AppLegalInformationComponent } from './addProcedure/legalInformation/app.legalInformation.component';
import { AppPaymentInformationComponent } from './addProcedure/paymentInformation/app.paymentInformation.component';
import { AppTechnicalInformationComponent } from './addProcedure/technicalInformation/app.technicalInformation.component';
import { AppProcedureComponent } from './app.procedure.component';
import { PROCEDURE_PATH } from './app.procedure.routing.name';
import { AppViewProcedureComponent } from './viewProcedure/app.viewProcedure.component';
import {AppAttachDocumentsComponent} from "./addProcedure/attachDocuments/app.attachDocuments.component";

const routes: Routes = [
    { path: '', redirectTo: UI_ROUTES_PATH.dashboard, pathMatch: 'full' },
    {
        path: '',
        component: AppProcedureComponent,
        canActivateChild: [AuthGuardService],
        data: {
            breadcrumb: UI_ROUTES_PATH.procedure.name
        },
        children: [
            {
                path: UI_ROUTES_PATH.dashboard,
                component: AppDashBoardComponent,
                data: {
                    breadcrumb: UI_ROUTES_PATH.dashboard
                }
            },
            {
                path: PROCEDURE_PATH.new.value,
                component: AppAddProcedureComponent,
                canActivate: [AuthGuardService],
                data: {
                    breadcrumb: PROCEDURE_PATH.new.name
                },
                children: [
                    {
                        path: PROCEDURE_PATH.generalInfo.value,
                        component: AppGeneralInformationComponent,
                        data: {
                            breadcrumb: PROCEDURE_PATH.generalInfo.name
                        }
                    },
                    {
                        path: PROCEDURE_PATH.generalRate.value,
                        component: AppGeneralRateComponent,
                        data: {
                            breadcrumb: PROCEDURE_PATH.generalRate.name
                        }
                    },
                    {
                        path: PROCEDURE_PATH.legalInfo.value,
                        component: AppLegalInformationComponent,
                        data: {
                            breadcrumb: PROCEDURE_PATH.legalInfo.name
                        }
                    },
                    {
                        path: PROCEDURE_PATH.technicalInfo.value,
                        component: AppTechnicalInformationComponent,
                        data: {
                            breadcrumb: PROCEDURE_PATH.technicalInfo.name
                        }
                    },
                    {
                        path: PROCEDURE_PATH.paymentInfo.value,
                        component: AppPaymentInformationComponent,
                        data: {
                            breadcrumb: PROCEDURE_PATH.paymentInfo.name
                        }
                    },
                    {
                        path: PROCEDURE_PATH.attachDocuments.value,
                        component: AppAttachDocumentsComponent,
                        data: {
                            breadcrumb: PROCEDURE_PATH.attachDocuments.name
                        }
                    }
                ]
            },
            {
                path: PROCEDURE_PATH.view.value,
                component: AppViewProcedureComponent,
                canActivateChild: [AuthGuardService],
                data: {
                    breadcrumb: PROCEDURE_PATH.view.name
                },
            }
        ]
    }
];


@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ProcedureRoutingModule {
}
