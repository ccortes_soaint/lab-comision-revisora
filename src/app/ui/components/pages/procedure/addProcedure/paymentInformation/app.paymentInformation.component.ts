import { Component, Input } from '@angular/core';
import {ParameterDTO} from "../../../../../../domain/model/dto/comunes/parameterDTO";
import {
    HEADERSCPC,
    HEADERSCPS,
    VALUESDEFAULTCPC, VALUESDEFAULTCPS
} from "../../../../../../domain/model/mocks/paymentInfoMock";


@Component({
    selector: 'app-paymentInformation',
    templateUrl: './app.paymentInformation.component.html',
})
export class AppPaymentInformationComponent {

    @Input() personTypeLs: ParameterDTO[];
    personTypeSelect: ParameterDTO;
    accion:boolean=false;
    options: any[];//opciones del dropdown
    paymentCode:any;
    paymentSearch:boolean;

    constructor() {
        this.options = [
            {name: 'Representante Legal', code: 'Rle'},
            {name: 'Persona juridica', code: 'Ju'},
            {name: 'Persona Natural', code: 'Na'}
        ];
    }


}
