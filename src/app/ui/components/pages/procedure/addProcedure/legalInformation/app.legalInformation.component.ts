import { Component } from '@angular/core';
import { ParameterDTO } from 'src/app/domain/model/dto/comunes/parameterDTO';
import {MessageService} from 'primeng';

@Component({
    selector: 'app-legalInformation',
    templateUrl: './app.legalInformation.component.html',
    providers: [MessageService]
})
export class AppLegalInformationComponent {

    procedureCategoryLs: ParameterDTO[];
    displayNotification: boolean = false;
    value: boolean;
    public headers: any[] = [];
    public list: any[] = [];
    public isActivateMoreData: boolean = false;

    constructor(private messageService: MessageService) {
    }

    ngOnInit() {

    }

    showDialogNotification() {
        if(this.value){
            this.displayNotification = true;
        }
    }
    onHideDataMore() {
        this.isActivateMoreData = false;
    }

}
