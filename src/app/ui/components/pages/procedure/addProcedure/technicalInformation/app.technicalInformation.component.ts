import {Component, ViewChild} from '@angular/core';
import { ParameterDTO } from 'src/app/domain/model/dto/comunes/parameterDTO';
import {MessageService} from "primeng";
import {TiPharmacologicalEvaluationFiveComponent} from "../../shared/technicalInformation/ti-pharmacological-evaluation-five/app.ti-pharmacological-evaluation-five.component";

@Component({
    selector: 'app-technicalInformation',
    templateUrl: './app.technicalInformation.component.html',
})
export class AppTechnicalInformationComponent {

    procedureCategoryLs: ParameterDTO[];
    pharmacologicalTwo: ParameterDTO[];
    interestInfo: string;
    @ViewChild('headLineVerification') headLineVerification;
    @ViewChild('attorneyVerification') attorneyVerification;
    @ViewChild('pharmacologicalFive') pharmacologicalFive : TiPharmacologicalEvaluationFiveComponent;
    index: any;
    infoFormFive: any;

    constructor(private messageService: MessageService) { }

    openHeadLineVerification() {
        this.headLineVerification.display = true;
    }

    openAttorneyVerification() {
        this.attorneyVerification.display = true;
    }

    formFive(event) {
        return  event;
    }

    save() {
        if (this.index == 4){
            console.log('entro al 4')
        }else if (this.index == 5){
            this.infoFormFive = this.pharmacologicalFive.formFive();
            if (this.infoFormFive){
                this.messageService.add({severity:'success', summary: 'Se ha guardado exitosamente'});
            }else {
                alert('ERROR');
            }
        }

    }

    handleChange(event) {
        this.index = event.index
        console.log(this.index);
    }
}
