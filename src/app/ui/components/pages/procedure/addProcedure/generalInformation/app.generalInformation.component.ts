import { Component } from '@angular/core';
import {ParameterDTO} from "../../../../../../domain/model/dto/comunes/parameterDTO";


@Component({
    selector: 'app-generalInformation',
    templateUrl: './app.generalInformation.component.html',
})
export class AppGeneralInformationComponent {

    productTypeLs: ParameterDTO[];
    groupLs: ParameterDTO[];
    subGroupLs: ParameterDTO[];
    productCategoryLs: ParameterDTO[];
    procedureCategoryLs: ParameterDTO[];
    procedureTypeLs: ParameterDTO[];
    procedureGroupLs: ParameterDTO[];
    pesticideClassLs: ParameterDTO[];
    drinkTypeLs: ParameterDTO[];
    creteProductsLs: ParameterDTO[];
    pesticideTypeLs: ParameterDTO[];
    toxCategoryLs: ParameterDTO[];
    interestInfo: string;

    constructor() { }
}
