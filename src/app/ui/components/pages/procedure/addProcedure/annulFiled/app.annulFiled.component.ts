import {Component} from "@angular/core";
import {ParameterDTO} from "../../../../../../domain/model/dto/comunes/parameterDTO";
import {ColumnsDTO} from "../../../../../../domain/model/dto/comunes/columnsDTO";




@Component({
    selector: 'app-annulFiled',
    templateUrl: './app.annulFiled.component.html',
})

export class AppAnnulFiledComponent {
    radicatedFoundLs: ParameterDTO[];
    columns: ColumnsDTO[] = [{field:"document", header:"documento de identidad"}, {field: "fullName", header:"Nombre completo"}];
    val: any[] = [{document: "12345", fullName:"Pepito Pérez"}];
}
