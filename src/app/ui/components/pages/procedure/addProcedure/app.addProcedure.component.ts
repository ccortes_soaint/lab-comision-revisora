import { Component } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { MenuItem } from 'primeng';
import { filter } from 'rxjs/operators';
import { MenuService } from 'src/app/infraestructure/services/menu/app.menu.service';
import { UI_ROUTES_PATH } from '../../../app.ui.routing.name';
import { PROCEDURE_PATH } from '../app.procedure.routing.name';

@Component({
    selector: 'app-addProcedure',
    templateUrl: './app.addProcedure.component.html',
})
export class AppAddProcedureComponent {

    private items: MenuItem[];

    constructor(
        private router: Router,
        private menuService: MenuService
    ) {
        this.router.events
            .pipe(filter(event => event instanceof NavigationEnd))
            .subscribe((event: any) => {
                const route = '/' + UI_ROUTES_PATH.procedure.value + '/' + PROCEDURE_PATH.new.value;
                if (event.url == route)
                    this.addProcedureMenu();
            });
    }

    addProcedureMenu() {
        const route = '/' + UI_ROUTES_PATH.procedure.value + '/' + PROCEDURE_PATH.new.value;

        this.items = [
            {
                id: PROCEDURE_PATH.generalInfo.value,
                label: PROCEDURE_PATH.generalInfo.name,
                routerLink: [route + '/' + PROCEDURE_PATH.generalInfo.value]
            },
            {
                id: PROCEDURE_PATH.generalRate.value,
                label: PROCEDURE_PATH.generalRate.name,
                routerLink: [route + '/' + PROCEDURE_PATH.generalRate.value]
            },
            {
                id: PROCEDURE_PATH.legalInfo.value,
                label: PROCEDURE_PATH.legalInfo.name,
                routerLink: [route + '/' + PROCEDURE_PATH.legalInfo.value]
            },
            {
                id: PROCEDURE_PATH.technicalInfo.value,
                label: PROCEDURE_PATH.technicalInfo.name,
                routerLink: [route + '/' + PROCEDURE_PATH.technicalInfo.value]
            },
            {
                id: PROCEDURE_PATH.paymentInfo.value,
                label: PROCEDURE_PATH.paymentInfo.name,
                routerLink: [route + '/' + PROCEDURE_PATH.paymentInfo.value]
            },
            {
                id: PROCEDURE_PATH.attachDocuments.value,
                label: PROCEDURE_PATH.attachDocuments.name,
                routerLink: [route + '/' + PROCEDURE_PATH.attachDocuments.value]
            }
        ];
        this.menuService.addChildItem(this.items, PROCEDURE_PATH.new.value);
    }
}
