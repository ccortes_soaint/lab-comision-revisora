import { Component } from '@angular/core';
import {ParameterDTO} from "../../../../../../domain/model/dto/comunes/parameterDTO";

@Component({
    selector: 'app-generalRate',
    templateUrl: './app.generalRate.component.html',
})
export class AppGeneralRateComponent {

    message;
    activo:boolean=true;
    procedureCategoryLs:ParameterDTO[];
    procedureGroupLs:[{ name: string; grupo: string }, { name: string; grupo: string }];

    constructor() {
        this.procedureGroupLs=[
        {name:'medicamentos de sintesis quimica',grupo:'1'},
        {name:'medicamentos biologicos', grupo:'2'}
    ]}

    verMessage(){
        this.message = [];
        this.message.push({severity: 'success', summary: 'Estado', detail: 'Datos guardados exitosamente'});
    }
}
