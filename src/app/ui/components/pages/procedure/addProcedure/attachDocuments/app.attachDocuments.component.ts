import { Component, OnInit } from '@angular/core';
import {FileUpload} from "primeng";



@Component({
  selector: 'app-attach-documents',
  templateUrl: './app.attachDocuments.component.html',
  styles: []
})
export class AppAttachDocumentsComponent implements OnInit {


    uploadedFiles: any[] = [];
    head: any[] = [];
    results: any[] = [];

  constructor() { }

  ngOnInit(): void {
  }

    viewPdf(file?: any) {
        const url = '../../../../../assets/layout/filePdf/fileTest.pdf';
        const miVentana = window.open('' + url, 'ventana1', 'height=800,width=800,left=300,location=no,menubar=no,resizable=no,scrollbars=yes,status=no,titlebar=no,top=300');
        //miVentana.print();

    }



    attachDocument(event, versionUploader: FileUpload, index) {
        if (event.files.length === 0) {
            return false;
        }
        const nv = {
            id: 1,
            nombre: event.files[0].name,
            tipo: event.files[0].type === 'application/pdf' ? 'pdf' : 'word',
            mimeType: event.files[0].type,
            version: '',
            contenido: '',
            file: event.files[0],
            size: event.files[0].size
        };

        //this.results[index].file = nv;
        console.log(nv, index)
        versionUploader.clear();
        versionUploader.basicFileInput.nativeElement.value = '';
    }

    onErrorUpload(event) {
        console.log(event);
        console.log(event);
    }

    delete(index){

        //this.results[index].file = null;



    }

    fileValidate(element){
        let hide;
        if(element != null ) {
            hide = false;
        }
        else {
            hide = true;
        }
        return hide;
    }
}
