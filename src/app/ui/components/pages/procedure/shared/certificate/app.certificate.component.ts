import { Component, Input } from '@angular/core';
import {DATE_ES} from "../../../../app.ui.environment";
import {ParameterDTO} from "../../../../../../domain/model/dto/comunes/parameterDTO";




@Component({
    selector: 'app-certificate',
    templateUrl: './app.certificate.component.html',

})

export class AppCertificateComponent  {

    @Input() certifiedTypeLs: ParameterDTO[];
    @Input() hasCertificateLs: ParameterDTO[];
    @Input() agencyExpeditionLs: ParameterDTO[];
    @Input() referenceAgencyLs: ParameterDTO[];

    certifiedType: ParameterDTO;
    hasCertificate: ParameterDTO;
    agencyExpedition: ParameterDTO;
    referenceAgency: ParameterDTO;

    expeditionDate: Date;
    validityDate: Date;
    es = DATE_ES;

    requiredCertificate: string;
    certificateNumber: string;



    // tabla

    value: any;
    selected: any;
    display: any;
    itemEdit: any;


    constructor() {

    }


    insert() {

    }







    viewPdf() {

    }

}
