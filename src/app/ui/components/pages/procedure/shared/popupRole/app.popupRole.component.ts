import {Component, EventEmitter, Input, OnInit, Output, OnChanges, SimpleChanges} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
    selector: 'app-popup-role',
    templateUrl: './app.popupRole.component.html',
    styles: []
})
export class AppPopupRoleComponent implements OnInit, OnChanges {

    items = [];
    selectedCities:any;
    public formInfoLegalRole: FormGroup;
    @Output('getData') public submit: EventEmitter<any> = new EventEmitter<any>();
    @Input('infoLegal') public _infoLegalItem: any;
    @Input('isEditing') public isEditing: boolean = false;
    private idCustomItem: number = -1;

    constructor(private fb: FormBuilder) {

        for (let i = 0; i < 10; i++) {
            this.items.push({label: 'Item ' + i, value: 'Item ' + i});
        }
    }

    get infoLegalItem() {

        if (!this._infoLegalItem) {

            return {};

        }
        return this._infoLegalItem;
    }

    ngOnInit() {
        this.formInfoLegalRole = this.fb.group({
            roleType: this.fb.control(!this._infoLegalItem ? '' : this._infoLegalItem.roleType, Validators.compose([Validators.required])),
            questionRol: this.fb.control(!this._infoLegalItem ? '' : this._infoLegalItem.questionRol, Validators.compose([Validators.required])),
            documentType: this.fb.control(!this._infoLegalItem ? '' : this._infoLegalItem.documentType, Validators.compose([Validators.required])),
            documentNumber: this.fb.control(!this._infoLegalItem ? '' : this._infoLegalItem.documentNumber, Validators.compose([Validators.required])),
            reasonName: this.fb.control(!this._infoLegalItem ? '' : this._infoLegalItem.reasonName, Validators.compose([Validators.required])),
            addressHouse: this.fb.control(!this._infoLegalItem ? '' : this._infoLegalItem.addressHouse, Validators.compose([Validators.required])),
            countryResidence: this.fb.control(!this._infoLegalItem ? '' : this._infoLegalItem.countryResidence, Validators.compose([Validators.required])),
            cityResidence: this.fb.control(!this._infoLegalItem ? '' : this._infoLegalItem.cityResidence, Validators.compose([Validators.required])),
            department: this.fb.control(!this._infoLegalItem ? '' : this._infoLegalItem.department, Validators.compose([Validators.required])),
            email: this.fb.control(!this._infoLegalItem ? '' : this._infoLegalItem.email, Validators.compose([Validators.required])),
            telephone: this.fb.control(!this._infoLegalItem ? '' : this._infoLegalItem.telephone, Validators.compose([Validators.required])),
            cellphone: this.fb.control(!this._infoLegalItem ? '' : this._infoLegalItem.cellphone, Validators.compose([Validators.required]))
        });

    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.isEditing && changes.isEditing.currentValue) {

            const editItem = changes._infoLegalItem ? changes._infoLegalItem.currentValue : null;
            if (!!editItem) {
                this.idCustomItem = editItem.documentNumber;
                this.formInfoLegalRole.markAllAsTouched();
                this.formInfoLegalRole.setValue({
                    roleType: editItem.roleType,
                    questionRol: editItem.questionRol,
                    documentType: editItem.documentType,
                    documentNumber: editItem.documentNumber,
                    reasonName: editItem.reasonName,
                    addressHouse: editItem.addressHouse,
                    countryResidence: editItem.countryResidence,
                    cityResidence: editItem.cityResidence,
                    department: editItem.department,
                    email: editItem.email,
                    telephone: editItem.telephone,
                    cellphone: editItem.cellphone,
                });
                this.formInfoLegalRole.get('documentNumber').disable();
            }
        } else if (this.formInfoLegalRole) {
            this.idCustomItem = -1;
            this.formInfoLegalRole.reset();
            this.formInfoLegalRole.get('documentNumber').enable();
        }

    }

    onSubmit(form: FormGroup) {
        if (form.valid) {
            if (this.isEditing) {
                this.submit.emit({...form.value, documentNumber:this.idCustomItem});
            }else{
                this.submit.emit(form.value);
            }
            this.formInfoLegalRole.reset();
        } else {

            alert('Formulario  invalido');
        }
    }

}
