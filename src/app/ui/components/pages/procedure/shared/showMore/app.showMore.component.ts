import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-show-more',
  templateUrl: './app.showMore.component.html',
  styles: []
})
export class AppShowMoreComponent implements OnInit {

    @Input() headers: any []=[];
    @Input() data:any[]=[];
  constructor() { }

  ngOnInit(): void {
  }

}
