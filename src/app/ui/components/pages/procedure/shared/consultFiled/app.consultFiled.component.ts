import {Component, OnInit} from '@angular/core';

import {DATE_ES} from "../../../../app.ui.environment";

@Component({
    selector: 'app-consult-filed',
    templateUrl: './app.consultFiled.component.html',
    styles: []
})
export class AppConsultFiledComponent implements OnInit {
    es = DATE_ES;
    initDate:Date;
    endDate:Date;
    numberRadicated:string;

    constructor() {

    }

    ngOnInit(): void {

    }
}
