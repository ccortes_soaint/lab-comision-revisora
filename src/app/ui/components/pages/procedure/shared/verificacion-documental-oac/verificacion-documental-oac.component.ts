import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-verificacion-documental-oac',
  templateUrl: './verificacion-documental-oac.component.html',
  styles: []
})
export class VerificacionDocumentalOacComponent implements OnInit {
    resultInformation: any;

  constructor() { }

  ngOnInit(): void {
      this.resultInformation = [
          {
              name: 'Solicitud del tramite firmada',
              date: 'xxxx'
          },
          {
              name: 'Copia del certificado de BPM del fabricante en el cual conste que las instalaciones industriales y las operaciones de fabricación se ajustan a  la BPM aceptadas en el país fabricante, solo si el fabricante no ha sido visitado por el INVIMA.  artículo 31, literal a',
              date: 'xxxx'
          }

      ];
  }

}
