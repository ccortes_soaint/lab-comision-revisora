import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {
    BIS,
    ORIENTATION,
    PLUGINTYPES,
    QUADRANTPREFIXES,
    ROADTYPES
} from "../../../../../../domain/model/mocks/addressMock";

@Component({
    selector: 'app-popup-address',
    templateUrl: './app.popupAddress.component.html',
    styles: []
})
export class AppPopupAddressComponent implements OnInit {

    items = [];
    public formAddress: FormGroup;
    @Output("getData") public submit: EventEmitter<any> = new EventEmitter<any>();
    @Input("infoLegal") public _infoLegalItem: any;
    @Input("isEditing") public isEditing: boolean = false;
    private idCustomItem: number = -1;
    public display: boolean;
    roadTypesList = ROADTYPES;
    quadrantPrefixesList = QUADRANTPREFIXES;
    bisList = BIS;
    orientationsList = ORIENTATION;
    pluginTypesList = PLUGINTYPES;


    constructor(private fb: FormBuilder) {

    }

    ngOnInit() {
        this.formAddress = this.fb.group({

            roadType: this.fb.control(!this._infoLegalItem ? '' : this._infoLegalItem.userType, Validators.compose([Validators.required])),
            numberRoadPrincipal: this.fb.control(!this._infoLegalItem ? '' : this._infoLegalItem.documentType, Validators.compose([Validators.required])),
            quadrantPrefix: this.fb.control(!this._infoLegalItem ? '' : this._infoLegalItem.documentNumber, Validators.compose([Validators.required])),
            bis: this.fb.control(!this._infoLegalItem ? '' : this._infoLegalItem.reasonName, Validators.compose([Validators.required])),
            orientation: this.fb.control(!this._infoLegalItem ? '' : this._infoLegalItem.stateInformation, Validators.compose([Validators.required])),
            numberRoad: this.fb.control(!this._infoLegalItem ? '' : this._infoLegalItem.documentType, Validators.compose([Validators.required])),
            quadrantPrefix2: this.fb.control(!this._infoLegalItem ? '' : this._infoLegalItem.documentNumber, Validators.compose([Validators.required])),
            plateNumber: this.fb.control(!this._infoLegalItem ? '' : this._infoLegalItem.documentNumber, Validators.compose([Validators.required])),
            orientation2: this.fb.control(!this._infoLegalItem ? '' : this._infoLegalItem.stateInformation, Validators.compose([Validators.required])),
            pluginType: this.fb.control(!this._infoLegalItem ? '' : this._infoLegalItem.stateInformation, Validators.compose([Validators.required])),
            additionalPlugin: this.fb.control(!this._infoLegalItem ? '' : this._infoLegalItem.stateInformation, Validators.compose([Validators.required])),
            address: this.fb.control(!this._infoLegalItem ? '' : this._infoLegalItem.stateInformation, Validators.compose([Validators.required])),
        });
    }
}
