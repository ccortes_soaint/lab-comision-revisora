import {Component, OnInit} from '@angular/core';

import {MessageService} from 'primeng';
import {ParameterDTO} from "../../../../../../domain/model/dto/comunes/parameterDTO";

@Component({
    selector: 'app-legal-information-attorney',
    templateUrl: './app.legalInformationAttorney.component.html',
    styles: [],
    providers: [MessageService]
})
export class AppLegalInformationAttorneyComponent implements OnInit {
    infoLegalAttorney: any[] = [];
    value: boolean;
    displayAttorney: boolean = false;
    private itemInfoLegalAttorney: any;
    public isEditing: boolean = false;
    public headers: any[] = [];
    public list: any[] = [];
    public isActivateMoreData: boolean = false;
    public headerValuesTable2  =[];
    public headers2 = this.headerValuesTable2;

    constructor(private messageService: MessageService) {
    }

    ngOnInit(): void {
    }

    onRowEditInitTable2(itemInfoLegalAttorney: any) {
        this.displayAttorney = true;
        this.isEditing = true;
        this.itemInfoLegalAttorney = itemInfoLegalAttorney;
    }

    get getItemInfoLegalAttorney() {

        return this.itemInfoLegalAttorney;
    }

    onRowEditSaveTable2(infoLegalAttorney: any) {
        console.log('Row edit saved');
    }

    onRowEditCancelTable2(infoLegalAttorney: any, index: number) {
        console.log('Row edit cancelled');
    }

    onRowDeleteTable2(index: number) {
        this.infoLegalAttorney.splice(index, 1);
    }

    catchDataFormInfoLegalAttorney(data) {
        console.log('dataforminfolegal', data);
        debugger
        this.displayAttorney = false;
        if (this.isEditing) {
            const indexItem = this.infoLegalAttorney.findIndex(item => item.documentNumber === data.documentNumber);
            if (indexItem !== -1) {
                this.infoLegalAttorney[indexItem] = {
                    userType: data.userType,
                    documentType: data.documentType,
                    documentNumber: data.documentNumber,
                    reasonName: data.reasonName,
                    stateInformation: data.stateInformation
                };
            }
            this.messageService.add({severity: 'success', summary: 'Actualizado Exítosamente', detail: 'Exítoso'});
        } else {
            this.infoLegalAttorney.push({
                userType: data.userType,
                documentType: data.documentType,
                documentNumber: data.documentNumber,
                reasonName: data.reasonName,
                stateInformation: data.stateInformation
            });
            this.messageService.add({severity: 'success', summary: 'Agregado Exítosamente', detail: 'Exítoso'});
        }
    }

    onHideAttorney() {
        this.isEditing = false;
        this.itemInfoLegalAttorney = null;
    }

    showDialogAttorney() {
        this.displayAttorney = !this.displayAttorney;
    }

    moreDetailTable(headers: any[] = [], list: any[] = []) {
        this.headers = headers;
        this.list = list;
        this.isActivateMoreData = true;

    }

    onHideDataMore() {
        this.isActivateMoreData = false;
    }


}
