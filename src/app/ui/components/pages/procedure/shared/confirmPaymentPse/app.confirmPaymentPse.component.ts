import {Component,Input} from "@angular/core";
import {HEADERSCPS, VALUESDEFAULTCPS} from "../../../../../../domain/model/mocks/paymentInfoMock";

@Component({
    selector: 'app-confirmPaymentPse',
    templateUrl: './app.confirmPaymentPse.component.html'
})
export class AppConfirmPaymentPseComponent{

    columnPse: any[] = [];
    dataPse: any[];

    constructor() {
        this.columnPse = HEADERSCPS;
        this.dataPse = VALUESDEFAULTCPS;
    }

}
