import {Component, Input} from "@angular/core";
import {HEADERSDP, VALUESDEFAULTDP} from "../../../../../../domain/model/mocks/paymentInfoMock";

@Component({
    selector: 'app-paymentDescription',
    templateUrl: './app.paymentDescription.component.html'
})
export class AppPaymentDescriptionComponent{
    @Input() accion:boolean;
    columnsDescriptionPayment: any[] = [];
    dataDescriptionPayment: any[];
    totalFull:number;

    constructor() {

        this.columnsDescriptionPayment=HEADERSDP;
        this.dataDescriptionPayment=VALUESDEFAULTDP;

    }

}
