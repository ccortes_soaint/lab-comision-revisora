import {Component} from "@angular/core";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {PREGUNTAS} from "../../../../../../domain/model/mocks/generalRateMock";


@Component({
    selector: 'app-guidingQuestions',
    templateUrl: './app.guidingQuestions.component.html'
})
export class AppGuidingQuestionsComponent{

    questions: any[];//datos del form preguntas
    formQuestion: FormGroup;//formulario
    visitingCityLs:any[];


    constructor() {
        this.questions= PREGUNTAS;

        this.formQuestion = new FormGroup({
            'grupo1': new FormControl('', Validators.required),
            'grupo2': new FormControl('', Validators.required),
            'grupo3': new FormControl('', Validators.required),
            'grupo4': new FormControl('1', [Validators.required, Validators.minLength(0)]),
            'grupo5': new FormControl('', Validators.required)
        });
    }

}
