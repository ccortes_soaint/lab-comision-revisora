import { Component, OnInit } from '@angular/core';
import {MessageService} from 'primeng';
import {ParameterDTO} from "../../../../../../domain/model/dto/comunes/parameterDTO";


@Component({
  selector: 'app-legal-information-headline',
  templateUrl: './app.legalInformationHeadline.component.html',
  styles: [],
  providers: [MessageService]
})
export class AppLegalInformationHeadlineComponent implements OnInit {

    infoLegal: any[] = [];
    value: boolean;
    displayHeadline: boolean = false;
    private itemInfoLegalHeadline: any;
    public isEditing: boolean = false;
    public headers: any[] = [];
    public list: any[] = [];
    public headers1:ParameterDTO[];
    public isActivateMoreData: boolean = false;
    editIndex: number = -1;


  constructor(private messageService: MessageService) {

  }

  ngOnInit(): void {

  }

    get getItemInfoLegalHeadline() {

        return this.itemInfoLegalHeadline;
    }

    onRowEditInit(itemInfoLegalHeadline: any, index :number) {
        this.displayHeadline = true;
        this.isEditing = true;
        this.itemInfoLegalHeadline = itemInfoLegalHeadline;
        this.editIndex = index;
    }

    onRowEditSave(infoLegal: any) {
        console.log('Row edit saved');
    }

    onRowEditCancel(infoLegal: any, index: number) {
        console.log('Row edit cancelled');
    }

    onRowDelete(index: number) {
        this.infoLegal.splice(index, 1);
    }

    catchDataFormInfoLegalHeadline(data) {
        console.log("dataforminfolegal", data);
        this.displayHeadline = false;
        if (this.isEditing) {


            if (this.editIndex !== -1) {
                this.infoLegal[this.editIndex] = {
                    documentType: data.documentType,
                    documentNumber: data.documentNumber,
                    reasonName: data.reasonName,
                    cityResidence: data.cityResidence,
                    addressHouse: data.addressHouse,
                    country: data.country,
                    department: data.department,
                    email: data.email,
                    telephone: data.telephone,
                    cellphone: data.cellphone
                };
            }
            this.messageService.add({severity: 'success', summary: 'Actualizado Exítosamente', detail: 'Exítoso'});
        } else {
            this.infoLegal.push({
                documentType: data.documentType,
                documentNumber: data.documentNumber,
                reasonName: data.reasonName,
                cityResidence: data.cityResidence,
                addressHouse: data.addressHouse,
                country: data.country,
                department: data.department,
                email: data.email,
                telephone: data.telephone,
                cellphone: data.cellphone
            });
            this.messageService.add({severity: 'success', summary: 'Agregado Exítosamente', detail: 'Exítoso'});
        }

        this.editIndex = -1;
    }

    onHideHeadline() {
        this.isEditing = false;
        this.itemInfoLegalHeadline = null;
    }

    showDialogHeadline() {
        this.displayHeadline = !this.displayHeadline;
    }

    moreDetailTable(headers: any[] = [], list: any[] = []) {
        this.headers = headers;
        this.list = list;
        this.isActivateMoreData = true;
    }
    onHideDataMore() {
        this.isActivateMoreData = false;
    }
}
