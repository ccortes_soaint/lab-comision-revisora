import {Component, EventEmitter, Input, OnInit, Output, OnChanges, SimpleChanges} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {CITIES, COUNTRIES, STATES} from "../../../../../../domain/model/mocks/countryStateCityMock";
import {DOCUMENTTYPES} from "../../../../../../domain/model/mocks/documentTypes";

@Component({
  selector: 'app-popup-headline',
  templateUrl: './app.popupHeadline.component.html',
  styles: []
})
export class AppPopupHeadlineComponent implements OnInit, OnChanges {

    public displayHeadline: boolean = true;
    items = [];
    countries = COUNTRIES;
    states = STATES;
    cities = CITIES;
    documentTypes = DOCUMENTTYPES;
    public formInfoLegalHeadline: FormGroup;
    @Output("getData") public submit:EventEmitter<any> = new EventEmitter<any>();
    @Input("infoLegal") public _infoLegalItem: any;
    @Input("isEditing") public isEditing : boolean=false;
    private idCustomItem: number = -1;

    constructor(private fb: FormBuilder) {

        for (let i = 0; i <10; i++) {
            this.items.push({label: 'Item ' + i, value: 'Item ' + i});
        }
    }

    get infoLegalItem(){

        if(!this._infoLegalItem){

            return {};

        }
        return this._infoLegalItem;
    }

    ngOnInit() {

        this.formInfoLegalHeadline = this.fb.group({

            documentType: this.fb.control(!this._infoLegalItem ?'':this._infoLegalItem.documentType, Validators.compose([Validators.required])),
            documentNumber: this.fb.control(!this._infoLegalItem ?'':this._infoLegalItem.documentNumber, Validators.compose([Validators.required])),
            reasonName: this.fb.control(!this._infoLegalItem ?'':this._infoLegalItem.reasonName, Validators.compose([Validators.required])),
            cityResidence: this.fb.control(!this._infoLegalItem ?'':this._infoLegalItem.cityResidence, Validators.compose([Validators.required])),
            addressHouse: this.fb.control(!this._infoLegalItem ?'':this._infoLegalItem.addressHouse, Validators.compose([Validators.required])),
            country: this.fb.control(!this._infoLegalItem ?'':this._infoLegalItem.country, Validators.compose([Validators.required])),
            department: this.fb.control(!this._infoLegalItem ?'':this._infoLegalItem.department, Validators.compose([Validators.required])),
            email: this.fb.control(!this._infoLegalItem ?'':this._infoLegalItem.email, Validators.compose([Validators.required])),
            telephone: this.fb.control(!this._infoLegalItem ?'':this._infoLegalItem.telephone, Validators.compose([Validators.required])),
            cellphone: this.fb.control(!this._infoLegalItem ?'':this._infoLegalItem.cellphone, Validators.compose([Validators.required]))
        })
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.isEditing && changes.isEditing.currentValue ) {

            const editItem = changes._infoLegalItem ? changes._infoLegalItem.currentValue : null
            if (!!editItem) {
                this.idCustomItem = editItem.documentNumber;
                this.formInfoLegalHeadline.markAllAsTouched();
                this.formInfoLegalHeadline.setValue({
                    documentType: editItem.documentType,
                    documentNumber: editItem.documentNumber,
                    reasonName: editItem.reasonName,
                    cityResidence: editItem.cityResidence,
                    addressHouse:editItem.addressHouse,
                    country:editItem.country,
                    department:editItem.department,
                    email:editItem.email,
                    telephone:editItem.telephone,
                    cellphone:editItem.cellphone
                });
                //this.formInfoLegalHeadline.get('documentNumber').disable();
            }
        }else if(this.formInfoLegalHeadline){
            this.formInfoLegalHeadline.reset()
            this.formInfoLegalHeadline.get('documentNumber').enable();
        }
    }



    onSubmit(form:FormGroup) {
        if (form.valid) {
            if (this.isEditing) {
                this.submit.emit({...form.value});
            }else{
                this.submit.emit(form.value);
            }
            this.formInfoLegalHeadline.reset()
        } else {

            alert("Formulario  invalido")
        }
    }

}
