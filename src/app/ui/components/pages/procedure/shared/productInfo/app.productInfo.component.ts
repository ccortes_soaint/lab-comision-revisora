import { Component, Input } from '@angular/core';
import {ParameterDTO} from "../../../../../../domain/model/dto/comunes/parameterDTO";


@Component({
    selector: 'app-productInfo',
    templateUrl: './app.productInfo.component.html'
})
export class AppProductInfoComponent {

    @Input() productTypeLs: ParameterDTO[];
    @Input() groupLs: ParameterDTO[];
    @Input() subGroupLs: ParameterDTO[];
    @Input() productCategoryLs: ParameterDTO[];
    productTypeSelect: ParameterDTO;
    groupSelect: ParameterDTO;
    subGroupSelect: ParameterDTO;
    productCategorySelect: ParameterDTO;

    constructor() { }
}
