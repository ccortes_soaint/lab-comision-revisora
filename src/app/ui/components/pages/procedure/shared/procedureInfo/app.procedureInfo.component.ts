import { Component, Input } from '@angular/core';

import {DATE_ES} from "../../../../app.ui.environment";
import {ParameterDTO} from "../../../../../../domain/model/dto/comunes/parameterDTO";



@Component({
    selector: 'app-procedureInfo',
    templateUrl: './app.procedureInfo.component.html'
})
export class AppProcedureInfoComponent {

    @Input() groupLs: ParameterDTO[];
    @Input() procedureCategoryLs: ParameterDTO[];
    @Input() procedureTypeLs: ParameterDTO[];
    @Input() modalityLs: ParameterDTO[];
    @Input() pesticideClassLs: ParameterDTO[];
    es = DATE_ES;
    requestDate: Date;
    purpose: string;
    groupSelect: ParameterDTO;
    procedureCategorySelect: ParameterDTO;
    procedureTypeSelect: ParameterDTO;
    modalitySelect: ParameterDTO;
    pesticideClassSelect: ParameterDTO;

    constructor() { }
}
