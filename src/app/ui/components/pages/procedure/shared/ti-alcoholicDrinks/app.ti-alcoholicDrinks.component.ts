import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-ti-alcoholicDrinks',
  templateUrl: './app.ti-alcoholicDrinks.component.html'
})
export class AlcoholicDrinksComponent implements OnInit {
  
  productNumber: string;
  brand: string;
  percentageAlcohol: string;
  usefulLife: string;º

  constructor() { }

  ngOnInit(): void {
  }

}
