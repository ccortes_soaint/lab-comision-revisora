import {Component, Input} from "@angular/core";
import {ParameterDTO} from "../../../../../../domain/model/dto/comunes/parameterDTO";


@Component({
    selector: 'app-searchFilter',
    templateUrl: './app.searchFilter.component.html'
})
export class AppSearchFilterComponent{

    fileNumber;
    intentionNumber;
    filingNumber;
    healthRegister;
    productName;
    @Input() documentTypeLs:ParameterDTO[];
    documentTypeSelect;
    documentNumber;
    @Input() procedureTypeLs:ParameterDTO[];
    procedureTypeSelect;
    @Input() productTypeLs:ParameterDTO[];
    productTypeSelect;
    @Input() stateTypeLs:ParameterDTO[];
    stateTypeSelect;
    resolutionNumber;
    tableData: any[];
    @Input() actionsTypeLs;
    actionsTypeSelect;


    constructor() {}

}
