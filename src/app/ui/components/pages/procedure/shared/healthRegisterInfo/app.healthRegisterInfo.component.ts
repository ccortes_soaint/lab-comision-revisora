import { Component, Input } from '@angular/core';
import { ParameterDTO } from 'src/app/domain/model/dto/comunes/parameterDTO';
import {DATE_ES} from "../../../../app.ui.environment";


@Component({
    selector: 'app-healthRegisterInfo',
    templateUrl: './app.healthRegisterInfo.component.html'
})
export class AppHealthRegisterInfoComponent {

    @Input() drinkTypeLs: ParameterDTO[];
    @Input() creteProductsLs: ParameterDTO[];
    @Input() pesticideTypeLs: ParameterDTO[];
    @Input() toxCategoryLs: ParameterDTO[];
    es = DATE_ES;
    recordNumber: string;
    healthRegister: String;
    validityDate: Date;
    drinkTypeSelect: ParameterDTO;
    creteProductsSelect: ParameterDTO;
    pesticideTypeSelect: ParameterDTO;
    pesticide: string;
    toxconceptCode: String;
    resolutionNumber: String;
    toxCategorySelect: ParameterDTO;

    constructor() { }
}
