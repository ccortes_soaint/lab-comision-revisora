import {Component, Input} from "@angular/core";
import {HEADERSCPC, VALUESDEFAULTCPC} from "../../../../../../domain/model/mocks/paymentInfoMock";

@Component({
    selector: 'app-confirmPaymentConsignment',
    templateUrl: './app.confirmPaymentConsignment.component.html'
})
export class AppConfirmPaymentConsignmentComponent{

    columnConsignment: any[] = [];
    dataConsignment: any[];

    constructor() {
        this.columnConsignment = HEADERSCPC;
        this.dataConsignment = VALUESDEFAULTCPC;
    }

}
