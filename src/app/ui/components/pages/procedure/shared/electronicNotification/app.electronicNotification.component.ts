import {Component, OnInit} from '@angular/core';
import {ParameterDTO} from "../../../../../../domain/model/dto/comunes/parameterDTO";


@Component({
    selector: 'app-electronic-notification',
    templateUrl: './app.electronicNotification.component.html',
    styles: []
})
export class AppElectronicNotificationComponent implements OnInit {
    infoNotification: any[] = [];
    value: boolean;
    enableBtnNotifi: boolean = true;

    constructor() {

    }

    ngOnInit(): void {

    }

    saveNotifi() {
        this.enableBtnNotifi = false;
    }

}
