import {Component, Input, OnInit} from '@angular/core';
import {ParameterDTO} from "../../../../../../domain/model/dto/comunes/parameterDTO";


@Component({
    selector: 'app-table-of-radicates',
    templateUrl: './app.tableOfRadicates.component.html',
    styles: []
})
export class AppTableOfRadicatesComponent implements OnInit {
    @Input() radicatedLs: ParameterDTO[];
    resultsTableRadicated: Array<any> = [];
    selectedRadicated:any;
    constructor() {

    }

    ngOnInit(): void {

    }
}
