import { Component, OnInit } from '@angular/core';

import {MessageService} from 'primeng';
import {ParameterDTO} from "../../../../../../domain/model/dto/comunes/parameterDTO";

@Component({
  selector: 'app-legal-information-role',
  templateUrl: './app.legalInformationRole.component.html',
  styles: [],
  providers: [MessageService]
})
export class AppLegalInformationRoleComponent implements OnInit {

    infoRole: any [] = [];
    displayRole: boolean = false;
    private itemInfoLegalRole: any;
    value: boolean;
    public isEditing: boolean = false;
    public headers: any[] = [];
    public list: any[] = [];
    public isActivateMoreData: boolean = false;
    public headers3:ParameterDTO[];

  constructor(private messageService: MessageService) {

  }

  ngOnInit(): void {
  }

    get getItemInfoLegalRole() {
        return this.itemInfoLegalRole;
    }

    onRowEditInitTable3(itemInfoLegalRole: any) {
        this.displayRole = true;
        this.isEditing = true;
        this.itemInfoLegalRole = itemInfoLegalRole;
    }

    onRowEditSaveTable3(infoRole: any) {
        console.log('Row edit saved');
    }

    onRowEditCancelTable3(infoRole: any, index: number) {
        console.log('Row edit cancelled');
    }

    onRowDeleteTable3(index: number) {
        this.infoRole.splice(index, 1)
    }

    catchDataFormInfoLegalRole(data) {
        console.log("dataforminfolegal", data);
        this.displayRole = false;
        if (this.isEditing) {
            const indexItem = this.infoRole.findIndex(item => item.documentNumber === data.documentNumber)
            if (indexItem !== -1) {
                this.infoRole[indexItem] = {
                    roleType: data.roleType,
                    documentType: data.documentType,
                    documentNumber: data.documentNumber,
                    reasonName: data.reasonName,
                    addressHouse: data.addressHouse,
                    countryResidence: data.countryResidence,
                    cityResidence: data.cityResidence,
                    department: data.department,
                    email: data.email,
                    telephone: data.telephone,
                    cellphone: data.cellphone
                };
            }
            this.messageService.add({severity: 'success', summary: 'Actualizado Exítosamente', detail: 'Exítoso'});
        } else {
            this.infoRole.push({
                roleType: data.roleType,
                documentType: data.documentType,
                documentNumber: data.documentNumber,
                reasonName: data.reasonName,
                addressHouse: data.addressHouse,
                countryResidence: data.countryResidence,
                cityResidence: data.cityResidence,
                department: data.department,
                email: data.email,
                telephone: data.telephone,
                cellphone: data.cellphone
            });
            this.messageService.add({severity: 'success', summary: 'Agregado Exítosamente', detail: 'Exítoso'});
        }
    }

    onHideRole() {
        this.isEditing = false
        this.itemInfoLegalRole = null
    }

    showDialogRole() {
        this.displayRole = !this.displayRole;
    }

    moreDetailTable(headers: any[] = [], list: any[] = []) {
        this.headers = headers;
        this.list = list;
        this.isActivateMoreData = true;
    }
    onHideDataMore() {
        this.isActivateMoreData = false;
    }
}
