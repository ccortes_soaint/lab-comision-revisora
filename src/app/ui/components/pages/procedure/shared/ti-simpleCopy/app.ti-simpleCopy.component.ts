import {Component, OnInit} from '@angular/core';

import {FormBuilder, FormGroup} from "@angular/forms";
import {ParameterDTO} from "../../../../../../domain/model/dto/comunes/parameterDTO";

@Component({
    selector: 'app-ti-simpleCopy',
    templateUrl: './app.ti-simpleCopy.component.html'
})
export class AppTiSimpleCopyComponent implements OnInit{


    procedureCategoryLs: ParameterDTO[];
    form: FormGroup;
    typeDocumentFilterLs: ParameterDTO;
    resultValuesFiltered: ParameterDTO;
    selectedRow: ParameterDTO;


    constructor(private formBuilder: FormBuilder) {
    }

    ngOnInit() {

        this.initForm();
    }

    saveDataFilter() {
    }

    initForm() {
        this.form = this.formBuilder.group({
            documentType: [[]],
            documentNumber: [''],
            autoNumber: [''],
            proceedingsNumber: ['']
        });
    }

    search() {



    }


}
