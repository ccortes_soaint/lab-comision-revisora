import {Component, OnInit, Input} from '@angular/core';
import {ParameterDTO} from 'src/app/domain/model/dto/comunes/parameterDTO';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";


@Component({
    selector: 'app-ti-pharmacological-evaluation-seven',
    templateUrl: './app.ti-pharmacological-evaluation-seven.component.html'
})
export class TiPharmacologicalEvaluationSevenComponent implements OnInit {

    head: any[] = [];
    results: any[]=[];
    list: any;
    form: FormGroup;
    procedureCategoryLs: ParameterDTO[];



    constructor(private formBuilder: FormBuilder) {
    }

    ngOnInit(): void {

        this.initForm();

        this.list = [
            {name: 'option 1', code: 'op1'},
            {name: 'Option 2', code: 'op2'},
            {name: 'option 3', code: 'op3'},
            {name: 'option 4', code: 'op4'},
            {name: 'option 5', code: 'op5'}

        ]
    }


    initForm() {

        this.form = this.formBuilder.group({
            'risk': [null, Validators.required],
            'additional': [null, Validators.required],
            'routine': [null, Validators.required],
            'routineGoal': [null, Validators.required],
            'additionalGoal': [null, Validators.required],
            'typeRisk': [null, Validators.required],
            'descriptionPlans': [null, Validators.required],
            'folio': [null, Validators.required]

        });
    }

    add() {
        if (this.form.valid) {
            this.results.push(this.form.value)
            this.form.reset();
        }

        console.log(this.form)
    }

}
