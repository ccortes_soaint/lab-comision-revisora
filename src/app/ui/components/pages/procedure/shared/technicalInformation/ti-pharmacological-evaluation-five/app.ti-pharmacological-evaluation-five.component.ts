import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

import {FormBuilder, FormGroup, Validators} from "@angular/forms";

import {SelectItem} from 'primeng';
import {DATE_ES} from "../../../../../app.ui.environment";
import {ParameterDTO} from "../../../../../../../domain/model/dto/comunes/parameterDTO";


@Component({
    selector: 'app-ti-pharmacological-evaluation-five',
    templateUrl: './app.ti-pharmacological-evaluation-five.component.html'
})
export class TiPharmacologicalEvaluationFiveComponent implements OnInit {

    es = DATE_ES;
    form: FormGroup;
    items: SelectItem[];
    firstAuthorization: Date;
    firstCommercialization: Date;
    pgrCut: Date = new Date();
    countryLs: ParameterDTO[];
    procedureCategoryLs: ParameterDTO[];
    countryCommercialization: ParameterDTO[];
    @Output() formFarmacologiaFive: EventEmitter<any> = new EventEmitter<any>();

    constructor(private formBuilder: FormBuilder) {
        this.countryLs = [
            {name: 'option 1', code: 'op1'},
            {name: 'Option 2', code: 'op2'},
            {name: 'option 3', code: 'op3'},
            {name: 'option 4', code: 'op4'},
            {name: 'option 5', code: 'op5'}
        ];
        this.countryCommercialization = [
            {name: 'option 1', code: 'op1'},
            {name: 'Option 2', code: 'op2'},
            {name: 'option 3', code: 'op3'},
            {name: 'option 4', code: 'op4'},
            {name: 'option 5', code: 'op5'}
        ];
        this.items = [];
        for (let i = 0; i < 10000; i++) {
            this.items.push({label: 'Item ' + i, value: 'Item ' + i});
        }
    }

    ngOnInit(): void {
        this.initForm();
    }

    initForm() {
        this.form = this.formBuilder.group({
            'grupoFTatc': [null, Validators.required],
            'folioFTatc': [null, Validators.required],
            'mechanism': [null, Validators.required],
            'folioMechanism': [null, Validators.required],
            'firstAuthorization': [null, Validators.required],
            'countryAuthorization': [null, Validators.required],
            'folio1': [null, Validators.required],
            'firstCommercialization': [null, Validators.required],
            'countryCommercialization': [null, Validators.required],
            'folio2': [null, Validators.required],
            'tradeName': [null, Validators.required],
            'folio3': [null, Validators.required],
            'pgrCut': [this.pgrCut, Validators.required],
            'folio4': [null, Validators.required],
            'groupPgr': [null, Validators.required],
            'folio5': [null, Validators.required],
            'pgrVersion': [null, Validators.required]
        });
        this.formFarmacologiaFive.emit(this.form.value);
    }

    formFive() {
        console.log(this.form.value);
        if (this.form.valid){
            return this.form.value;
        }else {
            return false;
        }

    }
}
