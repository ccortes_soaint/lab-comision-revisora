import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-ti-pharmacological-evaluation-eight',
  templateUrl: './ti-pharmacological-evaluation-eight.component.html',
  styles: []
})
export class TiPharmacologicalEvaluationEightComponent implements OnInit {

    head: any[] = [];
    results: any[] = [];
    list: any;
    form: FormGroup;
  constructor(private formBuilder: FormBuilder) {
      /*this.results = [
          {studyTitle: 'xxxxx',summaryStudies: 'xxxxxxx',Invoice:'xxxxxxx'}
      ]*/
  }

  ngOnInit(): void {
      this.initForm();

      this.list = [
          {name: 'option 1', code: 'op1'},
          {name: 'Option 2', code: 'op2'},
          {name: 'option 3', code: 'op3'},
          {name: 'option 4', code: 'op4'},
          {name: 'option 5', code: 'op5'}

      ]
  }

    onBasicUpload($event: any) {

    }
    initForm() {

        this.form = this.formBuilder.group({
            'risk': [null, Validators.required],
            'typerisk': [null, Validators.required],
            'activityMr': [null, Validators.required],
            'activityAdit': [null, Validators.required],
            'objetActivity': [null, Validators.required],
            'objetActivityAdit': [null, Validators.required],
            'actions': [null, Validators.required],


        });
    }
    add() {
        if (this.form.valid) {
            this.results.push(this.form.value)
            this.form.reset();
        }

        console.log(this.form)
    }
}
