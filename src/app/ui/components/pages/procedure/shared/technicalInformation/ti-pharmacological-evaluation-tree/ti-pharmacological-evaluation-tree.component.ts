import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {FileUpload} from "primeng";

@Component({
  selector: 'app-ti-pharmacological-evaluation-tree',
  templateUrl: './ti-pharmacological-evaluation-tree.component.html',
  styles: []
})
export class TiPharmacologicalEvaluationTreeComponent implements OnInit {

    option: any[];
    head: any[] = [];
    results: any[] = [];
    formClinicalStudy: FormGroup;
  constructor(private formBuilder: FormBuilder,
              private _changeDetectorRef: ChangeDetectorRef,) {
      this.option = [
      {name: 'option 1', code: 'op1'},
      {name: 'option 2', code: 'op1'},
      {name: 'option 3', code: 'op1'},
      {name: 'option 4', code: 'op1'},
  ]
      this.results = [
          {parameter: 'Seguridad',phase: 'xxxxxxx',title:'xxxxxxx', Invoice:'xxxxx'},
          {parameter: 'Eficacia',phase: 'xxxxxxx',title:'xxxxxxx', Invoice:'xxxxx'},
          {parameter: 'Farmacocinética',phase: 'xxxxxxx',title:'xxxxxxx', Invoice:'xxxxx'}
      ]}

  ngOnInit(): void {
      this.initForm();
  }

    private initForm() {
        this.formClinicalStudy = this.formBuilder.group({
            parameter: [null, Validators.required],
            phase: [null, Validators.required],
            intention: [null, Validators.required],
            summary: [null, Validators.required],
            invoice: [null, Validators.required],
        })
    }

    add(event?, anexoUploader?: FileUpload) {
        console.log(this.formClinicalStudy)
        if (this.formClinicalStudy.valid){
        this.addTable(event)
        }
        this.formClinicalStudy.reset();
        return true;
    }

    private addTable(event, anexoUploader?: FileUpload) {
        const form = {
            parameter: this.formClinicalStudy.get("parameter"),
            phase: this.formClinicalStudy.get("phase"),
            intention: this.formClinicalStudy.get("intention"),
            summary: this.formClinicalStudy.get("summary"),
            invoice: this.formClinicalStudy.get("invoice"),
        }
        this.addTableList(form);
        this._changeDetectorRef.detectChanges();
    }

    private addTableList(form: any) {
        this.results = [...this.results, ...[form]];
    }
}
