import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ParameterDTO} from "../../../../../../../domain/model/dto/comunes/parameterDTO";
import {SelectItem} from "primeng";

@Component({
  selector: 'app-ti-pharmacological-evaluation',
  templateUrl: './ti-pharmacological-evaluation.component.html',
  styles: []
})
export class TiPharmacologicalEvaluationComponent implements OnInit {
    form: FormGroup;
    list:  ParameterDTO[];
    items: SelectItem[];
    nameAttach: any;
     pdfFileBase;
     uploadedFiles:any;
     listaVersionesDocumento: any;

  constructor(private formBuilder: FormBuilder) {
      this.list = [
          { name: 'option 1', code: 'op1' },
          { name: 'Option 2', code: 'op2' },
          { name: 'option 3', code: 'op3' },
          { name: 'option 4', code: 'op4' },
          { name: 'option 5', code: 'op5' }
      ];
      this.items = [];
      for (let i = 0; i < 10000; i++) {
          this.items.push({ label: 'Item ' + i, value: 'Item ' + i });
      }
  }

  ngOnInit(): void {
      this.initForm();
  }
    initForm() {
        this.form = this.formBuilder.group({


            'indications': [null, Validators.required],
            'contraindications': [null, Validators.required],
            'precautions': [null, Validators.required],
            'reactions': [null, Validators.required],
            'interactions': [null, Validators.required],
            'version': [null, Validators.required],
            'referenceProduct': [null, Validators.required]


        });
    }

    toAttach(event) {
        if (event.files.length === 0) {
            return false;
        }
        this.uploadedFiles = [
            {
                id: this.listaVersionesDocumento.length > 0 ? this.listaVersionesDocumento[this.listaVersionesDocumento.length - 1].id : null,
                nombre: encodeURI(event.files[0].name),
                tipo: 'pdf',
                version: '',
                contenido: '',
                file: event.files[0],
                size: event.files[0].size
            },

        ];

        event.files.forEach((page) => {
            const reader = new FileReader();
            reader.readAsDataURL(page);
            reader.onload = () => {
                this.pdfFileBase = reader.result;
                console.log(this.pdfFileBase);
                console.log(this.uploadedFiles);
                this.nameAttach = this.uploadedFiles[0].file.name;
            };
        });
    }
}
