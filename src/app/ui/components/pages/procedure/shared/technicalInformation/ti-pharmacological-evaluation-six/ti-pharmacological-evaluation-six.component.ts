import { Component, OnInit } from '@angular/core';
import {INFORMACIONPRODUCTO} from "../../../../../../../domain/model/mocks/informacionProductoMock";

@Component({
  selector: 'app-ti-pharmacological-evaluation-six',
  templateUrl: './ti-pharmacological-evaluation-six.component.html',
  styles: []
})
export class TiPharmacologicalEvaluationSixComponent implements OnInit {

    informacionProduc: any;
  constructor() { }

  ngOnInit(): void {
        this.informacionProduc = INFORMACIONPRODUCTO;
        console.log(this.informacionProduc);
  }

}
