import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { SelectItem } from 'primeng';
import {ParameterDTO} from "../../../../../../../domain/model/dto/comunes/parameterDTO";


@Component({
  selector: 'app-ti-pharmacological-evaluation-two',
  templateUrl: './app.ti-pharmacological-evaluation-two.component.html'
})
export class TiPharmacologicalEvaluationTwoComponent implements OnInit {

  // form: FormGroup;
  // tittle: string;
  // folio: string;
  // studySummary: string;
  // items: SelectItem[];
  // countryLs: ParameterDTO[];
  requests: any[] = [];
  generateConcept = false;
  procedureCategoryLs: ParameterDTO[];

  // @Input() isRequest = true;
  // tableCols: any;
  // tableData: any;

  constructor(private formBuilder: FormBuilder) {
    // this.countryLs = [
    //   { name: 'option 1', code: 'op1' },
    //   { name: 'Option 2', code: 'op2' },
    //   { name: 'option 3', code: 'op3' },
    //   { name: 'option 4', code: 'op4' },
    //   { name: 'option 5', code: 'op5' }
    // ];
    // this.items = [];
    // for (let i = 0; i < 10000; i++) {
    //   this.items.push({ label: 'Item ' + i, value: 'Item ' + i });
    // }
  }

  ngOnInit(): void {

    this.requests = values;
    

    // this.initForm();

    // this.tableCols = [
    //   { header: 'Parametro' },
    //   { header: 'Título' },
    //   { header: 'Resumen de los estudios' },
    //   { header: 'Folio' },
    //   { header: 'Acciones' },
    // ];

    // this.tableData = [
    //   {
    //     parameter: 'xxxx',
    //     tittles: 'xxxx',
    //     studySummary: 'xxxx',
    //     folio: 'xxxx',
    //     actions: 'xxxx'
    //   },
    //   {
    //     parameter: 'xxxx',
    //     tittles: 'xxxx',
    //     studySummary: 'xxxx',
    //     folio: 'xxxx',
    //     actions: 'xxxx'
    //   }
    // ];
  }

  // initForm() {
  //   this.form = this.formBuilder.group({
  //     'parametro': [{ value: null }, Validators.required],
  //     'tittle': [{ value: null }, Validators.required],
  //     'studySummary': [{ value: null }, Validators.required],
  //     'folio': [{ value: null }, Validators.required],
  //   });
  // }
}

export let values = [
  {
    parameter: 'Farmacodinamia',
    tittle: 'xxxx',
    folio: 'xxxx'
  },
  {
    parameter: 'Farmacocinética',
    tittle: 'xxxx',
    folio: 'xxxx'
  },
  {
    parameter: 'Toxicidad',
    tittle: 'xxxx',
    folio: 'xxxx'
  },
  {
    parameter: 'Toxicidad agua estudios In vivo',
    tittle: 'xxxx',
    folio: 'xxxx'
  },
  {
    parameter: 'Toxicidad sub-aguda estudios In vivo',
    tittle: 'xxxx',
    folio: 'xxxx'
  },
  {
    parameter: 'Toxicidad crónica estudios In vivo',
    tittle: 'xxxx',
    folio: 'xxxx'
  },
  {
    parameter: 'Toxicidad reproductiva estudios In vivo',
    tittle: 'xxxx',
    folio: 'xxxx'
  },
  {
    parameter: 'Genotixidad ensayos In vivo',
    tittle: 'xxxx',
    folio: 'xxxx'
  },
  {
    parameter: 'Carcinogénesis y mutagénesis',
    tittle: 'xxxx',
    folio: 'xxxx'
  },
  {
    parameter: 'Inmunotoxicidad y reactogenecidad',
    tittle: 'xxxx',
    folio: 'xxxx'
  },
  {
    parameter: 'Seguridad viral',
    tittle: 'xxxx',
    folio: 'xxxx'
  },
  {
    parameter: 'Estudios de tolerancia local',
    tittle: 'xxxx',
    folio: 'xxxx'
  }
];