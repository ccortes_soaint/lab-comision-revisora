import { Component, Input, OnInit } from '@angular/core';
import { SelectItem } from 'primeng';
import { ParameterDTO } from 'src/app/domain/model/dto/comunes/parameterDTO';

@Component({
  selector: 'ti-medicines',
  templateUrl: './ti-medicines.html',
})
export class TiMedicinesComponent implements OnInit {

  unit: any[];
  option: any[];
  measure: any[];
  reference: any[];
  cars: SelectItem[];
  items: SelectItem[];
  requests: any[] = [];
  requests2: any[] = [];
  requests3: any[] = [];
  activePrinciple: any[];
  generateConcept = false;
  generateConcept2 = false;
  generateConcept3 = false;
  adminRoute: SelectItem[];
  @Input() isRequest = true;
  selectedCars1: string[] = [];
  procedureCategoryLs: ParameterDTO[];

  constructor() {
    this.adminRoute = [
      { label: 'Via de administración 1', value: 'Via de administración 1' },
      { label: 'Via de administración 2', value: 'Via de administración 2' }
    ]
    this.option = [
      { name: 'option 1', code: 'option 1' },
      { name: 'option 2', code: 'option 2' },
      { name: 'option 3', code: 'option 3' },
      { name: 'option 4', code: 'option 4' },
    ]
    this.unit = [
      { name: 'option 1', code: 'option 1' },
      { name: 'option 2', code: 'option 2' },
      { name: 'option 3', code: 'option 3' },
      { name: 'option 4', code: 'option 4' },
    ]
    this.reference = [
      { name: 'option 1', code: 'option 1' },
      { name: 'option 2', code: 'option 2' },
      { name: 'option 3', code: 'option 3' },
      { name: 'option 4', code: 'option 4' },
    ]
    this.measure = [
      { name: 'option 1', code: 'option 1' },
      { name: 'option 2', code: 'option 2' },
      { name: 'option 3', code: 'option 3' },
      { name: 'option 4', code: 'option 4' },
    ]
    this.activePrinciple = [
      { name: 'option 1', code: 'option 1' },
      { name: 'option 2', code: 'option 2' },
      { name: 'option 3', code: 'option 3' },
      { name: 'option 4', code: 'option 4' },
    ]
    this.items = [];
    for (let i = 0; i < 10000; i++) {
      this.items.push({ label: 'Item ' + i, value: 'Item ' + i });
    }
  }

  ngOnInit(): void {
    this.requests = values;
    this.requests2 = values2;
    this.requests3 = values3;
  }

  openMod(numberTable) {
    if (numberTable == 1) {
      this.generateConcept = true;  
    } if (numberTable == 2) {
      this.generateConcept2 = true;
    } else {
      this.generateConcept3 = true;      
    }
  }

  delete(index, numberTable) {
    if (numberTable == 1) {
      values.splice(index, 1);
    } if (numberTable == 2) {
      values2.splice(index, 1);
    } else {
      values3.splice(index, 1);
    }
  }

  change() {
    console.log(this.items);
  }
}

export let values = [
  {
    settledNum: 'Caja con 1 vial de vidrio tipo 1 de 50mg/10mg'
  },
  {
    settledNum: 'Muestra médica: Caja con 1 vial de vidrio tipo 1 de 50mg/10ml'
  }
];

export let values2 = [
  {
    activePrinciple: 'xxx',
    reason: 'xxx',
    quantity: 'xxxx',
    measure: 'xxxx',
    referenceUnit: 'xxx'
  },
  {
    activePrinciple: 'xxx',
    reason: 'xxx',
    quantity: 'xxxx',
    measure: 'xxxx',
    referenceUnit: 'xxx'
  }
]

export let values3 = [
  {
    component: 'xxx',
    quantity2: 'xxxx',
    measure2: 'xxxx'
  },
  {
    component: 'xxx',
    quantity2: 'xxxx',
    measure2: 'xxxx'
  }
]
