import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
    selector: 'app-ti-pharmacological-evaluation-four',
    templateUrl: './ti-pharmacological-evaluation-four.component.html',
    styles: []
})
export class TiPharmacologicalEvaluationFourComponent implements OnInit {
    form: FormGroup;

    tableData: any;
    listaProyectores: any[] = [];
    pharmaceutical: any;
    specialCondition: any;
    uploadedFiles;
    listaVersionesDocumento: any[] = [];
    pdfFileBase;
    nameFile: any;
    IPP: any;
    nameAttach: any;
    formATC: FormGroup;
    formContainerInformation: FormGroup;
    hasATC: boolean;
    selectATC: any;
    checkProducts: any;
    insert: any;
    selectPrescriptionInfo: any;
    selectContainerType: any;


    constructor(private formBuilder: FormBuilder) {
    }

    ngOnInit(): void {
        this.initForm();

        this.pharmaceutical = [
            {id: 1, name: 'option 1', code: 'op1'},
            {id: 2, name: 'option 2', code: 'op2'},
            {id: 3, name: 'option 3', code: 'op3'},
            {id: 4, name: 'option 4', code: 'op4'},
            {id: 5, name: 'option 5', code: 'op5'},
            {id: 5, name: 'Otro', code: 'OTRO'},
        ]
        this.specialCondition = [
            {id: 1, name: 'Especial', code: 'ES'},
            {id: 2, name: 'Control Especial', code: 'CES'},
            {id: 3, name: 'Control Esoecial - Escemcial', code: 'CEE'},
            {id: 4, name: 'Ninguna', code: 'NI'},
        ];
        this.IPP = [
            {id: 1, name: 'SI', code: 'SI'},
            {id: 2, name: 'NO', code: 'NO'},
            {id: 3, name: 'Otro', code: 'OTRO'},
        ];

        this.tableData = [
            {
                titleStudy: 'xxxx',
                parameter: 'xxxx',
                title: 'xxxx',
                resume: 'xxxx',
                folio: 'xxxx',
                actions: 'xxxx'
            },
            {
                titleStudy: 'xxxx',
                parameter: 'xxxx',
                title: 'xxxx',
                resume: 'xxxx',
                folio: 'xxxx',
                actions: 'xxxx'
            }

        ];
    }

    initForm() {
        this.form = this.formBuilder.group({
            'pharmaceutical': [null, Validators.required],
            'specialCondition': [null, Validators.required],
            'finishedProduct': [null, Validators.required],
            'pharmaceuticalIngredient': [null, Validators.required],
            'storageCondition': [null, Validators.required],
            'youProduct': [null],
            'solventLife': [null, Validators.required],
            'lifeCondition': [null, Validators.required],
            'hasInsert': [null],
            'version': [null, Validators.required],
            'prescriptionInformation': [null, Validators.required],
            'which': [null],
            'version2': [null, Validators.required],

        });
        this.formContainerInformation = this.formBuilder.group({
            'containerType': [null, Validators.required],
            'cual': [null],
            'technicalDocument': [null],
        });
        this.formATC = this.formBuilder.group({
            'hasATC': [null],
            'ATC': [null, Validators.required],
            'description': [null, Validators.required],
        });
    }

    add() {
        if (this.formContainerInformation.valid) {
            this.listaProyectores.push(this.formContainerInformation.value);
            this.formContainerInformation.reset();
        }

    }
    attachDocument(event) {
        if (event.files.length === 0) {
            return false;
        }
        this.uploadedFiles = [
            {
                id: this.listaVersionesDocumento.length > 0 ? this.listaVersionesDocumento[this.listaVersionesDocumento.length - 1].id : null,
                nombre: encodeURI(event.files[0].name),
                tipo: 'pdf',
                version: '',
                contenido: '',
                file: event.files[0],
                size: event.files[0].size
            },

        ];

        event.files.forEach((page) => {
            const reader = new FileReader();
            reader.readAsDataURL(page);
            reader.onload = () => {
                this.pdfFileBase = reader.result;
                console.log(this.pdfFileBase);
                console.log(this.uploadedFiles);
                this.nameFile = this.uploadedFiles[0].file.name;
            };
        });
    }

    toAttach(event) {
        if (event.files.length === 0) {
            return false;
        }
        this.uploadedFiles = [
            {
                id: this.listaVersionesDocumento.length > 0 ? this.listaVersionesDocumento[this.listaVersionesDocumento.length - 1].id : null,
                nombre: encodeURI(event.files[0].name),
                tipo: 'pdf',
                version: '',
                contenido: '',
                file: event.files[0],
                size: event.files[0].size
            },

        ];

        event.files.forEach((page) => {
            const reader = new FileReader();
            reader.readAsDataURL(page);
            reader.onload = () => {
                this.pdfFileBase = reader.result;
                console.log(this.pdfFileBase);
                console.log(this.uploadedFiles);
                this.nameAttach = this.uploadedFiles[0].file.name;
            };
        });
    }

    format() {
        console.log(this.formATC);
        console.log(this.hasATC);
    }

    check(event) {
        console.log(event.checked);
        this.hasATC = event.checked;
    }

    atc(event) {
        console.log(event.value.code);
        this.selectATC = event.value.code;
    }

    checkProduct(event) {
        console.log(event.checked)
        this.checkProducts = event.checked
    }

    checkInsert(event) {
        this.insert = event.checked;
    }

    selectPrescription(event) {
        this.selectPrescriptionInfo = event.value.code
    }

    containerType(event) {
        this.selectContainerType = event.value.code;
    }
}
