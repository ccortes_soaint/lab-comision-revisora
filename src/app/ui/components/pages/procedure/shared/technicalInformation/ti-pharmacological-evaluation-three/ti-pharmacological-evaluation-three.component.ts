import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {FileUpload} from "primeng";

@Component({
  selector: 'app-ti-pharmacological-evaluation-tree',
  templateUrl: './ti-pharmacological-evaluation-three.component.html',
  styles: []
})
export class TiPharmacologicalEvaluationThreeComponent implements OnInit {

    option: any[];
    head: any[] = [];
    results: any[] = [];
    formClinicalStudy: FormGroup;
  constructor(private formBuilder: FormBuilder,
              private _changeDetectorRef: ChangeDetectorRef,) {
      this.option = [
      {name: 'option 1', code: 'op1'},
      {name: 'option 2', code: 'op1'},
      {name: 'option 3', code: 'op1'},
      {name: 'option 4', code: 'op1'},
  ]
      this.results = [
          {parameter: 'xxxxx',phase: 'xxxxxxx',title:'xxxxxxx', studentSummary:'', Invoice:''}
      ]}

  ngOnInit(): void {
      this.initForm();
  }

    private initForm() {
        this.formClinicalStudy = this.formBuilder.group({
            parameter: [null, Validators.required],
            phase: [null, Validators.required],
            intention: [null, Validators.required],
            summary: [null, Validators.required],
            invoice: [null, Validators.required],
        })
    }

    add(event?, anexoUploader?: FileUpload) {
        console.log(this.formClinicalStudy)
        if (this.formClinicalStudy.valid){
        this.addTable(event)
        }
        this.formClinicalStudy.reset();
        return true;
    }

    private addTable(event, anexoUploader?: FileUpload) {
        const form = {
            parameter: this.formClinicalStudy.get("parameter"),
            phase: this.formClinicalStudy.get("phase"),
            intention: this.formClinicalStudy.get("intention"),
            summary: this.formClinicalStudy.get("summary"),
            invoice: this.formClinicalStudy.get("invoice"),
        }
        this.addTableList(form);
        this._changeDetectorRef.detectChanges();
    }

    private addTableList(form: any) {
        this.results = [...this.results, ...[form]];
    }
}
