import {Component} from '@angular/core';
import {NgForm} from "@angular/forms";
import {ParameterDTO} from "../../../../../../domain/model/dto/comunes/parameterDTO";

@Component({
    selector: 'app-ti-medicalDevices',
    templateUrl: './app.ti-medicalDevices.component.html'
})
export class AppTiMedicalDevicesComponent{

    deviceClasificationLs: ParameterDTO;
    deviceClassification: ParameterDTO;
    productName: string;
    usefulLife: string;
    internationalCode: string;
    genericName: string;
    mark: string;
    indications: string;
    commercialPresentation: string;
    inputDevicePart: string;
    inputQualitativeComposition: string;
    dataComponents: any[] = [];
    editDataComponents: any[] = [];
    showPopUpComponents: boolean;
    showPopUpReferences: boolean;
    inputFamily: string;
    inputCode: string;
    inputDescription: string;
    dataReferences: any[] = [];
    editDataReferences: any[] = [];
    observations: string;
    editDataIndexComponents: number;
    editDataIndexReferences: number;

    constructor() {
    }

    //dataComponents ===================================================================================================

    saveDataComponents(submitDataComponents: NgForm) {

        this.dataComponents.push(submitDataComponents.value);

        this.inputDevicePart = '';
        this.inputQualitativeComposition = '';

    }
    popUpEditDataComponents(itemComponent, indexComponent) {

        this.showPopUpComponents = true;

        this.editDataIndexComponents = null;

        this.editDataComponents = itemComponent;
        this.editDataIndexComponents = indexComponent;
        this.editDataComponents[0] = this.editDataIndexComponents;

    }
    saveEditDataComponents(submitEditDataComponents: NgForm){

        const result = this.dataComponents.filter(data => data.index == this.editDataComponents[0]);

        this.dataComponents[this.editDataIndexComponents] = submitEditDataComponents.value;

        this.showPopUpComponents = false;

    }
    deleteDataComponents(index) {
        this.dataComponents.splice(index, 1);
    }

    //dataReferences ===================================================================================================

    saveDataReferences(submitDataReferences: NgForm) {

        this.dataReferences.push(submitDataReferences.value);

        this.inputFamily = '';
        this.inputCode = '';
        this.inputDescription = '';

    }
    popUpEditDataReferences(itemReferences,indexReferences){

        this.showPopUpReferences = true;

        this.editDataIndexReferences = null;

        this.editDataReferences = itemReferences;
        this.editDataIndexReferences = indexReferences;
        this.editDataReferences[0] = this.editDataIndexReferences;

    }

    saveEditDataReferences(submitEditDataReferences: NgForm) {

        const result = this.dataReferences.filter(data => data.index == this.editDataIndexReferences[0]);
        console.log(result);
        this.dataReferences[this.editDataIndexReferences] = submitEditDataReferences.value;

        this.showPopUpReferences = false;

    }

    deleteDataReferences(index){
        this.dataReferences.splice(index, 1);
    }


}

