import {Component} from "@angular/core";
import {HEADERSRATE, VALUESRATE} from "../../../../../../domain/model/mocks/generalRateMock";

@Component({
    selector: 'app-rateInformation',
    templateUrl: './app.rateInformation.component.html'
})
export class AppRateInformationComponent{


    headerTable: any;//cabeceras de la tabla
    dataTable: any;//datos de la tabla
    totalValue:number[]=[];//variable que trae el valor de la cantidad

    constructor() {
        this.headerTable = HEADERSRATE;
        this.dataTable = VALUESRATE;

    }
    calculateValue(index: number, item: any) { //calcular datos del input cantidad editable guardando en variable global
        this.totalValue[index]=0;
        this.totalValue[index] = this.dataTable[index].valor * item;
    }

}
