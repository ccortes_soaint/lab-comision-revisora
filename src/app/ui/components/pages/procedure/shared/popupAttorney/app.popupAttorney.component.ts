import {Component, OnInit, EventEmitter, Output, Input, OnChanges, SimpleChanges} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-popup-attorney',
  templateUrl: './app.popupAttorney.component.html',
  styles: []
})
export class AppPopupAttorneyComponent implements OnInit, OnChanges{

    items = [];
    public formInfoLegalAttorney: FormGroup;
    @Output("getData") public submit: EventEmitter<any> = new EventEmitter<any>();
    @Input("infoLegal") public _infoLegalItem: any;
    @Input("isEditing") public isEditing: boolean = false;
    private idCustomItem: number = -1;
    public display: boolean;

    constructor(private fb: FormBuilder) {


        for (let i = 0; i < 10; i++) {
            this.items.push({label: 'Item ' + i, value: 'Item ' + i});
        }
    }

    get infoLegalItem() {

        if (!this._infoLegalItem) {

            return {};

        }
        return this._infoLegalItem;
    }

    ngOnInit() {

        this.formInfoLegalAttorney = this.fb.group({

            userType: this.fb.control(!this._infoLegalItem ? '' : this._infoLegalItem.userType, Validators.compose([Validators.required])),
            documentType: this.fb.control(!this._infoLegalItem ? '' : this._infoLegalItem.documentType, Validators.compose([Validators.required])),
            documentNumber: this.fb.control(!this._infoLegalItem ? '' : this._infoLegalItem.documentNumber, Validators.compose([Validators.required])),
            reasonName: this.fb.control(!this._infoLegalItem ? '' : this._infoLegalItem.reasonName, Validators.compose([Validators.required])),
            stateInformation: this.fb.control(!this._infoLegalItem ? '' : this._infoLegalItem.stateInformation, Validators.compose([Validators.required]))
        });
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.isEditing && changes.isEditing.currentValue) {
            const editItem = changes._infoLegalItem ? changes._infoLegalItem.currentValue : null
            if (!!editItem) {
                this.idCustomItem = editItem.documentNumber;
                this.formInfoLegalAttorney.markAllAsTouched()
                this.formInfoLegalAttorney.setValue({
                    userType: editItem.userType,
                    documentType: editItem.documentType,
                    documentNumber: editItem.documentNumber,
                    reasonName: editItem.reasonName,
                    stateInformation: editItem.stateInformation
                });
                this.formInfoLegalAttorney.get('documentNumber').disable();
            }
        } else if (this.formInfoLegalAttorney) {
            this.formInfoLegalAttorney.reset()
            this.formInfoLegalAttorney.get('documentNumber').enable();
        }
    }


    onSubmit(form: FormGroup) {
        if (form.valid) {
            if (this.isEditing) {
                this.submit.emit({...form.value, documentNumber:this.idCustomItem});
            }else{
                this.submit.emit(form.value);
            }
            this.formInfoLegalAttorney.reset()
        } else {

            alert("Formulario  invalido")
        }
    }


}
