import { Component } from '@angular/core';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';

@Component({
    selector: 'app-dashBoard',
    templateUrl: './app.dashBoard.component.html',
})
export class AppDashBoardComponent {
    Editor = ClassicEditor;
    dataEditor: string = '<div>Hello World!</div><div> ckeditor <b>Editor</b> Rocks</div><div><br></div>';
    text1: string = '<div>Hello World!</div><div>PrimeNG <b>Editor</b> Rocks</div><div><br></div>';

    text2: string;



    constructor() { }
}
