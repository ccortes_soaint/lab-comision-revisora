import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from 'src/app/infraestructure/services/auth/authGuard.service';
import { HEALTH_RECORD_PATH } from '../healthRecord/app.healthRecord.routing.name';
import { RequestHearingComponent } from './request-hearing.component';


const routes: Routes = [
  { path: '', redirectTo: HEALTH_RECORD_PATH.assignedWork.value, pathMatch: 'full' },
  {
      path: '',
      component: RequestHearingComponent,
      canActivateChild: [AuthGuardService],
      data: {
          breadcrumb: HEALTH_RECORD_PATH.assignedWork.value
      },
      children:[]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RequestHearingRoutingModule { }
