import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RequestHearingRoutingModule } from './request-hearing-routing.module';
import { RequestHearingComponent } from './request-hearing.component';
import { ListInputsModule } from '../unification/shared/list-inputs/list-inputs.module';
import { AppInquiriesModule } from '../inquiries/app.inquiries.module';
import { PRIMENG_MODULES } from 'src/app/shared/enums/primeng.elements';
import { ReactiveFormsModule } from '@angular/forms';
import { AppSelectRoomModule } from '../inquiries/select-room/app.select-room.module';


@NgModule({
  declarations: [RequestHearingComponent],
  imports: [
    CommonModule,
    RequestHearingRoutingModule,
    ...PRIMENG_MODULES,
    ListInputsModule,
    AppInquiriesModule,
    ReactiveFormsModule,
    FormsModule,
    AppSelectRoomModule 
  ],
  exports: [RequestHearingComponent]
})
export class RequestHearingModule { }
