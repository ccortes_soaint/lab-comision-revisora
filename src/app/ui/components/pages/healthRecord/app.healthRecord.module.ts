import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PRIMENG_MODULES } from 'src/app/shared/enums/primeng.elements';
import { AppUIModule } from '../../app.ui.module';
import { LayoutModule } from '../../layouts/layout.module';
import { AppHealthRecordComponent } from './app.healthRecord.component';
import { HealthRecordRoutingModule } from './app.healthRecord.route';
import { AppAssignedWorkComponent } from './assignedWork/app.assignedWork.component';
import { AppViewHealthRecordComponent } from './viewHealthRecord/app.viewHealthRecord.component';
import {NgxSpinnerModule} from "ngx-spinner";
import {AppReassignTasksComponent} from "./reassignTasks/app.reassignTasks.component";

@NgModule({
    declarations: [
        AppHealthRecordComponent,
        AppAssignedWorkComponent,
        AppViewHealthRecordComponent,
        AppReassignTasksComponent
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        LayoutModule,
        ...PRIMENG_MODULES,
        HealthRecordRoutingModule,
        AppUIModule,
        NgxSpinnerModule
    ]
})
export class HealthRecordModule { }
