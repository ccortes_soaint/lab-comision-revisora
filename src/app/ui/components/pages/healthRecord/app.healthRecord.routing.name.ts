export const HEALTH_RECORD_PATH = {
    assignedWork: { value: 'requestHearing', name: 'Solicitud Audiencia' },
    viewHealthRecord: { value: 'unification', name: 'Unificacion' },
    reassignTasks: { value: 'clarifications', name: 'Aclaraciones' },
    manageAssignedTask: { value: 'AdmTareaAsignada', name: 'Administrar Tarea Asignada' }
}
