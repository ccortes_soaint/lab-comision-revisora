import { Component, OnInit, ViewChild } from '@angular/core';
import { AppAssignedProceduresComponent } from "../../../tools/HealthRegister/app.assigned-procedures/app.assigned-procedures.component";
import { SpinnerService } from "../../../../../infraestructure/services/spinner/spinner.service";
import { ObtenerServiceExampleService } from "../../../../../infraestructure/services/spinner/obtener-service-example.service";

@Component({
    selector: 'app-assignedWork',
    templateUrl: './app.assignedWork.component.html',
})
export class AppAssignedWorkComponent implements OnInit {

    tabIndex = 0;
    activeIndex: number = 0;
    procedures: any = [];
    showTable: true;
    display: boolean = false;
    options: any = [];

    @ViewChild('data') data: AppAssignedProceduresComponent;
    constructor(private addCountry: ObtenerServiceExampleService,
        private spinnerService: SpinnerService,) {

        this.options = [
            { label: '...', value: null },
            { label: 'Consultas', value: { id: 1, name: 'consultas' } },
            { label: 'Aclaraciones', value: { id: 2, name: 'aclaraciones' } },
            { label: 'Unificaciones', value: { id: 3, name: 'unificaciones' } },
        ];
    }

    ngOnInit() {
        this.addCountry.getPaises().subscribe(res => {
            console.log(res);
            this.procedures = res
        }
        )
        // this.procedures = [
        //     {number: 'xxx', filingDate: '', type: '', productType:'', group:''},
        //     {number: 'xxx', filingDate: '', type: '', productType:'', group:''},
        //     {number: 'xxx', filingDate: '', type: '', productType:'', group:''},
        // ];
    }

    showViewTable() {
        return this.data.getShow;
    }

    sandviewTaskAssingned(event) {
        this.showTable = event;
    }
}
