import { Component } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { MenuItem } from 'primeng';
import { filter } from 'rxjs/operators';
import { MenuService } from 'src/app/infraestructure/services/menu/app.menu.service';
import { UI_ROUTES_PATH } from '../../app.ui.routing.name';
import { HEALTH_RECORD_PATH } from './app.healthRecord.routing.name';

@Component({
    selector: 'app-healthRecord',
    templateUrl: './app.healthRecord.component.html',
})
export class AppHealthRecordComponent {

    private items: MenuItem[];

    constructor(
        private router: Router,
        private menuService: MenuService
    ) {
        this.router.events
            .pipe(filter(event => event instanceof NavigationEnd))
            .subscribe((aux: NavigationEnd) => {
                const route = '/' + UI_ROUTES_PATH.healthRecord.value;

                if (aux.url == route)
                    this.addMenu();
            });
    }

    addMenu() {
        this.items = [
            {
                id: HEALTH_RECORD_PATH.assignedWork.value,
                label: HEALTH_RECORD_PATH.assignedWork.name,
                icon: 'add',
                routerLink: ['/' + UI_ROUTES_PATH.healthRecord.value + '/' + HEALTH_RECORD_PATH.assignedWork.value]
            },
            {
                id: HEALTH_RECORD_PATH.viewHealthRecord.value,
                label: HEALTH_RECORD_PATH.viewHealthRecord.name,
                icon: 'search',
                routerLink: ['/' + UI_ROUTES_PATH.healthRecord.value + '/' + HEALTH_RECORD_PATH.viewHealthRecord.value]
            },
            {
                id: HEALTH_RECORD_PATH.reassignTasks.value,
                label: HEALTH_RECORD_PATH.reassignTasks.name,
                icon: 'add',
                routerLink: ['/' + UI_ROUTES_PATH.healthRecord.value + '/' + HEALTH_RECORD_PATH.reassignTasks.value]
            }
        ];

        this.menuService.addChildItem(this.items, UI_ROUTES_PATH.healthRecord.value);
    }
}
