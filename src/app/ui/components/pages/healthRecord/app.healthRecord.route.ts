import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'src/app/infraestructure/services/auth/authGuard.service';
import { UI_ROUTES_PATH } from '../../app.ui.routing.name';
import { AppDashBoardComponent } from '../dashboard/app.dashBoard.component';
import { AppHealthRecordComponent } from './app.healthRecord.component';
import { HEALTH_RECORD_PATH } from './app.healthRecord.routing.name';
import { AppAssignedWorkComponent } from './assignedWork/app.assignedWork.component';
import { AppViewHealthRecordComponent } from './viewHealthRecord/app.viewHealthRecord.component';
import {ManageAssignedTasksComponent} from "../../tools/HealthRegister/manage-assigned-tasks/manage-assigned-tasks.component";
import {AppReassignTasksComponent} from "./reassignTasks/app.reassignTasks.component";

const routes: Routes = [
    { path: '', redirectTo: UI_ROUTES_PATH.dashboard, pathMatch: 'full' },
    {
        path: '',
        component: AppHealthRecordComponent,
        canActivateChild: [AuthGuardService],
        data: {
            breadcrumb: UI_ROUTES_PATH.healthRecord.name
        },
        children: [
            {
                path: UI_ROUTES_PATH.dashboard,
                component: AppDashBoardComponent,
                data: {
                    breadcrumb: UI_ROUTES_PATH.dashboard
                }
            },
            {
                path: HEALTH_RECORD_PATH.assignedWork.value,
                loadChildren: () => import('../request-hearing/request-hearing.module').then(m => m.RequestHearingModule),
                data: {
                    breadcrumb: HEALTH_RECORD_PATH.assignedWork.name
                }
            },
            {
                path: HEALTH_RECORD_PATH.viewHealthRecord.value,
                loadChildren: () => import('../unification/unification.module').then(m => m.UnificationModule),
                data: {
                    breadcrumb: HEALTH_RECORD_PATH.viewHealthRecord.name
                }
            },
            {
                path: HEALTH_RECORD_PATH.reassignTasks.value,
                loadChildren: () => import('../clarifications/clarifications.module').then(m => m.ClarificationsModule),
                data: {
                    breadcrumb: HEALTH_RECORD_PATH.reassignTasks.name
                }
            }
        ]
    }
];


@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class HealthRecordRoutingModule {
}
