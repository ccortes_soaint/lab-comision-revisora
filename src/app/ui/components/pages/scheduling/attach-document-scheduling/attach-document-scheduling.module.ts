import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AttachDocumentSchedulingComponent } from './attach-document-scheduling.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TableAttachDocumentModule } from '../../execution-room/shared/table-attach-document/table-attach-document.module';
import { PRIMENG_MODULES } from 'src/app/shared/enums/primeng.elements';
import { PdfViewerModule } from 'ng2-pdf-viewer';

@NgModule({
  declarations: [AttachDocumentSchedulingComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ...PRIMENG_MODULES,
    TableAttachDocumentModule,
    PdfViewerModule
  ],
  exports: [AttachDocumentSchedulingComponent]
})
export class AttachDocumentSchedulingModule { }
