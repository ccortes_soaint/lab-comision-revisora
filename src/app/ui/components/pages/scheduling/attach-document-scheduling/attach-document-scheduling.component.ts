import { formatDate } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { SchedulingService } from 'src/app/infraestructure/services/scheduling.service';
import { RoomScheduling } from '../shared/scheduling.model';


@Component({
  selector: 'app-attach-document-scheduling',
  templateUrl: './attach-document-scheduling.component.html',
  styleUrls: ['./attach-document-scheduling.component.css']
})
export class AttachDocumentSchedulingComponent implements OnInit {

  valuesTable;
  columnsTable;
  listActions = [];

  checkbox:boolean=false;
  viewPdfDocument:boolean=false;
  archivo: any;
  pdfSrc:any;

  listSchedulingMonth:any[];
  listSchedulingCut:any[];
  
  public fgAttachDocument:FormGroup;
  invalidImageFileSizeMessageSummary = '';

  @Input() fgSearchScheduling:FormGroup;

  constructor(
    public schedulingService:SchedulingService,
    private formBuilder:FormBuilder,
  ) { }

  ngOnInit(): void {
    this.fgAttachDocument = this.initFormGroupAttach();
    this.fgAttachDocument.valueChanges.subscribe(val => {
      console.log(val)
    });
    this.initTableVariables()

    this.fgAttachDocument.get('room').valueChanges.subscribe(res => {
      this.schedulingService.fetchListRoomScheduling(res.Id).then(values => {
        this.listSchedulingMonth = values.map((room:RoomScheduling, index) =>{
          return {id:index,Descripcion:room.monthAgenda};
        });
      })
      this.schedulingService.fetchListSchedulingCut(res.Id).then(values => {
        this.listSchedulingCut = values;
      })
    });
    
  }

  initFormGroupAttach(){
    return this.formBuilder.group({
      room:[''],
      schedulingMonth:[''],
      schedulingCut:[''],
      file:[''],
    })
  }

  initTableVariables(){
    this.columnsTable = this.initColumnsTable();
    this.valuesTable = this.initValuesTable();
    this.listActions = this.initListActions();
  } 

  initColumnsTable(){
    return [
      { field: 'name', header: 'Nombre de Documento' },
      { field: 'date', header: 'Corte' },
    ];
  }
  initValuesTable(){
    return [];
  }
  initListActions(){
    return [
      { icon: 'remove_red_eye', action: this.viewPdf.bind(this) },
      { icon: 'delete', action: this.removeItem.bind(this) },
    ];
  }

  viewPdf(fileDocument){
    console.log(fileDocument)
    this.archivo = fileDocument.file;
    this.viewPdfDocument = true;
  }

  removeItem(){
    this.valuesTable.length = 0;
  }

  toAttachDocument(upload){
    console.log(upload)
    upload.files[0].date = formatDate(new Date(), 'dd MMMM y h:mm:ss', 'en-US');
    this.valuesTable.push(upload.files[0]);

    this.fgAttachDocument.patchValue({file:this.valuesTable})

    // Get Base64 file
    if (typeof (FileReader) !== 'undefined') {
      let reader = new FileReader();
      reader.onload = (e: any) => {
        upload.files[0].file = e.target.result;
      };
      reader.readAsArrayBuffer(upload.files[0]);
    }
  }
}
