import { Component, OnInit } from '@angular/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import interactionPlugin from '@fullcalendar/interaction';
import { SchedulingService } from 'src/app/infraestructure/services/scheduling.service';
import { RoomScheduling } from 'src/app/ui/components/pages/scheduling/shared/scheduling.model';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-room-scheduling-admin',
  templateUrl: './room-scheduling-admin.component.html',
  styleUrls: ['./room-scheduling-admin.component.css']
})
export class RoomSchedulingAdminComponent implements OnInit {

  UTC_LOCALE = 'T00:00:00';
  events: any[];
  options: any;
  displayDateInfo: any;
  listControls: { url: string; nameicon: string; }[];
  listRoomSchedulingModels:RoomScheduling[];
  roomScheduling:RoomScheduling = new RoomScheduling() ;

  fgRoomsScheduling:FormGroup;

  constructor(
    public schedulingService:SchedulingService,
    private formBuilder:FormBuilder,
  ) { }

  ngOnInit(): void {
    this.fgRoomsScheduling = this.initFormGroupRoomScheduling()
    this.onChangesFormGroup(this.fgRoomsScheduling)
    this.options = this.initOptionCalendar();

    this.fgRoomsScheduling.valueChanges.subscribe(val => {
      console.log(val)
    });
  }

  onChangesFormGroup(form:FormGroup){
    form.get('idSala').valueChanges.subscribe(room => {
      this.roomScheduling.idRoom = room.Id;
      // get List of Scheduling Agenda
      this.schedulingService.fetchListRoomScheduling(room.Id).then((listRoomSchedulingModels:RoomScheduling[]) => {
        this.events = listRoomSchedulingModels.map((roomScheduling)=>{
          return roomScheduling.getFormatEvent()
        });
      })
    });
    form.get('idSesion').valueChanges.subscribe(val => {
      this.roomScheduling.idSession = val.Id;
    });
    form.get('fechaInicio').valueChanges.subscribe(val => {
      this.roomScheduling.start = val.toISOString().substring(0, 10);
    });
    form.get('fechaFin').valueChanges.subscribe(val => {
      this.roomScheduling.end = val.toISOString().substring(0, 10);
    });
    form.get('fechaCorte').valueChanges.subscribe(val => {
      this.roomScheduling.dateCut = val.toISOString().substring(0, 10);
    });
    form.get('horaInicio').valueChanges.subscribe(val => {
      this.roomScheduling.initHour = val.Descripcion;
    });
    form.get('horaFinal').valueChanges.subscribe(val => {
      this.roomScheduling.finalHour = val.Descripcion;
    });
    form.get('fechaCorteInicial').valueChanges.subscribe(val => {
      this.roomScheduling.initDateCut = val.toISOString().substring(0, 10);
      this.roomScheduling.dateCut = val.toISOString().substring(0, 10);
    });
    form.get('fechaCorteFinal').valueChanges.subscribe(val => {
      this.roomScheduling.finalDateCut = val.toISOString().substring(0, 10);
    });
    form.get('estado').valueChanges.subscribe(val => {
      this.roomScheduling.state = val;
    });

  }

  initOptionCalendar(){
    return {
      plugins: [dayGridPlugin, timeGridPlugin, interactionPlugin],
      header: {
          left: 'prev,next',
          center: 'title',
          right: 'month,agendaWeek,agendaDay',
          allDay:true,
          timeZone:'UTC'
      },
      eventClick:(e: any)=>{
        this.roomScheduling = e.event.extendedProps.model;
        console.log(this.roomScheduling)
        this.displayDateInfo = true;
      },
    }
  }

  initFormGroupRoomScheduling(){
    return this.formBuilder.group({
      idSala:[''],
      idSesion:[''],
      fechaInicio:[''],
      fechaFin:[''],
      fechaCorte:[''],
      horaInicio:[''],
      horaFinal:[''],
      fechaCorteInicial:[''],
      fechaCorteFinal:[''],
      estado:[''],
    })
  }
  createAgenda(){
    if(this.roomScheduling.getId()){
      this.roomScheduling.save();
    }else{
      this.roomScheduling.create().then(res=>{
        console.log(this.roomScheduling)
        this.fgRoomsScheduling.reset({},{emitEvent: false});
      });
    }
  }
  updateFormRoomScheduling(room){
    this.displayDateInfo = false;
    console.log(room.convertToDTO())
    console.log(room == this.roomScheduling)

    this.fgRoomsScheduling.patchValue({
      idSala:this.schedulingService.listRoom.find(items=>items.Id == room.idRoom),
      idSesion:this.schedulingService.listSession.find(items=>items.Id == room.idSession),
      fechaInicio:new Date(`${room.start}${this.UTC_LOCALE}`),
      fechaFin:new Date(`${room.end}${this.UTC_LOCALE}`),
      horaInicio:this.schedulingService.listHoursSchedulingIni.find(items=>items.Descripcion == room.initHour),
      horaFinal:this.schedulingService.listHoursSchedulingFin.find(items=>items.Descripcion == room.finalHour),
      fechaCorteInicial:new Date(`${room.initDateCut}`),
      fechaCorteFinal:new Date(`${room.finalDateCut}`),
    });
  }
  deleteRoomScheduling(room){
    this.displayDateInfo = false;
    room.delete()
  }
  formatDate(date){    
    let split = date.split('-');
    //return new Date(Date.UTC(split[2],split[1]-1,split[0])).toISOString();
    return (new Date(Date.UTC(+split[2],+split[1]-1,+split[0])).toISOString()).split("T")[0];
  }
}
