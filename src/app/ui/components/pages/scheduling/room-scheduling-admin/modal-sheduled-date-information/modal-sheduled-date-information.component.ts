import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ConfirmationService } from 'primeng';
import { RoomScheduling } from '../../shared/scheduling.model';

@Component({
  selector: 'app-modal-sheduled-date-information',
  templateUrl: './modal-sheduled-date-information.component.html',
  styleUrls: ['./modal-sheduled-date-information.component.css']
})
export class ModalSheduledDateInformationComponent implements OnInit {

  @Input() room: RoomScheduling;
  @Output() update = new EventEmitter<any>();
  @Output() delete = new EventEmitter<any>();

  constructor(private confirmationService: ConfirmationService) { }

  ngOnInit(): void {

  }
  confirm() {
    this.confirmationService.confirm({
        message: 'Esta seguro de eliminar este registro?',
        accept: () => {
            this.delete.emit(this.room)
        }
    });
}


}
