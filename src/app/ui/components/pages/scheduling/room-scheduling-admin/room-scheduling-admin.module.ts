import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoomSchedulingAdminComponent } from './room-scheduling-admin.component';
import { PRIMENG_MODULES } from 'src/app/shared/enums/primeng.elements';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FullCalendarModule } from 'primeng';
import { ModalSheduledDateInformationComponent } from './modal-sheduled-date-information/modal-sheduled-date-information.component';



@NgModule({
  declarations: [RoomSchedulingAdminComponent, ModalSheduledDateInformationComponent],
  imports: [
    CommonModule,
    ...PRIMENG_MODULES,
    FormsModule,
    ReactiveFormsModule,
    FullCalendarModule
  ],
  exports: [RoomSchedulingAdminComponent]
})
export class RoomSchedulingAdminModule { }
