import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ProcedureScheduled } from 'src/app/domain/model/dto/scheduling/procedure-scheduled.model';


@Component({
  selector: 'app-table-scheduling',
  templateUrl: './table-scheduling.component.html',
  styleUrls: ['./table-scheduling.component.css']
})
export class TableSchedulingComponent implements OnInit {

  @ViewChild('schedulingTable') table;

  @Input() columnsTable: ProcedureScheduled[];
  @Input() valuesTable: any[];

  selectedItems;

  constructor() { }

  ngOnInit(): void {
    
  }

  applyFilter(values){
    for(let field in values){
      if(typeof values[field] == "object" && values[field] != null )
        this.table.filter(values[field].name, field, 'equals');
      else
        this.table.filter('', field, 'contains');
    }
  }

}
