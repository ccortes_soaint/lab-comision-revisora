import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableSchedulingComponent } from './table-scheduling.component';
import { TableModule } from 'primeng/table';
import { PRIMENG_MODULES } from 'src/app/shared/enums/primeng.elements';

@NgModule({
  declarations: [TableSchedulingComponent],
  imports: [
    CommonModule,
    TableModule,
    ...PRIMENG_MODULES
  ],
  exports: [TableSchedulingComponent]
})
export class TableSchedulingModule { }
