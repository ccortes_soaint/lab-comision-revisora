import { Component, OnInit } from '@angular/core';
import { SchedulingService } from 'src/app/infraestructure/services/scheduling.service';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import interactionPlugin from '@fullcalendar/interaction';
import { RoomScheduling } from '../shared/scheduling.model';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-edit-agenda-scheduling',
  templateUrl: './edit-agenda-scheduling.component.html',
  styleUrls: ['./edit-agenda-scheduling.component.css']
})
export class EditAgendaSchedulingComponent implements OnInit {

  selectedCommissioners;
  listNumeralsRoom:any[];

  listRoomSchedulingModels:RoomScheduling[];
  fgEditAgenda:FormGroup;

  roomSelected:RoomScheduling = new RoomScheduling();
  listInputs;

  events: any[];
  options: any;

  constructor(
    public schedulingService:SchedulingService,
    private formBuilder:FormBuilder,
  ) { }

  ngOnInit(): void {
    this.options = this.initOptionCalendar();

    this.fgEditAgenda = this.initFormGroupEditAgenda();
    this.fgEditAgenda.valueChanges.subscribe(val => {
      console.log(val)
    });
    this.fgEditAgenda.get('idRoom').valueChanges.subscribe(value => {
      this.schedulingService.fetchListNumeralsRoom(value.Id).then(values => {
        this.listNumeralsRoom = values;
      })
      this.schedulingService.fetchListRoomScheduling(value.Id).then(values => {
        this.listRoomSchedulingModels = values;
        this.events = values.map((room:RoomScheduling) =>{
          return room.getFormatEvent();
        });
      })
    });

    

    this.listInputs = this.setListInputText(this.roomSelected);
  }

  initFormGroupEditAgenda(){
    return this.formBuilder.group({
      idRoom:[''],
      idNumeralRoom:[''],
      justification:[''],
      idLocation:[''],
      commissioners:[''],
    })
  }

  initOptionCalendar(){
    return {
      plugins: [dayGridPlugin, timeGridPlugin, interactionPlugin],
      header: {
          left: 'prev,next',
          center: 'title',
          right: 'month,agendaWeek,agendaDay',
          allDay:true,
          timeZone:'UTC'
      },
      eventClick:(e: any)=>{
        console.log(e)
        
        const parent = e.el.closest('.fc-day-grid-container');
        parent.querySelectorAll('.fc-day-grid-event').forEach(a=>a.style.backgroundColor='grey');
        e.el.style.backgroundColor = 'red';

        this.roomSelected = e.event.extendedProps.model;
        this.listInputs = this.setListInputText(this.roomSelected);
      },
    }
  }

  setListInputText(room:RoomScheduling){
    return [
      {id:"rad1",type:'date', name:"*Fecha inicial agendamiento", disabled:true, value:room.start },
      {id:"rad2",type:'date', name:"*Fecha final agendamiento", disabled:true, value:room.end},
      {id:"rad3",type:'text', name:"*Horario inicial", disabled:true, value:room.initHour},
      {id:"rad4",type:'text', name:"*Horario final", disabled:true, value:room.finalHour},
    ]
  }

}
