import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditAgendaSchedulingComponent } from './edit-agenda-scheduling.component';
import { DropdownModule } from 'primeng/dropdown';
import { CalendarModule } from 'primeng/calendar';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { ListboxModule } from 'primeng/listbox';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FullCalendarModule } from 'primeng';
import { PRIMENG_MODULES } from 'src/app/shared/enums/primeng.elements';
import { ListInputsModule } from '../../unification/shared/list-inputs/list-inputs.module';



@NgModule({
  declarations: [EditAgendaSchedulingComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ...PRIMENG_MODULES,
    ListInputsModule
  ],
  exports: [EditAgendaSchedulingComponent]
})
export class EditAgendaSchedulingModule { }
