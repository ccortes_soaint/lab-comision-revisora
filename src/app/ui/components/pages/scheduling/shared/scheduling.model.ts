import { formatDate } from "@angular/common";
import { SchedulingService } from "src/app/infraestructure/services/scheduling.service";

export interface RoomSchedulingDTO {
  idMesAgenda?:number,
  fechaCorteInicial:Date,
  fechaCorteFinal:Date,
  estado:number,
  fechaCorte?:string,
  idSesion:number,
  fechaInicio:Date,
  horaFinal:string,
  idSala:number,
  fechaFin:Date,
  horaInicio:string,
}

export class RoomScheduling {
  idMonthAgenda:number;
  initDateCut:Date;
  finalDateCut:Date;
  state:number;
  _dateCut:string;
  idSession:number;
  start:Date;
  end:Date;
  finalHour:string;
  idRoom:number;
  initHour:string;
  private _monthAgenda:string;
  private _service:SchedulingService;

  /* constructor(obj: Object = {}) {
    Object.assign(this, obj);
  }  */

  static createByDTO(dto: RoomSchedulingDTO) {
    return new RoomScheduling(dto);
  }

  constructor(obj:{idMesAgenda?,fechaCorteInicial?,fechaCorteFinal?,fechaCorte?,estado?,fechaInicio?,fechaFin?, idSesion?, idSala?, horaFinal?,horaInicio? }={}) {
    this.idMonthAgenda = obj?.idMesAgenda;
    this.initDateCut = obj?.fechaCorteInicial;
    this.finalDateCut = obj?.fechaCorteFinal;
    this.state = obj?.estado;
    this.start = obj?.fechaInicio;
    this.end = obj?.fechaFin;
    this.idSession = obj?.idSesion;
    this.idRoom = obj?.idSala;
    this.finalHour = obj?.horaFinal;
    this.initHour = obj?.horaInicio;
    this._dateCut = obj?.fechaCorte || '2021-06-28';
    this.monthAgenda = (this.start && this.end )?`${formatDate(this.start, 'dd', 'en-US')}-${formatDate(this.end, 'dd MMMM y', 'en-US')}`:null;
    
    this._service = SchedulingService.instance;
  }

  get dateCut() {
    return this._dateCut;
  }
  set dateCut(value) {
    this._dateCut = value;
  }

  get monthAgenda() {
    return this._monthAgenda;
  }
  set monthAgenda(value) {
    this._monthAgenda = value;
  }

  getId(){
    return this.idMonthAgenda
  }

  getFormatEvent(){
    return {title:"Ordinaria",start:this.start,end:this.end, id:this.idMonthAgenda , extendedProps: {model:this}, backgroundColor: 'grey'};
    //return {title:"Ordinaria",start:this.formatDate(this.start),end:this.formatDateForEnd(this.end), id:this.idMonthAgenda , extendedProps: {model:this} };
  }

  formatDate(date){    
    let split = date.split('-');
    //return new Date(Date.UTC(split[2],split[1]-1,split[0])).toISOString();
    return (new Date(Date.UTC(+split[2],+split[1]-1,+split[0])).toISOString()).split("T")[0];
  }

  formatDateForEnd(date){    
    let split = date.split('-');
    //return new Date(Date.UTC(+split[2],+split[1]-1,+split[0]+1)).toISOString();
    return (new Date(Date.UTC(+split[2],+split[1]-1,+split[0]+1)).toISOString()).split("T")[0];
  }
  create(){
    return this._service.createRoomScheduling(this.convertToDTO()).then(res=>{
      this.idMonthAgenda = res.idMesAgenda;
    })
  }
  save(){
    if(this.idMonthAgenda){
      return this._service.updateRoomScheduling(this.convertToDTO()).then(res=>{
        console.log(res)
      })
    }
  }
  delete(){
    this.state = 0;
    this.save();
  }
  getMonthAgenda(){

  }
  getDescriptionRoom(){
    if(this.idRoom){
      return this._service.listRoom.find(items=>items.Id == this.idRoom)['NombreCorto'];
    }
  }
  getDescriptionSession(){
    if(this.idSession){
      return this._service.listSession.find(items=>items.Id == this.idSession)['Descripcion'];
    }
  }
  convertToDTO():RoomSchedulingDTO{
    return {
      idMesAgenda:this.idMonthAgenda,
      fechaCorteInicial:this.initDateCut,
      fechaCorteFinal: this.finalDateCut,
      estado: this.state,
      fechaInicio:this.start,
      fechaFin:this.end,
      idSesion:this.idSession,
      idSala:this.idRoom,
      horaFinal:this.finalHour,
      horaInicio:this.initHour,
      fechaCorte:this.dateCut,
    }
  }
}
