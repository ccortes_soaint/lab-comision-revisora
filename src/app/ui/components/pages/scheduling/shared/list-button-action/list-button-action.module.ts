import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListButtonActionComponent } from './list-button-action.component';



@NgModule({
  declarations: [ListButtonActionComponent],
  imports: [
    CommonModule
  ],
  exports: [ListButtonActionComponent]
})
export class ListButtonActionModule { }
