import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SchedulingService } from 'src/app/infraestructure/services/scheduling.service';

@Component({
  selector: 'app-scheduling',
  templateUrl: './scheduling.component.html',
  styleUrls: ['./scheduling.component.css']
})
export class SchedulingComponent implements OnInit {

  listAccions: { url: string; nameicon: string; name: string;action: void; }[];
  valuesTable;
  columnsTable;
  fgSearchScheduling:FormGroup;
  fgReturnScheduling:FormGroup;
 
  
  displayEditAgenda: boolean = false;
  displayReturn: boolean = false;
  displayAttachDocument: boolean = false;
  displayExport: boolean = false;
  displayAdmin: boolean = false;

  constructor( 
    private formBuilder:FormBuilder,
    private schedulingService:SchedulingService
  ) { }

  ngOnInit(): void {

    this.fgSearchScheduling = this.initFormGroupSearch();
    this.fgReturnScheduling = this.initFormGroupReturn();
    
    this.listAccions = this.initListActionButtons();
    this.columnsTable = this.initColumnsTable();

    this.schedulingService.fetchProcedureScheduled().then(items => {
      this.valuesTable = items;
    })
  }

  initListActionButtons(){
    return [
      {url:"#",nameicon:"remove_red_eye", name:"Ver",action:this.showDialogByAction.bind(this)},
      {url:"#",nameicon:"edit", name:"Editar", action:this.showDialogByAction.bind(this)},
      {url:"#",nameicon:"notes", name:"Generar",action:this.showDialogByAction.bind(this)},
      {url:"#",nameicon:"attach_file", name:"Adjuntar",action:this.showDialogByAction.bind(this)},
      {url:"#",nameicon:"today", name:"Exportar",action:this.showDialogByAction.bind(this)},
      {url:"#",nameicon:"arrow_back", name:"Devolver",action:this.showDialogByAction.bind(this)},
    ]
  }
  
  showDialogByAction(e, action) {
    e.preventDefault()
    switch (action) {
      case 'Generar': break;
      case 'Ver': this.displayAdmin = true; break;
      case 'Exportar': this.displayExport = true; break;
      case 'Editar': this.displayEditAgenda = true; break;
      case 'Adjuntar': this.displayAttachDocument = true; break;
      case 'Devolver': this.displayReturn = true; break;
      default: break;
    }
  }

  initFormGroupSearch(){
    return this.formBuilder.group({
      stateProcedure:[''],
      room:[''],
      schedulingMonth:[''],
      schedulingCut:['']
    })
  }

  initFormGroupReturn(){
    return this.formBuilder.group({
      reasonReturn:[''],
      justifyReturn:['',[Validators.maxLength(1000)]],
    })
  }
  
  sendAttachDocument(form:FormGroup){
    console.log(form)
  }

  getfgReturnModal(form:FormGroup){
    let params = {
      idRegistroSala: 3,    //TODO: Set the parameter correctly
      idTipoDevolucion: form.get('filedNumber').value.IdConsulta,
      observacion: form.get('proceedings').value
    }
    this.schedulingService.createReasonReturned(params).then(resp => {
      this.displayReturn = false;
    })
  }

  initColumnsTable(){
    return [
      { field: 'numberRecorded', header: 'Número de Radicado' },
      { field: 'date', header: 'Fecha de Radicado - Recibido' },
      { field: 'group', header: 'Grupo' },
      { field: 'numberFile', header: 'Numero de Expediente' },
      { field: 'typeProcedure', header: 'Tipo de trámite' },
      { field: 'typeRequest', header: 'Tipo solicitud' },
      { field: 'stateProcedure', header: 'Estado del trámite' },
      { field: 'nameProduct', header: 'Nombre del Producto' },
      { field: 'activeSubstance', header: 'Principio Activo' },
      { field: 'room', header: 'Sala' },
    ];
  }
}
