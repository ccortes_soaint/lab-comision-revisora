import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchSchedulingComponent } from './search-scheduling.component';
import { PRIMENG_MODULES } from 'src/app/shared/enums/primeng.elements';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [SearchSchedulingComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ...PRIMENG_MODULES,
  ],
  exports: [SearchSchedulingComponent]
})
export class SearchSchedulingModule { }
