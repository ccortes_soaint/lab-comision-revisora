import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { SchedulingService } from 'src/app/infraestructure/services/scheduling.service';
import { RoomScheduling } from '../shared/scheduling.model';

@Component({
  selector: 'app-search-scheduling',
  templateUrl: './search-scheduling.component.html',
  styleUrls: ['./search-scheduling.component.css']
})
export class SearchSchedulingComponent implements OnInit {

  @Input() fgSearchScheduling:FormGroup;
  @Input() view:string;
  @Output() clickButton = new EventEmitter<any>();

  listSchedulingMonth:any[];
  listSchedulingCut:any[];
  listStateProcedure:any[];

  constructor(
    public schedulingService:SchedulingService
  ) { }

  ngOnInit(): void {

    this.schedulingService.fetchListStateProcedure(this.view).then(values => {
      this.listStateProcedure = values;
    })
   
    let fcStateProcedure = this.fgSearchScheduling.get('stateProcedure');
    fcStateProcedure.valueChanges.subscribe(value => {
      console.log(value)
    })

    let fcRoom = this.fgSearchScheduling.get('room');
    fcRoom.valueChanges.subscribe(value => {
      this.schedulingService.fetchListRoomScheduling(value.Id).then(values => {
        this.listSchedulingMonth = values.map((room:RoomScheduling, index) =>{
          return {id:index,Descripcion:room.monthAgenda};
        });
      })
      this.schedulingService.fetchListSchedulingCut(value.Id).then(values => {
        this.listSchedulingCut = values;
      })
    })

    
  }
}
