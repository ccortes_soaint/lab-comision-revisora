import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, Input, OnInit } from '@angular/core';
import { SchedulingService } from 'src/app/infraestructure/services/scheduling.service';

@Component({
  selector: 'app-export-modal',
  templateUrl: './export-modal.component.html',
  styleUrls: ['./export-modal.component.css']
})
export class ExportModalComponent implements OnInit {

  @Input() type;
  fgExport: FormGroup;
  schedulingMonth: { id: number; Descripcion: string; }[];
  
  PdfViewerComponent: any [];

  constructor(public schedulingService: SchedulingService, private formBuilder: FormBuilder) {
  }

  ngOnInit(): void {

    //this.schedulingService.fetchListRoom().then((value) => { this.selecRoom = value })
    

    this.fgExport = this.initFormGroupExport();
    this.fgExport.valueChanges.subscribe(val => {
      console.log(val)
    });

    let fcRoom = this.fgExport.get('selecRoom');
    fcRoom.valueChanges.subscribe(room => {
      this.schedulingService.fetchListRoomScheduling(room.Id).then(listRoomScheduling => {
        this.schedulingMonth = listRoomScheduling.map((roomScheduling, index) =>{
          return {id:index,Descripcion:roomScheduling.monthAgenda};
        });
      })
    })

  }
  initFormGroupExport() {
    return this.formBuilder.group({
      selecRoom: ['', Validators.required], 
      schedulingMonth:['', Validators.required]
    })
  }

  getfgExport(event: Event){
    event.preventDefault();
    console.log(this.fgExport.value);
  }
}


