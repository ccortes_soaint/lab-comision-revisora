import { FormGroup } from '@angular/forms';
import { FormBuilder, FormControl } from '@angular/forms';
import { Component, Input, OnInit } from '@angular/core';
import { SchedulingService } from 'src/app/infraestructure/services/scheduling.service';

@Component({
  selector: 'app-return-modal',
  templateUrl: './return-modal.component.html',
  styleUrls: ['./return-modal.component.css']
  
})


export class ReturnModalComponent implements OnInit {

  @Input() form:any;

  public fgReturnModal:FormGroup;
  returnOption:any[];
  proceedings: any;

  constructor(
    private formBuilder:FormBuilder, 
    private schedulingService:SchedulingService
  ){ }

  ngOnInit(): void {
    this.fgReturnModal = this.initFormGroupReturnModal();
    this.schedulingService.reasonReturn().then(items => {
      this.returnOption = items;
    })
  }

  initFormGroupReturnModal(){
    return this.formBuilder.group({
      filedNumber:[''],
      proceedings:['']
    })
  }
}
