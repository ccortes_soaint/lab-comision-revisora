import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SchedulingComponent } from './scheduling.component';
import { SchedulingRoutingModule } from './scheduling.routing';
import { TableSchedulingModule } from './table-scheduling/table-scheduling.module';
import { ListButtonActionModule } from './shared/list-button-action/list-button-action.module';
import { ReturnModalComponent } from './return-modal/return-modal.component';
import { PRIMENG_MODULES } from 'src/app/shared/enums/primeng.elements';
import { SearchSchedulingModule } from './search-scheduling/search-scheduling.module';
import { ExportModalComponent } from './export-modal/export-modal.component';
import { EditAgendaSchedulingModule } from './edit-agenda-scheduling/edit-agenda-scheduling.module';
import { SchedulingService } from 'src/app/infraestructure/services/scheduling.service';
import { AttachDocumentSchedulingModule } from './attach-document-scheduling/attach-document-scheduling.module';
import { RoomSchedulingAdminModule } from './room-scheduling-admin/room-scheduling-admin.module';
import { PdfViewerComponent, PdfViewerModule } from 'ng2-pdf-viewer';



@NgModule({
  declarations: [SchedulingComponent, ReturnModalComponent,ExportModalComponent],
  imports: [
    CommonModule,
    SchedulingRoutingModule,
    ...PRIMENG_MODULES,
    TableSchedulingModule,
    ListButtonActionModule,
    SearchSchedulingModule,
    EditAgendaSchedulingModule,
    FormsModule,
    ReactiveFormsModule,
    AttachDocumentSchedulingModule,
    RoomSchedulingAdminModule,
    PdfViewerModule
  ],
  providers:[SchedulingService],
  exports: [SchedulingComponent]
})
export class SchedulingModule { }

