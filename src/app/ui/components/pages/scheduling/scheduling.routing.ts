import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'src/app/infraestructure/services/auth/authGuard.service';
import { UI_ROUTES_PATH } from '../../app.ui.routing.name';
import { SchedulingComponent } from './scheduling.component';

const routes: Routes = [
    { path: '', redirectTo: UI_ROUTES_PATH.scheduling.value, pathMatch: 'full' },
    {
        path: '',
        component: SchedulingComponent,
        canActivateChild: [AuthGuardService],
        data: {
            breadcrumb: UI_ROUTES_PATH.scheduling.name
        },
        children:[]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SchedulingRoutingModule {
}