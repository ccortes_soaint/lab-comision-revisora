import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UnificationComponent } from './unification.component';
import { AuthGuardService } from 'src/app/infraestructure/services/auth/authGuard.service';
import { UI_ROUTES_PATH } from '../../app.ui.routing.name';
import { HEALTH_RECORD_PATH } from '../healthRecord/app.healthRecord.routing.name';


const routes: Routes = [
  { path: '', redirectTo: HEALTH_RECORD_PATH.viewHealthRecord.value, pathMatch: 'full' },
  {
      path: '',
      component: UnificationComponent,
      canActivateChild: [AuthGuardService],
      data: {
          breadcrumb: HEALTH_RECORD_PATH.viewHealthRecord.name
      },
      children:[]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UnificationRoutingModule { }
