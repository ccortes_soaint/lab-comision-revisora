import { Component, Input, OnInit } from '@angular/core';

interface ListInputs {
  id:       string;
  type:     string;
  name:     string;
  disabled: boolean;
  value:    string;
}

@Component({
  selector: 'app-list-inputs',
  templateUrl: './list-inputs.component.html',
  styleUrls: ['./list-inputs.component.css']
})
export class ListInputsComponent implements OnInit {

  @Input() listInputs:ListInputs;

  constructor() { }

  ngOnInit(): void {
    console.log(this.listInputs)
  }

}
