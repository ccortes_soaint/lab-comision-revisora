import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListInputsComponent } from './list-inputs.component';
import { PRIMENG_MODULES } from 'src/app/shared/enums/primeng.elements';



@NgModule({
  declarations: [ListInputsComponent],
  imports: [
    CommonModule,
    ...PRIMENG_MODULES
  ],
  exports: [ListInputsComponent]
})
export class ListInputsModule { }
