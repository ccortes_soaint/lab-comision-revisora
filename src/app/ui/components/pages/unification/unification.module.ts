import { AppSelectRoomModule } from './../inquiries/select-room/app.select-room.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UnificationRoutingModule } from './unification.routing';
import { UnificationComponent } from './unification.component';
import { PRIMENG_MODULES } from 'src/app/shared/enums/primeng.elements';
import { FormsModule } from '@angular/forms';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { AppFilingDataModule } from '../inquiries/filing-data/app.filing-data.module';
import { AppInquiriesModule } from '../inquiries/app.inquiries.module';
import { ListInputsModule } from './shared/list-inputs/list-inputs.module';


@NgModule({
  declarations: [UnificationComponent],
  imports: [
    CommonModule,
    UnificationRoutingModule,
    ...PRIMENG_MODULES,
    FormsModule,
    PdfViewerModule,
    AppFilingDataModule,
    AppInquiriesModule,
    ListInputsModule,
    AppSelectRoomModule
  ],
  exports: [UnificationComponent]
})
export class UnificationModule { }
