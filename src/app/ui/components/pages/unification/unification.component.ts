import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

enum QueryParams {
  OPTION1='anx15',
  OPTION2='anx14',
  OPTION3='anx16'
}

@Component({
  selector: 'app-unification',
  templateUrl: './unification.component.html',
  styleUrls: ['./unification.component.css']
})
export class UnificationComponent implements OnInit {

  type:string;
  query = QueryParams;

  listInputs;

  constructor(
    private route: ActivatedRoute,
  ) { 

    this.route.queryParams.subscribe(params => {
      this.type = params['type'] || this.query.OPTION2;
      this.initVariablesByOption(this.type)
    });
  }

  ngOnInit(): void {
    
  }
  initVariablesByOption(opt:string){
    this.listInputs = this.initListInputsGeneralInfo(opt);
  }

  initListInputsGeneralInfo(opt:string){
    if(opt == this.query.OPTION1){
      return [
        {id:"rad1",type:'text', name:"Radicado", disabled:true, value:"202012345"},
        {id:"rad2",type:'datetime-local', name:"Fecha solicitud", disabled:true, value:'2021-03-19T13:33'},
        {id:"rad3",type:'text', name:"Remitente", disabled:true, value:"Andrea Quintana - ciudadano"},
      ]
    }
    if(opt == this.query.OPTION2){
      return [
        {id:"rad1",type:'text', name:"Radicado relacionado", disabled:true, value:"xxxxxxxxx"},
        {id:"rad2",type:'date', name:"Fecha radicado relacionado", disabled:true, value:"xxxxxxxxx"},
        {id:"rad3",type:'text', name:"Nombre del producto", disabled:true, value:"xxxxxxxxx"},
        {id:"rad4",type:'text', name:"Principio activo", disabled:true, value:"xxxxxxxxx"},
        {id:"rad5",type:'text', name:"Composicion", disabled:true, value:""},
        {id:"rad6",type:'text', name:"Expediente", disabled:true, value:"xxxxxxxxx"},
      ]
    }
  }

}
