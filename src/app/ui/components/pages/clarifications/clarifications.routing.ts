import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClarificationsComponent } from './clarifications.component';
import { AuthGuardService } from 'src/app/infraestructure/services/auth/authGuard.service';
import { HEALTH_RECORD_PATH } from '../healthRecord/app.healthRecord.routing.name';

const routes: Routes = [
  { path: '', redirectTo: HEALTH_RECORD_PATH.reassignTasks.value, pathMatch: 'full' },
  {
      path: '',
      component: ClarificationsComponent,
      canActivateChild: [AuthGuardService],
      data: {
          breadcrumb: HEALTH_RECORD_PATH.reassignTasks.value
      },
      children:[]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClarificationsRoutingModule { }
