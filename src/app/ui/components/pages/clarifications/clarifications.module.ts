import { AppSelectRoomModule } from './../inquiries/select-room/app.select-room.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClarificationsRoutingModule } from './clarifications.routing';
import { ClarificationsComponent } from './clarifications.component';
import { PRIMENG_MODULES } from 'src/app/shared/enums/primeng.elements';
import { ListInputsModule } from '../unification/shared/list-inputs/list-inputs.module';
import { AppInquiriesModule } from '../inquiries/app.inquiries.module';


@NgModule({
  declarations: [ClarificationsComponent],
  imports: [
    CommonModule,
    ClarificationsRoutingModule,
    ...PRIMENG_MODULES,
    ListInputsModule,
    AppInquiriesModule,
    AppSelectRoomModule
  ],
  exports: [ClarificationsComponent]
})
export class ClarificationsModule { }
