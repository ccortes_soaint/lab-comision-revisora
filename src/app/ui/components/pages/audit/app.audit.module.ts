import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PRIMENG_MODULES } from 'src/app/shared/enums/primeng.elements';
import { AppUIModule } from '../../app.ui.module';
import { LayoutModule } from '../../layouts/layout.module';
import { AppAuditComponent } from './app.audit.component';
import { AuditRoutingModule } from './app.audit.route';

@NgModule({
    declarations: [
        AppAuditComponent
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        LayoutModule,
        ...PRIMENG_MODULES,
        AuditRoutingModule,
        AppUIModule
    ]
})
export class AuditModule { }