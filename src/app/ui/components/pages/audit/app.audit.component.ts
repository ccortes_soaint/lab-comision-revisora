import { Component } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { MenuItem } from 'primeng';
import { filter } from 'rxjs/operators';
import { MenuService } from 'src/app/infraestructure/services/menu/app.menu.service';
import { UI_ROUTES_PATH } from '../../app.ui.routing.name';
import { AUDIT_PATH } from './app.audit.routing.name';

@Component({
    selector: 'app-audit',
    templateUrl: './app.audit.component.html',
})
export class AppAuditComponent {

    private items: MenuItem[];

    constructor(
        private router: Router,
        private menuService: MenuService
    ) {
        this.router.events
            .pipe(filter(event => event instanceof NavigationEnd))
            .subscribe((aux: NavigationEnd) => {
                const route = '/' + UI_ROUTES_PATH.audit.value;

                if (aux.url == route)
                    this.addMenu();
            });
    }

    addMenu() {
        this.items = [
            {
                id: AUDIT_PATH.reviewRequest.value,
                label: AUDIT_PATH.reviewRequest.name,
                icon: 'add',
                routerLink: ['/' + UI_ROUTES_PATH.audit.value + '/' + AUDIT_PATH.reviewRequest.value]
            },
            {
                id: AUDIT_PATH.scheduleAudit.value,
                label: AUDIT_PATH.scheduleAudit.name,
                icon: 'search',
                routerLink: ['/' + UI_ROUTES_PATH.audit.value + '/' + AUDIT_PATH.scheduleAudit.value]
            },
            {
                id: AUDIT_PATH.assignOfficer.value,
                label: AUDIT_PATH.assignOfficer.name,
                icon: 'add',
                routerLink: ['/' + UI_ROUTES_PATH.audit.value + '/' + AUDIT_PATH.assignOfficer.value]
            },
            {
                id: AUDIT_PATH.manageAudit.value,
                label: AUDIT_PATH.manageAudit.name,
                icon: 'add',
                routerLink: ['/' + UI_ROUTES_PATH.audit.value + '/' + AUDIT_PATH.manageAudit.value]
            },
            {
                id: AUDIT_PATH.consultActs.value,
                label: AUDIT_PATH.consultActs.name,
                icon: 'add',
                routerLink: ['/' + UI_ROUTES_PATH.audit.value + '/' + AUDIT_PATH.consultActs.value]
            },
            {
                id: AUDIT_PATH.followUpVisits.value,
                label: AUDIT_PATH.followUpVisits.name,
                icon: 'add',
                routerLink: ['/' + UI_ROUTES_PATH.audit.value + '/' + AUDIT_PATH.followUpVisits.value]
            }
        ];

        this.menuService.addChildItem(this.items, UI_ROUTES_PATH.audit.value);
    }
}