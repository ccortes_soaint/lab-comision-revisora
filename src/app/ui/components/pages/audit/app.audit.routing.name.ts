export const AUDIT_PATH = {
    reviewRequest: { value: 'RevisarSolicitudes', name: 'Revisar solicitudes' },
    scheduleAudit: { value: 'ProgramarAuditoria', name: 'Programar auditoria' },
    assignOfficer: { value: 'AsignarFuncionario', name: 'Asignar funcionario' },
    manageAudit: { value: 'GestionarAuditoria', name: 'Gestionar auditoria' },
    consultActs: { value: 'ConsultarActas', name: 'Consultar actas' },
    followUpVisits: { value: 'VisitasSeguimiento', name: 'Visitas seguimiento' }
}