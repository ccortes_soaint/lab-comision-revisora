import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'src/app/infraestructure/services/auth/authGuard.service';
import { UI_ROUTES_PATH } from '../../app.ui.routing.name';
import { AppDashBoardComponent } from '../dashboard/app.dashBoard.component';
import { AppAuditComponent } from './app.audit.component';
import { AUDIT_PATH } from './app.audit.routing.name';

const routes: Routes = [
    { path: '', redirectTo: UI_ROUTES_PATH.dashboard, pathMatch: 'full' },
    {
        path: '',
        component: AppAuditComponent,
        canActivateChild: [AuthGuardService],
        data: {
            breadcrumb: UI_ROUTES_PATH.audit.name
        },
        children: [
            {
                path: UI_ROUTES_PATH.dashboard,
                component: AppDashBoardComponent,
                data: {
                    breadcrumb: UI_ROUTES_PATH.dashboard
                }
            },
            {
                path: AUDIT_PATH.reviewRequest.value,
                data: {
                    breadcrumb: AUDIT_PATH.reviewRequest.name
                }
            },
            {
                path: AUDIT_PATH.scheduleAudit.value,
                data: {
                    breadcrumb: AUDIT_PATH.scheduleAudit.name
                }
            },
            {
                path: AUDIT_PATH.assignOfficer.value,
                data: {
                    breadcrumb: AUDIT_PATH.assignOfficer.name
                }
            },
            {
                path: AUDIT_PATH.manageAudit.value,
                data: {
                    breadcrumb: AUDIT_PATH.manageAudit.name
                }
            },
            {
                path: AUDIT_PATH.consultActs.value,
                data: {
                    breadcrumb: AUDIT_PATH.consultActs.name
                }
            },
            {
                path: AUDIT_PATH.followUpVisits.value,
                data: {
                    breadcrumb: AUDIT_PATH.followUpVisits.name
                }
            }
        ]
    }
];


@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AuditRoutingModule {
}