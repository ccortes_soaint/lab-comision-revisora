import { Component } from '@angular/core';
import { MenuItem } from 'primeng';
import { LoginService } from './infraestructure/services/auth/login.service';
import { MenuService } from './infraestructure/services/menu/app.menu.service';
import { PoliciesService } from './infraestructure/services/profile/policies.service';
import { DEFAULT_POLICIES } from './ui/components/app.ui.environment';
import { UI_ROUTES_PATH } from './ui/components/app.ui.routing.name';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html'
})
export class AppComponent {

    constructor(
        private loginService: LoginService,
        private policieService: PoliciesService,
        private menuService: MenuService,
    ) {
        if (!this.loginService.currentUserValue)
            this.policieService.addPagesPolicies([DEFAULT_POLICIES]);

        if (!this.menuService.menuValue)
            this.menuService.addItemsMenu(this.addMenuItem());
    }

    addMenuItem(): MenuItem[] {
        return [
            {
                id: UI_ROUTES_PATH.home,
                label: UI_ROUTES_PATH.home,
                icon: 'home',
                routerLink: ['/'],
            },
            {
                id: UI_ROUTES_PATH.procedure.value,
                label: UI_ROUTES_PATH.procedure.name,
                icon: 'folder_open',
                routerLink: ['/' + UI_ROUTES_PATH.procedure.value]
            },
            {
                id: UI_ROUTES_PATH.healthRecord.value,
                label: UI_ROUTES_PATH.healthRecord.name,
                icon: 'edit',
                routerLink: ['/' + UI_ROUTES_PATH.healthRecord.value]
            },
            {
                id: UI_ROUTES_PATH.audit.value,
                label: UI_ROUTES_PATH.audit.name,
                icon: 'collections',
                routerLink: ['/' + UI_ROUTES_PATH.audit.value]
            },
            {
                id: UI_ROUTES_PATH.scheduling.value,
                label: UI_ROUTES_PATH.scheduling.name,
                icon: 'schedule',
                routerLink: ['/' + UI_ROUTES_PATH.scheduling.value]
            },
            {
                id: UI_ROUTES_PATH.executionRoom.value,
                label: UI_ROUTES_PATH.executionRoom.name,
                icon: 'chevron_right',
                routerLink: ['/' + UI_ROUTES_PATH.executionRoom.value]
            },
            {
                id: UI_ROUTES_PATH.inquiries.value,
                label: UI_ROUTES_PATH.inquiries.name,
                icon: 'question_answer',
                routerLink: ['/' + UI_ROUTES_PATH.inquiries.value]
            }
        ];
    }
}
