import { Injectable } from '@angular/core';
import { MenuItem } from 'primeng';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { LoginService } from '../auth/login.service';
import { PoliciesService } from '../profile/policies.service';

@Injectable()
export class MenuService {

    private menuSource = new Subject<string>();
    private menu: BehaviorSubject<MenuItem[]>;
    private resetSource = new Subject();
    public menuSource$ = this.menuSource.asObservable();
    public menu$: Observable<MenuItem[]>;
    public resetSource$ = this.resetSource.asObservable();

    constructor(
        private authService: LoginService,
        private policieService: PoliciesService
    ) {
        this.menu = new BehaviorSubject<MenuItem[]>(JSON.parse(localStorage.getItem('currentMenu')));
        this.menu$ = this.menu.asObservable();
    }

    onMenuStateChange(key: string) {
        this.menuSource.next(key);
    }

    reset() {
        this.resetSource.next();
    }

    public get menuValue(): MenuItem[] {
        return this.menu.value;
    }

    addItemsMenu(items: MenuItem[]) {
        localStorage.setItem('currentMenu', JSON.stringify(items));
        this.menu.next(items);
    }

    addChildItem(items: MenuItem[], patherId: string) {
        items = this.callMenuItem(items);

        var oldMenu: MenuItem[] = JSON.parse(localStorage.getItem('currentMenu'));

        if (oldMenu && oldMenu.length && items && items.length)
            oldMenu = this.addChildToChild(oldMenu, items, patherId);

        localStorage.setItem('currentMenu', JSON.stringify(oldMenu));
        this.menu.next(oldMenu);
    }

    addChildItemNotUser(items: MenuItem[], patherId: string) {
        var oldMenu: MenuItem[] = JSON.parse(localStorage.getItem('currentMenu'));

        if (oldMenu && oldMenu.length && items && items.length)
            oldMenu = this.addChildToChild(oldMenu, items, patherId);

        localStorage.setItem('currentMenu', JSON.stringify(oldMenu));
        this.menu.next(oldMenu);
    }

    callMenuItem(items: MenuItem[]): Array<MenuItem> {
        var roles = ['any'];
        var currentOwner = this.authService.currentUserValue;
        var pass = new Array<string>();
        var newItem: MenuItem[];
        var pagesPolicies = this.policieService.pagesPoliciesValue();

        if (!!currentOwner && !!currentOwner.roles)
            currentOwner.roles.forEach(element => {
                roles.push(element.rol);
            });

        if (pagesPolicies && pagesPolicies.length)
            for (let idx = 0; idx < pagesPolicies.length; idx++) {
                for (let index = 0; index < roles.length; index++) {
                    const role = roles[index];
                    if (role.toUpperCase() == pagesPolicies[idx].role.toUpperCase()) {
                        if (pagesPolicies[idx].subMenuHeaderPass)
                            pagesPolicies[idx].subMenuHeaderPass.forEach(item => {
                                if (item.policiesStatus)
                                    pass.push(item.nameCode)
                            });
                        break;
                    }
                }
            }

        if (items && items.length > 0 && pass.length > 0) {
            pass = pass.filter((value, index, matriz) => matriz.indexOf(value) === index);
            newItem = new Array<MenuItem>();
            for (let idx = 0; idx < items.length; idx++) {
                loop2: for (let idy = 0; idy < pass.length; idy++) {
                    if (items[idx].id && pass[idy]) {
                        if (pass[idy].toUpperCase() == items[idx].id.toUpperCase()) {
                            newItem.push(items[idx]);
                            break loop2;
                        }
                    }
                }
            }
        }

        return newItem;
    }

    private addChildToChild = function (pather: MenuItem[], items: MenuItem[], patherId: string): MenuItem[] {
        var band = true;
        for (let index = 0; index < pather.length; index++) {
            if (pather[index].id.indexOf(patherId) == 0) {
                pather[index].items = items;
                band = false;
                return pather;
            }
        }
        if (!!band) {
            for (let index = 0; index < pather.length; index++) {
                var aux = new Array<MenuItem>();
                if (pather[index] && pather[index].items) {
                    pather[index].items.forEach(element => {
                        aux.push(element);
                    });
                    if (!!this.addChildToChild(aux, items, patherId)) {
                        pather[index].items = this.addChildToChild(aux, items, patherId);
                        return pather;
                    }
                }
            }
        }
    }

    resetMenu() {
        localStorage.removeItem('currentMenu');
        this.menu.next(null);
    }
}
