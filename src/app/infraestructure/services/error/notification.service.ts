import { Injectable, NgZone } from '@angular/core';
import { MessageService } from 'primeng';

@Injectable({
    providedIn: 'root'
})
export class NotificationService {

    constructor(
        private zone: NgZone,
        public messageService: MessageService
    ) { }

    showSuccess(message: string): void {
        this.zone.run(() => {
            this.messageService.add({
                severity: 'success',
                summary: 'Mensaje',
                detail: message
            });
        });
    }

    showError(message: string): void {
        this.zone.run(() => {
            this.messageService.add({
                severity: 'error',
                summary: 'Mensaje error:',
                detail: message
            });
        });
    }
}