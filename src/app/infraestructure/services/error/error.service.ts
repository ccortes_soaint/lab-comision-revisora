import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class ErrorService {

    getClientErrorMessage(error: Error): string {
        if (!!error)
            return error.message ? error.message : error.toString();
        return 'Dont know error';
    }

    getServerErrorMessage(error: HttpErrorResponse): string {
        return navigator && navigator.onLine ? error.message : 'Refuse Connection';
    }
}