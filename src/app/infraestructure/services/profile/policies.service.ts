import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { PagesPoliciesDTO } from 'src/app/domain/model/dto/profile/pagesPoliciesDTO';

@Injectable()
export class PoliciesService {


    private pagesPolicies: BehaviorSubject<PagesPoliciesDTO[]>;
    public pagesPolicies$: Observable<PagesPoliciesDTO[]>;

    constructor() {
        this.pagesPolicies = new BehaviorSubject<PagesPoliciesDTO[]>(JSON.parse(localStorage.getItem('pagesPolicies')));
        this.pagesPolicies$ = this.pagesPolicies.asObservable();
    }

    public pagesPoliciesValue(): PagesPoliciesDTO[] {
        return this.pagesPolicies.value;
    }

    public addPagesPolicies(pagesPolicies: PagesPoliciesDTO[]) {
        localStorage.removeItem('pagesPolicies');
        localStorage.setItem('pagesPolicies', JSON.stringify(pagesPolicies));
        this.pagesPolicies.next(pagesPolicies);
    }

    public resetPagesPolicies() {
        localStorage.removeItem('pagesPolicies');
        this.pagesPolicies.next(null);
    }

}
