import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Inquiries } from 'src/app/domain/model/dto/inquiries/inquiries.model';

@Injectable({
  providedIn: 'root'
})
export class SettledService {

  settledUrl = 'http://srvobtenerdatossolicitud-pruebades-pliegue-oc.apps.openshiftqa.soain.lcl';
  catalogsUrl = 'http://srvcatalogos-pruebades-pliegue-oc.apps.openshiftqa.soain.lcl';

  constructor(private http: HttpClient) {  }

  getTypeRequest() {
    return this.http.get(this.catalogsUrl + '/v1/catalogos/obtenerTipoSolicitud');
  }

  getInformationGeneral() {
    const httpOptions = {
      params: new HttpParams().append('TipoUsuario', 'M')
    };

    return this.http.get(this.catalogsUrl + '/v1/catalogos/obtenerConsulta', httpOptions);
  }

  getRooms() {
    return this.http.get(this.catalogsUrl + '/v1/catalogos/obtenerSala');
  }

  insertSettled(inquiries: Inquiries) {
    return this.http.post<Inquiries[]>(this.settledUrl + '/v1/datossolicitud/insertarRadicado', inquiries);
  }

  getSettledId(settled: string) {
    const httpOptions = {
      params: new HttpParams().append('radicado', settled)
    };

    return this.http.get(this.settledUrl + '/v1/datossolicitud/consultarRadicado', httpOptions);
  }
}
