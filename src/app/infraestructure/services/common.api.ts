import {Injectable} from "@angular/core";
import {SelectItem} from "primeng/api";

@Injectable()
export class CommonApi {
    constructor() {
    }


    getRowsOptions(): SelectItem [] {
        const rows = [5,10, 25, 50, 100];
        const options: SelectItem[] = [];
        rows.forEach(row => options.push({
            label: row.toString(),
            value: row
        }))
        return options;
    }
}

