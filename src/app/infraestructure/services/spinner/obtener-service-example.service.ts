import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class ObtenerServiceExampleService {

    baseUrl = environment.baseUrl;

  constructor(private Http: HttpClient) { }

  public getPaises(){
      return this.Http.get(`${this.baseUrl}/all`);
  }
}
