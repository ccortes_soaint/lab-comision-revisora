import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { UserDTO } from 'src/app/domain/model/dto/auth/userDTO';

@Injectable()
export class LoginService {

    private currentUserSubject: BehaviorSubject<UserDTO>;
    public currentUser: Observable<UserDTO>;

    constructor() {
        this.currentUserSubject = new BehaviorSubject<UserDTO>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): UserDTO {
        return this.currentUserSubject.value;
    }

    login(user: UserDTO) {
        localStorage.setItem('currentUser', JSON.stringify(user));
        this.currentUserSubject.next(user);
    }

    logout() {
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
    }

}
