import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { APP_INITIALIZER, ErrorHandler, NgModule } from '@angular/core';
import { AuthConfig, OAuthModule } from 'angular-oauth2-oidc';
import { ConfirmationService, MessageService } from 'primeng';
import { authConfig, OAuthModuleConfig } from '../../shared/providers/auth.config.const';
import { GlobalErrorHandler } from '../../shared/providers/global.error.handler';
import { HttpConfigInterceptor } from '../security/interceptors/http.config.interceptor';
import { ApiManagerService } from './api/api.manager.service';
import { AuthConfigService } from './auth/authConfig.service';
import { AuthGuardService } from './auth/authGuard.service';
import { LoginService } from './auth/login.service';
import { ErrorService } from './error/error.service';
import { LoggingService } from './error/logging.service';
import { NotificationService } from './error/notification.service';
import { MenuService } from './menu/app.menu.service';
import { PoliciesService } from './profile/policies.service';
import {InterceptedService} from "./spinner/intercepted.service";
import {SpinnerService} from "./spinner/spinner.service";
import {CommonApi} from "./common.api";


export function init_app(authConfigService: AuthConfigService) {
    return () => authConfigService.initAuth();
}

@NgModule({
    imports: [
        CommonModule,
        HttpClientModule,
        OAuthModule.forRoot()
    ],
    providers: [
        { provide: ErrorHandler, useClass: GlobalErrorHandler },
        { provide: HTTP_INTERCEPTORS, useClass: HttpConfigInterceptor, multi: true },
        // { provide: AuthConfig, useValue: authConfig },
        // OAuthModuleConfig,
        // { provide: APP_INITIALIZER, useFactory: init_app, deps: [AuthConfigService], multi: true },
        ConfirmationService,
        MessageService,
        ApiManagerService,
        AuthConfigService,
        AuthGuardService,
        LoginService,
        ErrorService,
        LoggingService,
        NotificationService,
        MenuService,
        PoliciesService,
        InterceptedService,
        SpinnerService,
        CommonApi


    ],
    declarations: []
})
export class ServiceModule { }
