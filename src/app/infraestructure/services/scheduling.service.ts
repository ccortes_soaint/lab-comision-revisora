import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { ProcedureScheduled } from 'src/app/domain/model/dto/scheduling/procedure-scheduled.model';
import { RoomScheduling, RoomSchedulingDTO } from 'src/app/ui/components/pages/scheduling/shared/scheduling.model';


@Injectable({
  providedIn: 'root'
})
export class SchedulingService {

  listRoom;
  listSchedulingMonth;
  listSchedulingCut;
  listHoursSchedulingIni;
  listHoursSchedulingFin;
  listNumeralsRoom;
  listCommissioners;
  listLocation;
  listSession;
  static instance: SchedulingService;

  urlServerAdminAgenda:string = 'http://srvtramitesalas-des-comision-revisora.apps.openshiftdev.soain.lcl/v1/administraragendasala';
  urlServerCatalogos:string = 'http://srvcatalogos-des-comision-revisora.apps.openshiftdev.soain.lcl/v1/catalogos';
  urlServerAgenda:string = 'http://service-agendamiento-des-comision-revisora.apps.openshiftdev.soain.lcl/v1/agendamiento';
  urlServerRoomManagement:string = 'http://srvgestionsala-des-comision-revisora.apps.openshiftdev.soain.lcl/v1/gestionsala';

  constructor(
    private http:HttpClient,
  ){
    SchedulingService.instance = this;
    this.setDropdownOptions()
  }

  setDropdownOptions(){
    this.fetchListRoom().then(items => {
      this.listRoom = items;
    });
    this.fetchListSchedulingMonth().then(items => {
      this.listSchedulingMonth = items;
    });
    this.fetchListSchedulingCut(2).then(items => {
      this.listSchedulingCut = items;
    });
    this.fetchListNumeralsRoom(1).then(items => {
      this.listNumeralsRoom = items;
    });
    this.fetchListCommissioners({room:"SEM"}).then(items => {
      this.listCommissioners = items;
    });
    this.fetchListLocation().then(items => {
      this.listLocation = items;
    });
    this.fetchListSession().then(items => {
      this.listSession = items;
    });
    this.fetchListHoursScheduling().then(items => {
      this.listHoursSchedulingIni = items.filter(item => item.Rango == 'INI');
      this.listHoursSchedulingFin = items.filter(item => item.Rango == 'FIN');
    });
  }

  fetchProcedureScheduled(){
    return this.http.get<any>('assets/data/procedure-scheduled.json')
        .toPromise()
        .then(res => <ProcedureScheduled[]>res.data)
        .then(data => { return data; });
  }

  fetchListStateProcedure(cod:string){
    //return this.http.get<any>('assets/data/state-procedure-options.json', { params:{ codigo:cod }})
    return this.http.get<any>(`${this.urlServerCatalogos}/obtenerEstado`, { params:{ codigo:cod }})
        .toPromise()
        .then(res => <any[]>res.objectResponse)
        .then(data => { return data; });
  }

  fetchListRoom(){
    //return this.http.get<any>('assets/data/room-options.json')
    return this.http.get<any>(`${this.urlServerCatalogos}/obtenerSala`)
        .toPromise()
        .then(res => <any[]>res.objectResponse)
        .then(data => { return data; });
  }

  fetchListSchedulingMonth(){
    return this.http.get<any>('assets/data/scheduling-month-options.json')
        .toPromise()
        .then(res => <any[]>res.data)
        .then(data => { return data; });
  }

  fetchListSchedulingCut(id){
    //return this.http.get<any>('assets/data/scheduling-month-options.json', {params:{idSala:id}})
    return this.http.get<any>(`${this.urlServerAgenda}/consultarAgendas`, {params:{idSala:id}})
        .toPromise()
        .then(res => <any[]>res.objectResponse)
        .then(data => { return data; });
  }

  fetchListNumeralsRoom(id){
    //return this.http.get<any>('assets/data/numerals-room-options.json',{params:{idSala:id}})
    return this.http.get<any>(`${this.urlServerCatalogos}/obtenerNumeracion`, {params:{idSala:id}})
        .toPromise()
        .then(res => <any[]>res.objectResponse)
        .then(data => { return data; });
  }
  fetchListCommissioners(data){
    return this.http.get<any>('assets/data/commissioners.json',{params:data})
        .toPromise()
        .then(res => <any[]>res.data)
        .then(data => { return data; });
  }
  fetchListLocation(){
    //return this.http.get<any>('assets/data/location-options.json')
    return this.http.get<any>(`${this.urlServerCatalogos}/obtenerLugar?id=1`)
        .toPromise()
        .then(res => <any[]>res.objectResponse)
        .then(data => { return data; });
  }
  fetchListSession(){
    //return this.http.get<any>('assets/data/type-session.json')
    return this.http.get<any>(`${this.urlServerCatalogos}/obtenerTipoSesion`)
        .toPromise()
        .then(res => <any[]>res.objectResponse)
        .then(data => { return data; });
  }
  fetchListHoursScheduling(){
    //return this.http.get<any>('assets/data/list-hours.json')
    return this.http.get<any>(`${this.urlServerCatalogos}/obtenerHorario?id=1`)
        .toPromise()
        .then(res => <any[]>res.objectResponse)
        .then(data => { return data; });
  }
  reasonReturn(){
    //return this.http.get<any>('assets/data/reasonReturn.json')
    return this.http.get<any>(`${this.urlServerCatalogos}/obtenerAgenda?id=1`)
        .toPromise()
        .then(res => <any[]>res.objectResponse)
        .then(data => { return data; });
  }
  //************************************
  // Services of Admin agendas for room
  fetchListRoomScheduling(id){
    //return this.http.get<any>('assets/data/room-scheduling.json', {params:{idSala:id}} )
    return this.http.get<any>(`${this.urlServerAdminAgenda}/consultarAgendaPorSala?idSala=2`)
        .toPromise()
        .then(res => <RoomScheduling[]>res.objectResponse.map(sala=>{
            return RoomScheduling.createByDTO(sala)
          }))
        .then(data => { return data; });
  }
  createRoomScheduling(args:RoomSchedulingDTO){
    return this.http.post<any>(`${this.urlServerAdminAgenda}/insertarAgendaSala`, args)
        .toPromise()
        .then(res => <RoomSchedulingDTO>res.objectResponse)
        .then(data => { return data; });
  }
  updateRoomScheduling(args:RoomSchedulingDTO){
    return this.http.post<any>(`${this.urlServerAdminAgenda}/actualizarAgendaSala`, args)
        .toPromise()
        .then(res => <RoomSchedulingDTO>res.objectResponse)
        .then(data => { return data; });
  }

  //************************************
  // Services of Room Managment
  createUpdateState(args){
    return this.http.post<any>(`${this.urlServerRoomManagement}/insertarRespuesta`, args)
        .toPromise()
        .then(res => <any>res)
        .then(data => { return data; });
  }

  createReasonReturned(args){
    return this.http.post<any>(`${this.urlServerRoomManagement}/insertarDevolucion`, args)
        .toPromise()
        .then(res => <any>res)
        .then(data => { return data; });
  }
  
}
