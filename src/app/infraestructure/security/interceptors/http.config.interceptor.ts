import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, delay, retry } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class HttpConfigInterceptor implements HttpInterceptor {

    constructor() { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(
            retry(3), // retry 3 tiempos
            delay(1000),// 1 second,
            catchError((error: HttpErrorResponse) => {
                switch (error.status) {
                    default:
                        return throwError(error);
                }
            })
        );
    }
}