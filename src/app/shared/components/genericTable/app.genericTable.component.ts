import {Component, Input, OnInit} from '@angular/core';
import { ColumnsDTO } from'../../../domain/model/dto/comunes/columnsDTO'

@Component({
    selector: 'app-generic-table',
    templateUrl: './app.genericTable.component.html',
    styles: []
})
export class AppGenericTableComponent implements OnInit {
    @Input() columnsTable: ColumnsDTO[];
    @Input() valuesTable: any[];

    constructor() {

    }

    ngOnInit(): void {

    }
}
