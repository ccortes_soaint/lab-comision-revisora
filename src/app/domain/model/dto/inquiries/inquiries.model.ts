export class General {
    status: number; // "estado": 12,
    typeQuery: number; // "tipoConsulta": 10,
    sender: string; // "remitente": "nifer aguilar",
    idQuery: number; // "idConsulta": 0,
    numberSettled: string; // "numeroRadicado": "N00018235",
    fechaRadicado: string; // "fechaRadicado": "20-05-2021 09:06:29"
}

export class SettledRelated {
    numberSettled: string; // "numeroRadicado": "202005189",
    dateSettled: string; // "fechaRadicado": null,
    beginningActive: string;// "principioActivo": "NA",
    precautions: string; // "precauciones": "NADA",
    againstIndications: string; // "contraIndicaciones": "NA",
    indications: string; // "indicaciones": "NA",
    typeQuery: number; // "tipoConsulta": 8,
    proceedings: number; // "expediente": "0986666",
    product: string; // "producto": "PILDORAS",
    composition: string; // "composicion": "NA",
    formPharmaceutical: string; // "formaFarmaceutica": "NA",
    headline: string; // "titular": "FARMACOLOGOS COLOMBIA"
}

export class Trace {
    source: string; // "origen": null,
    official: string; // "funcionario": "SOAINT COLOMBIA",
    request: string; // "requerimiento": "Solicitud de informacion para el trámite 202100001"
}

export class Document {
    link: string; // "enlace": "http://www.mitienda.es/producto2"
}

export interface Inquiries {
    _general: General[];
    _settledRelated: SettledRelated[];
    _trace: Trace[];
    _document: Document[];
}