export interface ProcedureScheduled {
  id:number,
  numberRecorded:string, 
  date:string, 
  group:string, 
  numberFile:string, 
  typeProcedure:string, 
  typeRequest:string, 
  stateProcedure:string, 
  nameProduct:string, 
  activeSubstance:string
}