export interface VersionDocumentoDTO {
    id: string;
    tipo: string;
    nombre: string;
    size: number;
    version?: string;
    contenido?: string;
    file?: Blob;
    taskId?:any;
    disabled?:boolean;
    editable?:boolean;
}


