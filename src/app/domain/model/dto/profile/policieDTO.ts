export class PolicieDTO {
    parameterCode?: string;
    nameCode?: string;
    route?: string;
    placement?: number;
    number?: number;
    profileCode?: string;
    policiesStatus?: boolean;
}

