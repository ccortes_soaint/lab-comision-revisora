import { PolicieDTO } from './policieDTO';

export class PagesPoliciesDTO {
    role: string;
    menuHeaderPass: PolicieDTO[];
    subMenuHeaderPass: PolicieDTO[];
    routePath: PolicieDTO[];
}
