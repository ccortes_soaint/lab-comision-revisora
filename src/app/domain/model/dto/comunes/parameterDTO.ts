export class ParameterDTO {
    type?: string;
    description?: string;
    code?: string;
    name?: string;
    parent?: ParameterDTO;
    state?: boolean;
    value?: string;
}