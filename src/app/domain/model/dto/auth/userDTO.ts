import { ParameterDTO } from '../comunes/parameterDTO';
import { RolDTO } from '../profile/rolDTO';

export class UserDTO {
    user?: String;
    roles?: RolDTO[];
    identType?: ParameterDTO;
    identNumber?: string
    names?: string;
    surnames?: string;
    fullName?: string;
    companyName?: string;
    birthDate?: Date;
    address?: string;
    email?: string;
    phoneNumber?: number;
    cellPhone?: number;
    webSite?: string;
    country?: ParameterDTO;
    state?: ParameterDTO;
    city?: ParameterDTO;
}