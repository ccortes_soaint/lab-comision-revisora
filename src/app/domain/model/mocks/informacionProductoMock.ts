

export const INFORMACIONPRODUCTO = {
    fechaSolicitud: "2019-10-16T03:29:25.000+0000",
    estado: true,
    numeroRadicado: "192",
    solicitante: {numeroDocumento: "987654321"},
    certificado:{
        observaciones: "SinComentarios",
        tipoCertificado: "@TC_LS",
    },
    tipoSolicitud: null,
    solicitudVacacionesDto: {
        fechaInicio: null,
        fechaFin: null,
        diasDisponibles: 15,
        diasAnticipados: 0,
        diasSolicitados: 0,
        fechaReintegro: "2019-10-18T02:25:35.000+0000"
    },
    detalleSolicitudDto:{
        diaSolicitado: "2019-09-23T02:25:35.000+0000",
        horaInicio: "2019-09-23T02:25:35.000+0000",
        horaFin: "2019-09-23T02:25:35.000+0000",
        descripcionMotivo: "Homologacion Materia",
        diasSolicitados: 3,
        fechaInicio: "2019-09-23T02:25:35.000+0000",
        fechaFin: "2019-09-23T02:25:35.000+0000",
        motivoSolicitud: "@MS_TA"
    }
};
