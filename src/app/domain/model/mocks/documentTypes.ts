export const DOCUMENTTYPES = [
    {name: 'Cedula', id: '1'},
    {name: 'Cèdula de extranjerìa', id: '2'},
    {name: 'Tarjeta de identidad', id: '3'},
    {name: 'Pasaporte', id: '4'},
    {name: 'Registro civil', id: '5'},
];
