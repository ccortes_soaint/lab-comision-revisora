export const HEADERSDP = [
    {field: 'codigoTarifa', header: 'Codigo Tarifa'},
    {field: 'concepto', header: 'Concepto'},
    {field: 'descripcion', header: 'Descripcion'},
    {field: 'valor', header: 'Valor'}
];
export const HEADERSCPC = [
    {field: 'entidadRecaudadora', header: 'entidad Recaudadora'},
    {field: 'tipoCuenta', header: 'tipo Cuenta'},
    {field: 'numeroCuenta', header: 'numero Cuenta'},
    {field: 'fechaConsignacion', header: 'fecha Consignacion'},
    {field: 'ciudad', header: 'ciudad'},
    {field: 'valorPagado', header: 'valor Pagado'}
];
export const HEADERSCPS = [
    {field: 'tipoDocumento', header: 'tipo Documento'},
    {field: 'numeroDocumento', header: 'numero Documento'},
    {field: 'nombreApellido', header: 'nombre Apellido'},
    {field: 'entidadFinanciera', header: 'entidad Financiera'},
    {field: 'valor', header: 'Valor'},
    {field: 'fechaTransaccion', header: 'fecha Transaccion'},
    {field: 'estado', header: 'estado'},
    {field: 'numeroAprobacion', header: 'numero Aprobacion'}
];
export const VALUESDEFAULTDP = [
    {codigoTarifa: '001', concepto: 'Fijo', descripcion: 'Tramite 1', valor: 50000}
];
export const VALUESDEFAULTCPC = [
    {entidadRecaudadora: 'Banco Davivienda', tipoCuenta: 'Cuenta Corriente ', numeroCuenta: '002869998688  ', fechaConsignacion: '19/08/2020',ciudad: 'Bogota',valorPagado: 50000}
];
export const VALUESDEFAULTCPS = [
    {tipoDocumento: 'C.C', numeroDocumento: '987654321', nombreApellido: 'Ana Paz', entidadFinanciera: 'Davivienda', valor: '50000',fechaTransaccion: '24/08/2020',estado: 'Aprobada',numeroAprobacion: '123456'}
];
