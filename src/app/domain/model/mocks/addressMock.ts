export const ROADTYPES = [
    {name: 'Autopista', id: '1'},
    {name: 'Avenida', id: '2'},
    {name: 'Avenida calle', id: '3'},
    {name: 'Avenida carrera', id: '4'},
    {name: 'Calle', id: '5'},
    {name: 'Carrera', id: '6'},
    {name: 'Curcunvalar', id: '7'},
    {name: 'Diagonal', id: '8'},
    {name: 'Kilometro', id: '9'},
    {name: 'Peatonal', id: '10'},
    {name: 'Transversal', id: '11'},
    {name: 'Troncal', id: '12'},
    {name: 'Variante', id: '13'},
];

export const QUADRANTPREFIXES = [
    {name: 'A', id: '1'},
    {name: 'B', id: '2'},
    {name: 'C', id: '3'},
    {name: 'D', id: '4'},
    {name: 'E', id: '5'},
    {name: 'F', id: '6'},
    {name: 'G', id: '7'},
    {name: 'H', id: '8'},
    {name: 'I', id: '9'},
    {name: 'J', id: '10'},
];

export const BIS = [
];

export const ORIENTATION = [
    {name: 'Norte', id: '1'},
    {name: 'Sur', id: '2'},
    {name: 'Este', id: '3'},
    {name: 'Oeste', id: '4'},
];

export const PLUGINTYPES = [
    {name: 'Apartamento', id: '1'},
    {name: 'Barrio', id: '2'},
    {name: 'Bloque', id: '3'},
    {name: 'Bodega', id: '4'},
    {name: 'Casa', id: '5'},
    {name: 'Comuna', id: '6'},
    {name: 'Conjunto', id: '7'},
    {name: 'Edificio', id: '8'},
    {name: 'Finca', id: '9'},
    {name: 'Hacienda', id: '10'},
    {name: 'Localidad', id: '11'},
    {name: 'Oficina', id: '12'},
    {name: 'Parcela', id: '13'},
    {name: 'Piso', id: '14'},
    {name: 'Sección', id: '15'},
    {name: 'Torre', id: '16'},
    {name: 'Urbanización', id: '17'},
    {name: 'Vereda', id: '18'},
];


