
export const HEADERSRATE = [
    {field: 'codigoTarifa', header: 'Codigo Tarifa'},
    {field: 'concepto', header: 'Concepto'},
    {field: 'descripcion', header: 'Descripcion'},
    {field: 'tipoMoneda', header: 'Tipo Moneda'},
    {field: 'cantidad', header: 'cantidad'},
    {field: 'valor', header: 'Valor'}
];
export const VALUESRATE= [
    {codigoTarifa: 'xxx', concepto: 'xxxx', descripcion: 'xx xxxxxxxx', tipoMoneda: '1', cantidad: '0', valor: '100000'},
    {codigoTarifa: 'xxx', concepto: 'xxxx', descripcion: 'xx xxxxxxxx', tipoMoneda: '1', cantidad: '0', valor: '50000'},];
export const PREGUNTAS= [
    {name: '¿Desea solicitar evaluación de estudios de bioequivalencia en el registro sanitario?',grupo:"grupo1",respuesta:""},
    {name: '¿Desea solicitar visita en planta con fines de evaluación farmacéutica?',grupo:"grupo2",respuesta:""},
    {name: 'Pais de visita',grupo:"grupo3",respuesta:""},
    {name: 'Número de productos que se amparan en el registro sanitario',grupo:"grupo4",respuesta:""},
    {name: '¿Desea solicitar protección de datos?',grupo:"grupo5",respuesta:""}
];
