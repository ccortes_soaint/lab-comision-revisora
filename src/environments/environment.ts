////////////////////////////////// DEVELOP //////////////////////////////////
const hostParameters = 'https://prod.argoz.co:10084';
const hostSSO = 'http://prod.argoz.co:8080';

////////////////////////////////// RELEASE //////////////////////////////////
// const hostParameters = 'https://argoz.co:8444';

const version = 'v1';

export const environment = {
  production: false,
  host: `${hostParameters}`,
  // Authentication
  keycloak: {
    issuer: `${hostSSO}/` + `auth/realms/SpringBootKeyCloak`,
    redirectUri: 'http://localhost:4200/',
    clientId: 'login-app',
    responseType: 'code',
    scope: 'openid profile email',
    requireHttps: false,
    showDebugInformation: true,
    disableAtHashCheck: true
  },
    //servicio de prueba
  baseUrl: `https://restcountries.eu/rest/v2`
  // Business
};
